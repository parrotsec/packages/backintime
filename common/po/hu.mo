��    �      l  9  �      �     �     �     �  �   �  �   Z                7     ;  �   D  
   �  
   �  
   �  
   �  
   
  
           
   (  
   3     >     D  
   M     X     a  2   e  2   �  0   �     �                ,     ;  @   H     �  e   �  [     J   c     �  %   �     �  %   �  7   !     Y     `  A   l     �     �  q   �  .   9     h     p     �  &   �  .   �  '   �  2     T   @      �     �     �  t   �     T     [  $   `     �     �     �     �  !   �     �  @   �  F        Y     v     {     �     �  
   �     �     �     �     �     �     �               "  	   0     :  
   F  
   Q     \  #   d     �     �     �  "   �     �     �     �     �     �       
        (     0     7  ^   =     �     �     �     �     �     �     �     �  �         �      �      �      �      �      �   &   �   (   $!  '   M!  (   u!  	   �!     �!     �!     �!     "     	"  
   "     $"     1"     7"     Q"     ]"     b"     f"     r"     z"     �"     �"  m   �"     #  .   #     I#     a#  &   g#     �#  $   �#     �#     �#  4   �#      $     )$     7$  m   N$  (   �$  .   �$  "   %      7%     X%     h%     %     �%     �%     �%     �%     �%     �%     �%  y   �%     g&     k&     x&     �&     �&     �&     �&     �&  '   �&  
   '     '  	    '     *'     <'  ,   E'     r'     '     �'  	   �'     �'     �'     �'  	   �'     �'  #   �'     (     4(  !   7(     Y(     x(     ~(     �(     �(     �(  !   �(     �(  &   	)     0)     8)     @)     I)     i)  
   �)     �)     �)  	   �)  &   �)  '   �)  !   �)  #   *  /   <*  &   l*     �*     �*  S   �*     �*     +     +  �  &+  +   �,     �,     -  �   -  �   �-     p.  -   �.     �.     �.  �   �.  
   y/  
   �/  
   �/  
   �/  
   �/  
   �/     �/  
   �/  
   �/     �/     �/     �/     0  	   0  D   0  $   c0  C   �0     �0     �0     �0     1     '1  Y   41  !   �1  �   �1  v   42  g   �2     3  @   -3  #   n3  @   �3  7   �3     4     4  W   4     w4     �4  �   �4  ;   *5     f5  )   n5     �5  5   �5  C   �5  B   6  G   ^6  `   �6  /   7  '   77     _7  �   z7     8     8  *   8     J8     [8     c8     p8  H   �8     �8  D   �8  M   9  6   l9     �9  :   �9     �9     �9     :     :     #:     *:     ::     O:     _:     n:     :     �:     �:     �:     �:     �:  	   �:  3   �:     �:     ;     #;  (   4;  	   ];     g;     �;     �;  -   �;     �;     �;     �;  	   �;     �;  e   �;     b<     i<     |<     �<     �<     �<  *   �<      �<  �   �<     n=     z=     �=     �=     �=  +   �=  /   �=  /   %>  /   U>  /   �>     �>  4   �>  +   �>  >   #?     b?     h?     {?  
   �?     �?  2   �?  
   �?  	   �?     �?     �?     �?     @     @     "@  x   =@     �@  M   �@     A     -A  7   3A     kA  0   ~A     �A     �A  J   �A     B     &B  (   3B  �   \B  3   �B  3   !C  '   UC  $   }C     �C  $   �C     �C     �C     D     D  "   5D     XD     wD     �D  m   �D     E     E     E     1E      CE  !   dE  )   �E  	   �E  6   �E     �E     F     F     (F     HF  8   UF     �F     �F     �F     �F  (   �F     �F     G     1G     ?G  4   BG  1   wG     �G  5   �G  ,   �G     H  !   H  0   AH  ,   rH  4   �H     �H  
   �H  5   �H     ,I     3I     8I  !   DI     fI     �I  	   �I     �I     �I  6   �I  3   �I  1   J  )   CJ  9   mJ  *   �J     �J     �J  o   �J  
   aK     lK     �K                   �   Y       .      X   �   �               �   J   �   f   �   �   �          \       Z       I   d   �   �   G   �   *   {   N   	   �   �   �   �   �   �           g       6      
   A       )   o   h                 i   �   �       �       �   �       C   +      �   q          /   �          �      s       �       �   �   4   `   �          �      =       7   �                  �   z           ~   T   t   S              �       �           �   �   -   �   �   �      �      ,       �              �   :   y   L   1      R   �       [   p   �       !   %   M   �               �   �   �   F       E   &           �   �       �   �   �   0   �   �   _   �       (   j      @   O      ;           �       K   �      �   <       �   �   U       �       �   �   B   �   �   �   r           �   �   �       3         �   ?       n   �   �   P       "   �   �           e       >   �       ^       �       2   �      �   �          |   �   �   �   �   �   v   �   �   �          �   �   V   w   �   5   c       �   �   a   }   �   #   $   �   �           D           Q   b      �   ]   x   9      u   H   l       �       �   m   �   8   �               �       �   �   '   W   �           �   k   �        
Create a new encrypted folder?  EXPERIMENTAL!  KB/sec ### This log has been decoded with automatic search pattern
### If some paths are not decoded you can manually decode them with:
 %(user)s is not member of group 'fuse'.
 Run 'sudo adduser %(user)s fuse'. To apply changes logout and login again.
Look at 'man backintime' for further instructions. %s is not a folder ! %s not found in ssh_known_hosts. ... 3DES-CBC <b>Warning:</b> Wildcards ('foo*', '[fF]oo', 'fo?') will be ignored with mode 'SSH encrypted'.
Only separate asterisk are allowed ('foo/*', 'foo/**/bar') AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add file Add folder Advanced All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s Ask a question At every boot/reboot Auto-remove Backup folders Blowfish-CBC Cache Password for Cron (Security issue: root can read password) Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't mount %s Can't mount '%(command)s':

%(error)s Can't remove folder: %s Can't rename %(new_path)s to %(path)s Can't write to: %s
Are you sure you have write access ? Cancel Cast128-CBC Change these options only if you really know what you are doing ! Changes Changes & Errors Check commands on host %(host)s returned unknown error:
%(err)s
Look at 'man backintime' for further instructions Cipher %(cipher)s failed for %(host)s:
%(err)s Cipher: Command not found: %s Command: Config for encrypted folder not found. Continue on errors (keep incomplete snapshots) Copy links (dereference symbolic links) Copy unsafe links (works only with absolute links) Could not unlock ssh private key. Wrong password or password not available for cron. Couldn't create remote path:
 %s Couldn't find UUID for "%s" Custom Hours Custom Hours can only be a comma separated list of hours (e.g. 8,12,18,23) or */3 for periodic backups every 3 hours Day(s) Day: Deep check (more accurate, but slow) Default Delete Diff Diff Options Disable snapshots when on battery Disabled Do you really want to delete "%(file)s" in %(count)d snapshots?
 Do you really want to delete "%(file)s" in snapshot "%(snapshot_id)s?
 Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Encryption Error: Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Exclude Exclude "%s" from future snapshots? Exclude file Exclude folder Exclude pattern Exclude patterns, files or folders Exit Expert Options FAILED FAQ Failed to take snapshot %s !!! Filter: Finalizing General Global Go To Hash collision occurred in hash_id %s. Incrementing global value hash_collision and try again. Help Highly recommended: Home Host: Hour: Hours: If free inodes is less than: If free space is less than: If you close this window Back In Time will not be able to shutdown your system when the snapshot has finished.
Do you really want to close? Include Include file Include files and folders Include folder Informations Keep all snapshots for the last Keep one snapshot per day for the last Keep one snapshot per month for the last Keep one snapshot per week for the last Keep one snapshot per year for all years Last week Limit rsync bandwidth usage:  List only different snapshots List only equal snapshots to:  Local Local encrypted Log Level: Main profile Mode: Mountprocess lock timeout New profile None Now Older than: Options Parameters: Password Password doesn't match Password-less authentication for %(user)s@%(host)s failed. Look at 'man backintime' for further instructions. Path: Ping %s failed. Host is down or wrong address. Please confirm password Port: Power status not available from system Preserve ACL Preserve extended attributes (xattr) Private Key: Profile "%s" already exists ! Profile '%(profile)s': Enter password for %(mode)s:  Profile: Profile: "%s" Refresh snapshots list Remote host %(host)s doesn't support '%(command)s':
%(err)s
Look at 'man backintime' for further instructions Remote host %s doesn't support hardlinks Remote path exists but is not a directory:
 %s Remote path is not executable:
 %s Remote path is not writable:
 %s Remove Snapshot Removing old snapshots Rename profile Report a bug Restore Restore '%s' Restore '%s' to ... Restore permissions: Restore to ... Root Run Back In Time as soon as the drive is connected (only once every X days).
You will be prompted for your sudo password. SSH SSH Settings SSH encrypted SSH private key Save Password to Keyring Saving config file... Saving permissions... Schedule Schedule udev doesn't work with mode %s Select All Settings Shortcuts Show hidden files Shutdown Shutdown system after snapshot has finished. Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min %d%% free inodes Trying to keep min free space Up Use %1 and %2 for path parameters Use checksum to detect changes User: View Last Log View Snapshot Log View the current disk content View the snapshot made at %s WARNING: This can not be revoked! WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) Weekday: When drive get connected (udev) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! [E] Error, [I] Information, [C] Change day(s) decode paths encfs version 1.7.2 and before has a bug with option --reverse. Please update encfs month(s) mountpoint %s not empty. weeks(s) Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-10-30 18:01+0000
Last-Translator: Germar <Unknown>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
Language: hu
 
Létrehozzak egy új titkosított mappát?  KÍSÉRLETI!  KB/sec ### Ezt a naplófájlt automatikus keresési mintával dekódolták
### Ha néhány útvonal dekódolása elmaradt, a következővel tudod kézzel dekódolni:
 A(z) %(user)s nem tagja a 'fuse' csoportnak.
 Futtasd le a 'sudo adduser %(user)s fuse' parancsot, majd jelentkezz ki és vissza.
Nézd meg a 'man backintime'-ot további részletekért. A(z) %s nem könyvtár A(z) %s nem található itt: ssh_known_hosts. ... 3DES-CBC <b>Figyelem:</b> a helyettesítő karaktreket ('foo*', '[fF]oo', 'fo?') az 'SSH encrypted' mód nem veszi figyelembe.
Csak az elválasztó csillag megengedett ('foo/*', 'foo/**/bar') AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Névjegy Fájl hozzáadása Mappa hozzáadása Szakértői Mindegyik Biztos, hogy szeretnéd megváltoztatni a pillanatképek mappáját? Biztosan törlöd a(z) "%s" profilt? Biztos, hogy el akar távolítani a következő pillanatképet:
%s? Kérdezz Minden (újra)indításkor Automatikus eltávolítás Biztonsági mentés mappái Blowfish-CBC Jelszó mentése a Cron számára (biztonsági figyelmeztetés: a root látja a jelszót) A(z) %s mappa nem hozható létre A crontab nem található.
Biztos, hogy telepítve van a cron?
Ha nem, akkor ki kell kapcsolni az automatikus biztonsági mentést. Nem található a pillanatképek könyvtára.
Ha egy eltávolítható lemezen van, csatlakoztasd és nyomd meg az OK-t A pillanatképek könyvtára nem található.
Ha egy eltávolítható adathordozón van, csatlakoztasd. Nem tudom felcsatolni: %s Nem tudtam felcsatolni a következőt: '%(command)s':

%(error)s A(z) %s mappa nem távolítható el Nem tudom a(z) %(path)s útvonalat átnevezni erre: %(new_path)s Nem tudok ide írni: %s
Biztos, hogy van írási jogod? Mégsem Cast128-CBC Ezeket a beállításokat csak akkor változtasd meg, ha biztosan tudod, mit csinálsz! Változások Változások & Hibák A parancsok ellenőzése a(z) %(host)s kiszolgálón ismeretlen hibát okozott:
%(err)s
Nézd meg a 'man backintime'-ot további részletekért A(z) %(cipher)s nem sikerült a %(host)s esetében:
%(err)s Cipher: A következő parancs nem található: %s Parancs: A titkosított mappa konfigurációja nem található Folytatás hibák esetén (megtartja a nem teljes pillanatképeket) Linkek másolása (szimbolikus linkeknél nem csak a hivatkozást) Nem biztonságos linkek másolása (csak abszolút linkekkel működik) Nem tudtam feloldani az ssh privát kulcsát. Hibás jelszó vagy a cron nem éri el a jelszót. Nem tudom távoli útvonalat elkészíteni:
 %s Nem találom a "%s" meghajtó UUID-ját Egyedi beállítás (óra) Az egyedi órák csak vesszővel elválasztott, listázott értékek lehetnek (pl. 8,12,18,23), vagy */3, ha háromóránként akarsz mentést készíteni. Nap Nap: Alapos ellenőrzés (pontosabb, de lassú) Alapértelmezett Töröl Különbség Diff beállítások Pillanatképek készítésének tiltása akkumulátorról működésnél Kikapcsolva Biztos, hogy törlöd a "%(file)s"-t a "%(count)d-pillanatképből?
 Biztosan töröljem a "%(file)s" fájlt a "%(snapshot_id)s pillanatképből?
 Ne távolítsa el a névvel ellátott pillanatképeket Kész Kész, nincs szükség biztonsági mentés készítésére Szerkesztés Értesítések engedélyezése Titkosítás Hiba: Hibák Tízpercenként Tizenkétóránként Kétóránként 30 percenként Négyóránként Ötpercenként Hatóránként Naponta Havonta Hetente Óránként Kihagyás Kizárjam a "%s"-t a jövőbeli pillanatképekből? Kihagyandó fájl Kihagyandó mappa Kihagyási minta Kihagyandó minták, fájlok és mappák Kilépés Szakértői beállítások MEGHIÚSULT GYIK A %s pillanatkép készítése meghiúsult!!! Szűrő: Véglegesítés Általános Globális Ugrás ide: Hash-ütközés történt: hash_id %s. Megnövelem a hash_collision értékét, és újra próbálom. Súgó Erősen ajánlott: Home Kiszolgáló: Óra: Óra: Ha a szabad inode-ok száma kevesebb, mint Ha a szabad hely kevesebb, mint: Ha bezárod ezt az ablakot, a Back In Time nem tudja lekapcsolni a rendszert a biztonsági mentés elvégzése után.
Biztos bezárod? Hozzáadás Felveendő fájl Felveendő fájlok és mappák Felveendő mappa Információk Tartsd meg a pillanatképeket a legutóbbi: Tarts meg napi egy pillanatképet a legutóbbi: Tarts meg havi egy pillanatképet a legutóbbi: Tarts meg heti egy pillanatképet a legutóbbi: Tarts meg évi egy pillanatképet a legutóbbi: Múlt héten Az rsync sávszélesség-használatának korlátja:  Csak az eltérő pillanatképek listázása Csak a következőkkel megegyező pillanatképek listázása:  Helyi Helyi titkosított Naplózás szintje: Fő profil Mód: A csatolási folyamat túl sok időt vett igénybe Új profil Egyik sem Most Régebbi, mint: Beállítások Paraméterek: Jelszó A jelszavakat nem egyeznek A %(user)s@%(host)s nem tudott jelszó nélkül bejelentkeztni. Nézd meg a 'man backintime'-ot további részletekért. Elérési út: A(z) %s ping sikertelen volt. A kiszolgáló nem működik vagy rossz a cím. Erősítsd meg a jelszót Port: A rendszer nem adja meg az energiaellátás állapotát ACL-ek megtartása Kiterjesztett jogosultságok (xattr) megtartása Privát kulcs A "%s" már létezik! A(z) '%(profile)s' profil: add meg a jelszót a következőhöz: %(mode)s  Profil: Profil: "%s" Pillanatképek listájának frissítése A(z) %(host)s távoli kiszolgáló nem támogatja a következőt: '%(command)s':
%(err)s
Nézd meg a 'man backintime'-ot további részletekért A(z) %s kiszolgáló nem támogatja a hardlinkeket. A távoli útvonal létezik, de nem könyvtár:
 %s A távoli útvonal nem futtatható:
 %s A távoli útvonal nem írható:
 %s Pillanatkép eltávolítása Régi pillanatképek eltávolítása Profil átnevezése Hiba jelentése Visszaállítás A(z) '%s' visszaállítása A(z) '%s' visszaállítása ide... Engedélyek visszaállítása: Visszaállítás ide... Root Futtassa a Back In Time-ot, amint csatolod a meghajtót (X naponta egyszer).
Be fogja kérni a sudo jelszót. SSH Az SSH beállításai SSH-val titkosított SSH privát kulcs Jelszó mentése a kulcstartóba Konfigurációs fájl mentése... Hozzáférési jogosultságok mentése... Ütemterv Az udev időzítése nem működik ezzel a móddal: %s Minden kijelölése Beállítások Billentyűparancsok Rejtett fájlok megjelenítése Leállítás Állítsd le a rendszert, ha elkészült a pillanatkép. Okos eltávolítás Pillanatkép neve Pillanatkép: %s Pillanatképek A pillanatkép-könyvtár nem érvényes Pillanatkép készítése Pillanatkép készítése Ezen a héten Ma Megpróbálok legalább %d%% szabad inode-ot tartani A minimálisan megadott szabad hely kialakítása Fel Használja ezeket útvonal-paraméterként: %1 és %2 Checksum használata hibák felfedezéséhez Felhasználó: Utolsó naplófájl megtekintése A pillanatképek naplófájljának megtekintése A lemez jelenlegi tartalmának megtekintése A %s időpontban észült pillanatkép megtekintése Nem lehet visszavonni! HIBÁKKAL! %s másodperc várakozás. %s másodperc várakozás. Honlap Hét Hét napja: Ha csatlakozik a meghajtó (udev) Hova mentse a pillanatképeket Dolgozom... Dolgozik: Év Tegnap Egy pillanatkép nem hasonlítható össze önmagával Nem adható hozzá a biztonsági mentés almappája Nem adható hozzá a biztonsági mentés mappája Nem távolíthatod el az utolsó profilt! Legalább egy könyvtárat ki kell jelölnie a mentéshez [E] hiba, [I] információ, [C] változtat napból dekódolás útvonalai Az encfs az 1.7.2-es és korábbi verzióknál hibás, ha a --reverse opciót használod. Frissítsd az encfs-t hónapból A %s csatolási pont foglalt hétből 