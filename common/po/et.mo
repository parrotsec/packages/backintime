��    s      �  �   L      �	     �	     �	     �	     �	  
   �	     �	     �	  2   
  2   4
  0   g
     �
     �
     �
     �
  [   �
  J   <     �  7   �     �     �     �                               )     .     3     H     O     V     g  	   w     �  
   �     �     �     �  "   �     �     �     �  
   �     �                         ,     1     7     =     Y     a     n     �     �  	   �  
   �     �     �     �     �     �     �     �     �          "     0     G     W     n     }     �     �     �     �     �  	   �     �     �            	   "     ,     L     Z  	   j     t     z     }     �     �     �     �     �     �     �     �       
        (     1  	   9  &   C  '   j  !   �  #   �  /   �                 �  !     �     �     �  	   �  
   �     �       0     ,   B  +   o  $   �     �     �     �  [   	  P   e     �  =   �               3     H     O     X     _     h     z     �     �  	   �  	   �     �     �  	   �     �  
   �     �     �       ,   %     R     Y     f     n     w          �     �     �     �     �     �     �     �  
   �     �                  	   /     9     I     U     Z  
   a     l     t     �     �     �     �     �     �     �               (     4     L     b     i     r     �     �     �     �      �     �     �  
   �          	  	             ,     @      Z  	   {  	   �  
   �     �     �     �  	   �     �     �  &   �  >     2   J  %   }  B   �  	   �     �  
   �     m           s   F              %      	   
   Z   !   h   f   2       ;   R          /           ,                    <       N       ^   K   o       A      8       `               e   $         4   L      @       i   .   #   J   5       l   r   P       *   Q   G   ?       \       U          V   M   &               7           H      Y   6   T   =          +      >      O      q   0          a       c                '                 :       n       d       j   9   E   D      W   I   (   1   b   "       C       ]   B   S          -   k          p      3      [   _       )   g          X    %s is not a folder ! ... About Add file Add folder Advanced All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s At every boot/reboot Auto-remove Backup folders Can't create folder: %s Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Changes Changes & Errors Command not found: %s Command: Day(s) Day: Diff Disabled Done Edit Enable notifications Error: Errors Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exclude file Exclude folder Exclude patterns, files or folders Exit FAILED Filter: Finalizing General Global Go To Help Highly recommended: Home Host: Hour: If free space is less than: Include Include file Include files and folders Include folder Informations Last week Log Level: Main profile New profile None Now Older than: Options Parameters: Profile "%s" already exists ! Profile: Profile: "%s" Refresh snapshots list Remove Snapshot Removing old snapshots Rename profile Restore Restore permissions: Restore to ... Saving config file... Saving permissions... Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Up User: View Last Log View Snapshot Log View the current disk content View the snapshot made at %s WITH ERRORS ! Website Week(s) Weekday: Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! day(s) month(s) weeks(s) Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:25+0000
Last-Translator: Tauno Erik <sedumacre@gmail.com>
Language-Team: Estonian <et@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s ei ole kataloog! ... Programmist Lisa fail Lisa kaust Põhjalikum Kõik Oled kindel, et tahad muuta tõmmiste kataloogi? Oled kindel, et tahad kustuda profiili "%s"? Oled kindel, et tahad eemalada tõmmist:
%s Igal käivitamisel/taaskäivitamisel Automaatselt eemalda Tagavarakoopia kataloogid Ei saa luua kataloogi: %s Ei leia tõmmiste kataloogi.
Kui see on eemaldataval seadmel palun ühenda see ja vajuta OK Ei leia tagavarakoopia kataloogi.
Kui see on eemaldatav seade palun ühenda see. Ei saa eemaldada kataloogi: %s Ei saa kirjutada: %s
Oled kindel, et sul kirjutamis õigused? Muutused Muudatused & Veateated Käsku ei leitud: %s Käsk: Päev(a) Päev: Erinevus Välja lülitatud Valmis Muutmine Luba teated Veateade: Veateated Iga 10 minuti tagant Iga 5 minuti tagant Iga päev Iga kuu Iga nädal Jäta välja Jäta välja fail Jäta välja kataloog Jäta välja mustrid, failid või kataloogid Välju EBAÕNNESTUS Filter: Lõpetan Üldine Üleüldine Mine Abi Rangelt soovitatud: Kodukataloog Server: Tund: Kui vabaruumi on vähem kui: Kaasa Kaasa fail Kaasa failid ja kataloogid Kaasa katloog Teave Eelmine nädal Logitase: Peamine profiil Uus profiil Pole Nüüd Vanem kui: Valikud Parameetrid: Profiil "%s" on juba olemas! Profiil: Profiil: "%s" Uuenda tõmmiste nimekirja Eemalda Tõmmis Eemalda vanad tõmmised Muuda profiili nime Taasta Taastamise õigused: Taasata ... Salvesta seade fail ... Salvesta õigused ... Seaded Otseteed Näita peidetud faile Tark eemaldamine Tõmmise Nimi Tõmmis: %s Tõmmis Tõmmise kataloog ei ole kehtiv! Võta tõmmis Võta tõmmis See nädal Täna Üles Kasutaja: Vaata Viimast Logi Vaata Tõmmise Logi Vaata praeguse ketta sisu Vaata tõmmist, mis on tehtud %s VIGATEGA! Veebileht Nädal(at) Nädalapäev: Kuhu salvestada tõmmis Töötan... Töötan: Aasta(t) Eile Sa ei saa võrrelda tõmmist iseendaga Sa ei saa kaasata tagavarakoopia kataloogis olevaid katalooge! Sa ei saa kaasata tagavarakoopia kataloogi ennast! Sa ei saa eemaldada viimast profiili! Sa pead valima vähemalt ühe kataloogi tagavarakoopia tegemiseks! päev(ad) kuu(d) nädal(ad) 