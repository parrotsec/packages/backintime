��    �      �  �   <	      P     Q     q  �   �  �        �     �     �  
   �  
   �  
   �  
   �  
   �  
          
     
         +     1  
   :  2   E     x     �     �     �     �  e   �  J   3     ~  %   �     �  %   �  7   �     )     0     <     D     U     k  &   t     �     �     �     �  !   �     �     �                7     <  
   Q     \     c     j     {     �     �     �     �     �  	   �     �  
   �  
   �          	          %     *     9     @  
   _     j     r     y          �     �     �     �     �     �     �     �     �  	   �     �                    '     3     7     C     K     W     n     �  4   �     �     �     �               +     3     H     M     Q     _     o     �     �  '   �     �  	   �     �     �  	   �          (     6  	   F     P     V     t     w     �     �     �  &   �     �     �     �       
   7     B     K  	   S  '   ]  !   �  #   �  /   �  S   �     O  �  h  !        6  �   E  �   �     �     �     �  
   �  
   �  
   �  
   �  
   �  
          
     
         +     1     G  2   [     �     �     �     �     �  �   �  g   �     �  ,   	     6  /   T  6   �     �     �     �     �     �       4     "   L     o     �     �  (   �     �     �  
   �  /   �           '     A     O     U     [     m     }     �     �     �     �     �     �     �     �                       -      2      F   %   K      q      }      �      �      �      �      �   	   �      �      �      �      �      �      !     !     (!     .!     ?!  
   P!     [!     g!     m!     �!     �!     �!     �!     �!  4   �!     "     #"     0"     J"     Z"  	   r"     |"     �"     �"     �"     �"  %   �"     �"     �"  3    #     4#     D#     L#     i#     #  &   �#     �#     �#     �#     �#     �#     �#  3   �#     /$     ;$     Y$  *   e$     �$  	   �$  )   �$     �$     �$     �$     %     %  .   %  *   B%  #   m%  R   �%  X   �%  %   =&     M       <   B   K                  �   |   c          q       �   %   h   u             .   w   `          Q   C      z       ~       f      5                   7   �       �   n   A                 \              �   P      l       s       U   W   &   Y       +   4         �   r       1   I       v       ^   >      �       L              J   F   /   =   6   Z      T   (   _       #          '   0   [   3   *       i                 H   �   g   !   9   �       O   e   �              k       d   x   ;       p         �   2       �           �   R   �              j   X       y   
       N   "   E   :   )   ,   �       S       -       V       	   ?   o       {   t   �      D   @      G   }                   8   b   �   m   �   $   ]       �   a    
Create a new encrypted folder?  EXPERIMENTAL! ### This log has been decoded with automatic search pattern
### If some paths are not decoded you can manually decode them with:
 %(user)s is not member of group 'fuse'.
 Run 'sudo adduser %(user)s fuse'. To apply changes logout and login again.
Look at 'man backintime' for further instructions. %s is not a folder ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add file Add folder Are you sure you want to delete the profile "%s" ? At every boot/reboot Auto-remove Backup folders Blowfish-CBC Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it. Can't mount %s Can't mount '%(command)s':

%(error)s Can't remove folder: %s Can't rename %(new_path)s to %(path)s Can't write to: %s
Are you sure you have write access ? Cancel Cast128-CBC Changes Changes & Errors Command not found: %s Command: Config for encrypted folder not found. Couldn't find UUID for "%s" Custom Hours Day(s) Default Deferring backup while on battery Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Encryption Error: Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Exclude Exclude file Exclude folder Exit Expert Options FAILED Failed to take snapshot %s !!! Finalizing General Global Go To Help Highly recommended: Home Host: Hour(s) Hour: If free space is less than: Include Include folder Informations Last week Local Local encrypted Main profile Month(s) New profile Now Older than: Options Parameters: Password doesn't match Please confirm password Profile "%s" already exists ! Profile '%(profile)s': Enter password for %(mode)s:  Profile: Profile: "%s" Removing old snapshots Rename profile Repeatedly (anacron) Restore Restore permissions: Root SSH SSH encrypted SSH private key Saving config file... Saving permissions... Schedule Schedule udev doesn't work with mode %s Settings Shortcuts Show hidden files Smart remove Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min free space Up Use checksum to detect changes User: View the current disk content WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) When drive get connected (udev) Where to save snapshots Working... Working: Year(s) Yesterday You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! encfs version 1.7.2 and before has a bug with option --reverse. Please update encfs mountpoint %s not empty. Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:15+0000
Last-Translator: Ivo Xavier <ivofernandes12@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 
Criar uma nova pasta encriptada?  EXPERIMENTAL! ### Este ficheiro log foi descodificado com os padrões automáticos de pesquisa
### Se alguns caminhos não estão descodificados pode descodifica-los manualmente com:
 %(user)s não é membro do grupo 'fuse'.
 Executar 'sudo adduser %(user)s fuse'. Para aplicar as alterações faça logout e login.
Leia 'man backintime' para mais informações. %s não é uma directoria ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Sobre Adicionar um ficheiro Adicionar pasta ... Tem a certeza que desja remover este perfil "%s" ? A cada arranque/reinício Auto-remoção Directorias de backup Blowfish-CBC Impossível criar pasta: %s Crontab não encontrado.
Confirme que tem o cron instalado.
Caso não esteja, deverá desactivar todas as cópias de segurança automáticas. Não foi possível encontrar a pasta dos snapshots.
Se estiver numa drive amovível, ligue-a por favor. Não foi possível montar %s Impossível montar '%(command)s':

%(error)s Impossível remover pasta: %s Impossível renomear %(new_path)s para %(path)s Impossível escrever em : %s
Verifique se tem acesso . Cancelar Cast128-CBC Alterações Alterações & Erros Comando não encontrado: %s Comando: Configuração para pasta encriptada não encontrada Impossível encontrar UUID para %s A Horas Definidas Dia(s) Padrão Adiar o backup se estiver a usar bateria Desactivado Não remover snapshots nomeadas Concluído Concluído, cópia de segurança desnecessária Editar Activar as notificações Encriptação Erro: Erros A cada 10 minutos A cada 12 horas A cada duas horas A cada 30 minutos A cada quatro horas A cada 5 minutos A cada 6 horas Diariamente Mensalmente Semanalmente A cada hora Excluir Excluir ficheiro Excluir pasta Sair Opções Avançadas ERRO Falhou a criação da snapshot %s !!! A finalizar Geral Global Ir para Ajuda Altamente recomendado: Pasta Pessoal Servidor: Hora(s) Hora: Se espaço livre é inferior a: Incluir Incluir pasta Informações Semana passada Local Local encriptado Perfil Principal Mês/Meses Novo perfil Agora Mais antigo do que: Opções Parâmetros: Password não coincide Por favor, confirmar password O perfil "%s" já existe! Perfil '%(profile)s': Insira password for %(mode)s:  Perfil: Perfil: "%s" Remover snapshots antigas Renomear perfil Repetidamente (anacron) Restaurar Restaurar permissões Raiz SSH SSH encriptado Chave privada SSH Gravar ficheiro de configuração ... Gravar permissões ... Agenda Calendarização udev não funciona com o perfil %s Configurações Atalhos Mostrar ficheiros escondidos Remoção inteligente Capturas de ecrã A pasta dos Snapshots não é valida ! Tirar snapshot Tirar snapshot Esta semana Hoje Tentar manter Subir Use a soma de verificação para detectar mudanças Utilizador: Ver conteúdo actual do disco COM ERROS ! Aguardar %s segundo. Aguardar %s segundos. Página Web Semana(s) Qunado o dispositifo for conectado (udev) Onde guardar as snapshots A Processar... A processar: Ano(s) Ontem Não pode incluir a sub-directoria de backup ! Não pode incluir a directoria de backup ! Não pode remover o último perfil! Tem de selecionar pelo menos uma directoria para realizar a cópia de segurança ! A versão 1.7.2 de encfs tem um erro com a opção --reverse. Por favor, actualize encfs Ponto de montagem %s não está vazio 