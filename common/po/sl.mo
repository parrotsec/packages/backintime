��    V      �     |      x     y     �     �     �  
   �  
   �  
   �  
   �  
   �  
   �     �  
   �  
   �     
  0        A     V     c     j     v     �     �     �     �     �     �     �     �     �     �  
   	     	     !	     0	     >	     O	     ]	     m	  	   {	     �	  
   �	  
   �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	  	   
     
     
     +
     8
     A
     E
     Q
     o
     }
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
            	   "     ,     2  !   5     W     _          �  	   �  &   �  �  �     �     �     �     �  
   �  
     
     
     
   #  
   .     9  
   A  
   L  
   W  !   b      �     �  	   �     �     �     �     �     �     �  
             )      6     W  &   `     �     �     �     �     �     �     �     �     �  
   �  
     	             2     8  
   K     V     e     n     w     ~     �     �     �     �     �     �  	   �     �  
   �     �               '     ?     F     L     P     _  
   s     ~     �     �     �     �     �     �     �  %   �               ,     I     M  +   U                       <   T   '       3      I   +   9   A   G                 2   K   P         M                        J   @   	       Q       (   #       %   8           !   H   L       "       ?   -                                 7         D   :      6              4      O   >   &       /   C   E   R   ,              .   $      F           ;          0   1           5   S   )                       B                                   *   V               =   U   N   
     EXPERIMENTAL! %s is not a folder ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Are you sure you want to remove the snapshot:
%s At every boot/reboot Blowfish-CBC Cancel Cast128-CBC Command not found: %s Command: Custom Hours Day(s) Default Diff Diff Options Disabled Don't remove named snapshots Done Done, no backup needed Encryption Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Exclude pattern Exit Expert Options FAILED Finalizing Global Go To Help Home Hour(s) Include folder Last week Local Local encrypted Main profile Month(s) Now Parameters: Profile "%s" already exists ! Profile: "%s" Remove Snapshot Repeatedly (anacron) Restore Root SSH SSH encrypted SSH private key Settings Show hidden files Snapshot Name Snapshots Take snapshot Taking snapshot This week Today Up Use %1 and %2 for path parameters Week(s) When drive get connected (udev) Where to save snapshots Year(s) Yesterday You can't compare a snapshot to itself Project-Id-Version: Back In Time 0.9.10
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-02-14 11:38+0000
Last-Translator: Matic Gradišer <Unknown>
Language-Team: <cvelbar@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
X-Poedit-Country: SLOVENIA
X-Poedit-Language: Slovenian
  ESKPERIMENTALNO! %s ni mapa ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 O programu Res želite zbrisati posnetek:
%s Ob vsakem zagonu/ponovnem zagonu Blowfish-CBC Prekliči Cast128-CBC Ukaza ni mogoče najti: %s Ukaz: Specifične ure Dni Privzeto Primerjava Možnosti za primerjavo Onemogočeno Ne odstranjuj posnetkov z imenom Narejeno Narejeno, varnostna kopija ni potrebna Šifriranje Vsakih 10 minut Vsakih 12 ur Vsaki 2 uri Vsakih 30 minut Vsake 4 ure Vsakih 5 minut Vsakih 6 ur Vsak dan Vsak mesec Vsak teden Vsako uro Izključitveni vzorec Izhod Napredne možnosti SPODLETELO Zaključevanje Splošno Pojdi na Pomoč Dom Ura(Ure) Vključi imenik Prejšnji teden Lokalno Lokalno šifrirano Glavni profil Mesec(ev) Zdaj Parametri: Profil "%s" Že obstaja ! Profil: "%s" Odstrani posnetek Ponavljajoče (anacron) Obnovi Koren SSH SSH šifrirano Privatni SSH ključ Nastavitve Pokaži skrite datoteke Ime posnetka Posnetki Naredi posnetek Naredi posnetek Ta teden Danes Gor Uporabi %1 in %2 kot vrednosti za pot Tednov Ko se priključi naprava (udev) Kam naj se shranijo posnetki Let Včeraj Posnetka ni mogoče primerjati s samim sabo 