��    ^       �  �      H     I     i     x  "   �  �   �  �   0  R   �  ;     �   A     �      �           "   �   +   �   �   
   �!  
   �!  
   �!  
   �!  
   �!  
   �!     �!  
   �!  
   �!     �!     �!     �!     �!  
   "     "     ("     7"     F"     O"  2   S"  2   �"  >   �"  H   �"  0   A#     r#     �#     �#     �#     �#  M   �#      $     ($  @   5$     v$  e   �$  [   �$  J   P%     �%  %   �%     �%  %   �%  *   &  7   9&     q&     x&  A   �&  	   �&     �&     �&  q   �&     ['  3   z'  .   �'     �'     �'     �'     (  &   (  .   <(     k(  '   p(  a   �(  2   �(  j   -)  T   �)      �)     *  &   **     Q*  t   ^*     �*     �*     �*  $   �*     +  !   +     5+     <+  �   �+  �   @,     �,     �,  !   �,     -  @   '-  F   h-  G   �-  ,   �-     $.     A.     ^.     c.     z.     .     �.     �.  
   �.     �.     �.     �.     �.     �.     �.     �.     
/     /     (/  	   6/     @/  
   L/  
   W/     b/     i/  #   q/     �/    �/     �0     �0     �0  "   �0     1     1     &1     -1  (   11     Z1     t1     �1     �1     �1  
   �1     �1  +   �1     2  i  %2     �3     �3     �3  ^   �3     4     4     4  '   !4     I4     O4     W4     ]4     d4     �4  �   �4     )5     15     >5     X5     g5     t5  &   �5  (   �5  '   �5  (   6     56     >6     L6  	   Z6     d6     l6     �6     �6     �6     �6  
   �6     �6     �6     �6     7     "7     <7  �   H7     �7     �7  *   �7     8     #8  l   /8     �8  F   �8  �   �8     �9     �9     �9  m   �9  !   H:     j:     p:  .   �:     �:  m  �:  T   <<     �<  &   �<  �   �<     �=  $   �=     �=  +   �=     >  4   ,>     a>     j>     x>  )   �>  )   �>     �>  m   �>  (   Z?  .   �?  "   �?      �?     �?     �?  %   @  +   3@     _@     v@     �@     �@     �@     �@     �@     �@     �@     �@  �   A  �   �A  L   pB  S   �B  ;   C  B   MC     �C     �C     �C     �C     �C     �C  y   �C  U   lD  !   �D     �D     E     E     E     #E     3E     LE     bE     xE  '   �E  
   �E     �E     �E  2   �E  	   �E      F     F      F  ,   )F     VF     cF     lF     ~F     �F  	   �F     �F     �F     �F     �F  <   �F     .G     <G     YG  Z   iG  ;   �G  :    H  	   ;H     EH     KH  #   XH     |H     �H  !   �H     �H     �H     �H     �H     �H     	I     'I  !   DI  K   fI     �I  &   �I  w   �I  `   _J     �J     �J     �J     �J     �J  
   K     K  \   %K     �K  	   �K  &   �K  '   �K  !   �K  #   L  v   )L  /   �L  &   �L     �L     M     
M     M     M     (M  S   0M     �M     �M     �M     �M  5   �M     �M     N  �  #N     �O     �O     �O  .   �O  �   #P  �   �P  W   FQ  F   �Q  �   �Q     �R  #   �R     �R     �R  �   �R  �   �S  
   CT  
   NT  
   YT  
   dT  
   oT  
   zT     �T  
   �T  
   �T     �T  
   �T     �T     �T     �T  $   �T     	U      U  	   7U     AU  C   FU  5   �U  D   �U  O   V  ;   UV     �V     �V  
   �V     �V     �V  d   �V  5   ^W     �W  G   �W     �W  �   X  p   �X  b   �X     ]Y  *   qY     �Y  5   �Y  0   �Y  F   Z     dZ     kZ  9   wZ     �Z  
   �Z     �Z  s   �Z  )   O[  >   y[  6   �[     �[     �[  	   \     \  ,   ;\  <   h\     �\  ,   �\  f   �\  >   A]  u   �]  g   �]  $   ^^     �^  &   �^     �^  �   �^     m_     u_     z_  )   �_     �_  -   �_     �_  �   �_  ~   w`  �   �`     �a     �a  -   �a     �a  D   b  I   Fb  @   �b  3   �b     c  (   c     Fc  ,   Nc  	   {c     �c     �c     �c  
   �c     �c     �c     �c     �c     �c     �c     �c     d     d     *d  	   8d     Bd     Od     [d     gd  	   nd  0   xd     �d  H  �d      f     f     -f  &   @f     gf     of     �f     �f  3   �f  ,   �f  ,   g  ,   /g  '   \g     �g     �g     �g  -   �g     �g  �  �g     }i     �i     �i  b   �i     �i      j     j  /   j     Jj  
   Qj     \j     cj      kj  !   �j  �   �j  	   8k     Bk     Pk     kk     zk  .   �k  -   �k  0   �k  4   l  0   Il  	   zl     �l     �l     �l     �l  '   �l  $   �l  *   m     9m     ?m  
   Om     Zm     fm  5   mm  
   �m      �m  	   �m  �   �m     ~n     �n  8   �n     �n     �n  z   �n     co  R   ro  �   �o     �p  	   �p     �p  u   �p  ,   bq  	   �q  $   �q  7   �q     �q  �  
r  Q   �s     �s  ,   �s  �   *t     %u  !   1u     Su  '   bu     �u  4   �u     �u     �u     �u  ,   �u  ,   !v  (   Nv  k   wv  %   �v  1   	w  $   ;w  %   `w     �w     �w  $   �w  5   �w      x     "x     6x     Ix  
   \x     gx     xx     �x     �x     �x  �   �x  �   �y  J   sz  W   �z  ?   {  L   V{     �{  )   �{     �{     �{     �{     |  �   |  \   �|  $   �|  $   #}     H}     L}     _}     m}     }     �}     �}     �}  '   �}  
   ~     ~     ~  1   $~  	   V~     `~     w~  	   �~  8   �~     �~     �~     �~     
     #     7  *   I  
   t       %   �  D   �     �      �     0�  a   D�  =   ��  N   �     3�     ?�     D�  *   U�  .   ��     ��  *   ��  .   ށ     �     �     �     0�  !   O�      q�  '   ��  K   ��     �  '   �  |   7�  q   ��     &�  
   /�  	   :�     D�      d�  
   ��     ��  i   ��     �     �  7   �  9   E�  3   �  )   ��  o   ݅  5   M�  $   ��     ��     ��     ��     φ     ؆  	   �  b   �  
   Q�  "   \�     �     ��  7   ��     ؇  )   �       �   �   �   U              3              �     \  �   N   B  h       �     S       �       6      -   _   
  W      q   u   W   V   ;  �   ]          �               #  j       H   �   "   X      �   M   �   }           Q      �      �   y   Q         ?           �       �             +      �   %   �   l       J       �   �   -  �   	  �   �           O                           2         �   7        >             �     <   K  )      �          �       �   G  1   `   �       �     �   �   �   �   �   <  @  �           �   �   ^             A   �     D          �       �   �   k       i   T   O  o      P  �              2        F          m   \   '      �   {      �       5   �   !  |           �   *  M  '   Y      �       A  �                  �   �   z        �   w     r           Z   �   �   �   4  1  ,   *   8   R          %  �   �                   �       �   L  �       �       I  �   �       �   �       .  >              #           5  $   9          :         �   4   F   L         I       =                 �   x   P   p   �       s   D  �   @   �      �   ~     C       )   �                  f   0   �   V          d         �   g   �       a   	     /  �   �   �   �   E  &       �   J      C  �   U      �                   �   [  /   v   t      �   �   �   �       G       �   N  �   �   �      :   R  �   �       �   �       (   K   "  �       �   �   S  �   �   c   �   �   �   �   6   0        �   �   �   H  �   =   �   n   �           X   �   �   3       �       ^  $  �   7  T  &     �   .      �          (  �   +   �   ?  ;       Z  �                         ,      �   �   �   b           [   !   �   �   9   8     �   �   
   �         e   �   E   Y       ]   B        
Create a new encrypted folder?  EXPERIMENTAL!  KB/sec  and add your user to group 'fuse' "%s" is a symlink. The linked target will not be backed up until you include it, too.
Would you like to include the symlinks target instead? ### This log has been decoded with automatic search pattern
### If some paths are not decoded you can manually decode them with:
 %(appName)s is not configured. Would you like to restore a previous configuration? %(proc)s not found. Please install e.g. %(install_command)s %(user)s is not member of group 'fuse'.
 Run 'sudo adduser %(user)s fuse'. To apply changes logout and login again.
Look at 'man backintime' for further instructions. %s is not a folder ! %s not found in ssh_known_hosts. ... 3DES-CBC <b>Warning:</b> %(app)s uses EncFS for encryption. A recent security audit revealed several possible attack vectors for this. Please take a look at 'A NOTE ON SECURITY' in 'man backintime'. <b>Warning:</b> Wildcards ('foo*', '[fF]oo', 'fo?') will be ignored with mode 'SSH encrypted'.
Only separate asterisk are allowed ('foo/*', 'foo/**/bar') AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add Add default Add file Add folder Add prefix to SSH commands Add to Exclude Add to Include Advanced All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove all newer files in '%(path)s'? Are you sure you want to remove all newer files in your original folder? Are you sure you want to remove the snapshot:
%s Ask a question At every boot/reboot Authors Auto-remove Backup folders Backup local files before overwriting or
removing with trailing '%(suffix)s'. Backup replaced files on restore Blowfish-CBC Cache Password for Cron (Security issue: root can read password) Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't mount %s Can't mount '%(command)s':

%(error)s Can't remove folder: %s Can't rename %(new_path)s to %(path)s Can't unmount %(proc)s from %(mountpoint)s Can't write to: %s
Are you sure you have write access ? Cancel Cast128-CBC Change these options only if you really know what you are doing ! Changelog Changes Changes & Errors Check commands on host %(host)s returned unknown error:
%(err)s
Look at 'man backintime' for further instructions Check if remote host is online Check if remote host support all necessary commands Cipher %(cipher)s failed for %(host)s:
%(err)s Cipher: Command not found: %s Command: Config File Help Config for encrypted folder not found. Continue on errors (keep incomplete snapshots) Copy Copy links (dereference symbolic links) Copy public ssh-key "%(pubkey)s" to remote host "%(host)s".
Please enter password for "%(user)s": Copy unsafe links (works only with absolute links) Could not install Udev rule for profile %(profile_id)s. DBus Service '%(dbus_interface)s' wasn't available Could not unlock ssh private key. Wrong password or password not available for cron. Couldn't create remote path:
 %s Couldn't find UUID for "%s" Create a new SSH key without Password. Custom Hours Custom Hours can only be a comma separated list of hours (e.g. 8,12,18,23) or */3 for periodic backups every 3 hours Day(s) Day: Decode Deep check (more accurate, but slow) Default Deferring backup while on battery Delete Destination filesystem for '%(path)s is a sshfs mounted share. sshfs doesn't support hard-links. Please use mode 'SSH' instead. Destination filesystem for '%(path)s' is formatted with FAT which doesn't support hard-links. Please use a native Linux filesystem. Destination filsystem for '%(path)s' is a SMB mounted share. Please make sure the remote SMB server supports symlinks or activate '%(copyLinks)s' in '%(expertOptions)s'. Diff Diff Options Disable snapshots when on battery Disabled Do you really want to delete "%(file)s" in %(count)d snapshots?
 Do you really want to delete "%(file)s" in snapshot "%(snapshot_id)s?
 Do you really want to restore this files(s)
into new folder '%(path)s': Do you really want to restore this files(s): Do you want to exclude this? Don't remove named snapshots Done Done, no backup needed ETA: Edit Edit user-callback Enable notifications Encryption Error Error: Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Every: Exclude Exclude "%s" from future snapshots? Exclude file Exclude files bigger than value in %(prefix)s.
With 'Full rsync mode' disabled this will only affect new files
because for rsync this is a transfer option, not an exclude option.
So big files that has been backed up before will remain in snapshots
even if they had changed. Exclude files bigger than:  Exclude folder Exclude pattern Exclude patterns, files or folders Exit Expert Options FAILED FAQ Failed to create new SSH key in %(path)s Failed to load config: %s Failed to save config: %s Failed to take snapshot %s !!! Failed to write new crontab. Filter: Finalizing Folder Found leftover '%s' which can be continued. Full snapshot path:  Full system backup can only create a snapshot to be restored to the same physical disk(s) with the same disk partitioning as from the source; restoring to new physical disks or the same disks with different partitioning will yield a potentially broken and unusable system.

Full system backup will override some settings that may have been customized. Continue? General Global Go To Hash collision occurred in hash_id %s. Incrementing global value hash_collision and try again. Help Highly recommended: Home Host/User/Profile-ID must not be empty! Host: Hour(s) Hour: Hours: If free inodes is less than: If free space is less than: If you close this window Back In Time will not be able to shutdown your system when the snapshot has finished.
Do you really want to close? Include Include file Include files and folders Include folder Informations Keep all snapshots for the last Keep one snapshot per day for the last Keep one snapshot per month for the last Keep one snapshot per week for the last Keep one snapshot per year for all years Key File Last Log View Last check %s Last week License Limit rsync bandwidth usage:  List only different snapshots List only equal snapshots to:  Local Local encrypted Log Level: Main profile Mode: Modify for Full System Backup Month(s) Mountprocess lock timeout New profile Newer versions of files will be renamed with trailing '%(suffix)s' before restoring.
If you don't need them anymore you can remove them with '%(cmd)s' No config found None Nothing changed, no new snapshot necessary Now Older than: Only restore files which do not exist or
are newer than those in destination.
Using "rsync --update" option. Options Options must be quoted e.g. --exclude-from="/path/to/my exclude file". Other snapshots will be blocked until the current snapshot is done.
This is a global option. So it will effect all profiles for this user.
But you need to activate this for all other users, too. Parameters: Password Password doesn't match Password-less authentication for %(user)s@%(host)s failed. Look at 'man backintime' for further instructions. Paste additional options to rsync Path: Pause snapshot process Ping %s failed. Host is down or wrong address. Please confirm password Please navigate to the snapshot from which you want to restore %(appName)s's configuration. The path may look like: 
%(samplePath)s

If your snapshots are on a remote drive or if they are encrypted you need to manually mount them first. If you use Mode SSH you also may need to set up public key login to the remote host%(addFuse)s.
Take a look at 'man backintime'. Please verify this fingerprint! Would you like to add it to your 'known_hosts' file? Port: Power status not available from system Prefix to run before every command on remote host.
Variables need to be escaped with \$FOO.
This doesn't touch rsync. So to add a prefix
for rsync use "%(cbRsyncOptions)s" with
%(rsync_options_value)s

%(default)s: %(def_value)s Preserve ACL Preserve extended attributes (xattr) Private Key: Private key file "%(file)s" does not exist. Profile "%s" already exists ! Profile '%(profile)s': Enter password for %(mode)s:  Profile: Profile: "%s" Question Redirect stderr to /dev/null in cronjobs. Redirect stdout to /dev/null in cronjobs. Refresh snapshots list Remote host %(host)s doesn't support '%(command)s':
%(err)s
Look at 'man backintime' for further instructions Remote host %s doesn't support hardlinks Remote path exists but is not a directory:
 %s Remote path is not executable:
 %s Remote path is not writable:
 %s Remove Remove Snapshot Remove newer files in original folder Removing leftover '%s' folder from last run Removing old snapshots Rename profile Repeatedly (anacron) Report a bug Restore Restore '%s' Restore '%s' to ... Restore Config Restore Settings Restore permissions: Restore selected file or folder.
If this button is grayed out this is most likely because "%(now)s" is selected in left hand snapshots list. Restore selected files or folders to the original destination and
delete files/folders which are not in the snapshot.
This will delete files/folders which where excluded during taking the snapshot!
Be extremely careful!!! Restore the currently shown folder and all its content to a new destination. Restore the currently shown folder and all its content to the original destination. Restore the selected files or folders to a new destination. Restore the selected files or folders to the original destination. Restore to ... Resume snapshot process Root Run 'ionice': Run 'nice': Run 'rsync' with 'nocache': Run Back In Time as soon as the drive is connected (only once every X days).
You will be prompted for your sudo password. Run Back In Time repeatedly. This is useful if the computer is not running regularly. Run in background on remote Host. Run only one snapshot at a time SSH SSH Settings SSH encrypted SSH private key Save Password to Keyring Saving config file... Saving permissions... Schedule Schedule udev doesn't work with mode %s Select All Sent: Settings Shebang in user-callback script is not executable. Shortcuts Show full Log Show hidden files Shutdown Shutdown system after snapshot has finished. Smart remove Snapshot Snapshot Log View Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Speed: Start BackInTime Stop snapshot process Take a new snapshot regardless of there were changes or not. Take snapshot Take snapshot with checksums Taking snapshot The authenticity of host "%(host)s" can't be established.

%(keytype)s key fingerprint is: This folder doesn't exist
in the current selected snapshot! This is NOT a snapshot but a live view of your local files This week Today Translations Trying to keep min %d%% free inodes Trying to keep min free space Up Use %1 and %2 for path parameters Use checksum to detect changes User: View View Last Log View Snapshot Log View the current disk content View the snapshot made at %s WARNING: This can not be revoked! WARNING: deleting files in filesystem root could break your whole system!!! WITH ERRORS ! Waiting %s second. Waiting %s seconds. Warning: if disabled and the remote host
does not support all necessary commands,
this could lead to some weird errors. Warning: if disabled and the remote host
is not available, this could lead to some
weird errors. Website Week(s) Weekday: When drive get connected (udev) Where to save snapshots Working... Working: Would you like to copy your public SSH key to the
remote host to enable password-less login? Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You did not choose a private key file for SSH.
Would you like to generate a new password-less public/private key pair? You must select at least one folder to backup ! [E] Error, [I] Information, [C] Change as cron job day(s) decode paths default disabled enabled encfs version 1.7.2 and before has a bug with option --reverse. Please update encfs month(s) mountpoint %s not empty. on local machine on remote host user-callback script has no shebang (#!/bin/sh) line. weeks(s) when taking a manual snapshot Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-01-31 19:11+0000
Last-Translator: Jonatan Nyberg <Unknown>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 
Skapa en ny krypterad mapp?  EXPERIMENTELL!  KB/sek  lägg till din användare till gruppen "fuse" "%s" är en symlink. Det länkade målet kommer inte att säkerhetskopieras tills du inkluderar det också.
Vill du inkludera symlinks-målet istället? ### Den här loggen har avkodats med automatiskt sökmönster
### Om några sökvägar inte har avkodats kan du manuellt avkoda dem med:
 %(appName)s är inte konfigurerad. Vill du återställa till en tidigare konfiguration? %(proc)s hittades inte. Vänligen installera t.ex. %(install_command)s %(user)s är inte medlem i gruppen "fuse".
 Kör "sudo adduser %(user)s fuse". För att tillämpa ändringar logga ut och logga in igen.
Se "man backintime" för ytterligare instruktioner. %s är inte en mapp! %s hittades inte i ssh_known_hosts. ... 3DES-CBC <b>Varning:</b> %(app)s använder EncFS för kryptering. En ny säkerhetsrevision avslöjade flera möjliga angreppsvektorer för detta. Ta en titt på "A NOTE ON SECURITY" i "man backintime". <b>Varning:</b> Jokertecken ("foo*", "[fF]oo", "fo?") ignoreras med läget "SSH krypterat".
Endast separat asterisk är tillåten ("foo/*", "foo/**/bar") AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Om Lägg till Lägg till standard Lägg till fil Lägg till mapp Lägg till prefix till SSH-kommandon Lägg till i exkludera Lägg till i inkludera Avancerat Alla Är du säker på att du vill ändra mappen för ögonblicksbilder? Är du säker på att du vill ta bort profilen "%s" ? Är du säker på att du vill ta bort alla nyare filer i "%(path)s"? Är du säker på att du vill ta bort alla nyare filer i din ursprungliga mapp? Är du säker på att du vill ta bort ögonblicksbilden:
%s Ställ en fråga Vid varje start/omstart Utvecklare Automatisk borttagning Mappar för säkerhetskopior Säkerhetskopiera lokala filer innan du skriver över eller
ta bort med efterföljande "%(suffix)s". Säkerhetskopiering ersatte filer vid återställning Blowfish-CBC Cache-lösenord för Cron (säkerhetsproblem: root kan läsa lösenord) Kan inte skapa mapp: %s Kan inte hitta crontab.
Är du säker på att cron är installerad?
Om inte så måste du inaktivera alla automatiska säkerhetskopior. Kan inte hitta mappen för ögonblicksbilder.
Anslut enheten om den finns på en flyttbar enhet och tryck på OK Det går inte att hitta mapp för ögonblicksbilder.
Om den är på en flyttbar enhet, anslut den. Kan inte montera %s Kan inte montera "%(command)s":

%(error)s Kan inte ta bort mapp: %s Kunde inte byta namn från %(new_path)s till %(path)s Kan inte avmontera %(proc)s från %(mountpoint)s Kan inte skriva till: %s
Är du säker på att du har skrivrättighet? Avbryt Cast128-CBC Ändra dessa inställningar endast om du vet vad du gör! Ändringslogg Ändringar Ändringar & fel Kontrollera kommandon på värd %(host)s gav okänt fel:
%(err)s
Se "man backintime" för ytterligare instruktioner Kontrollera om fjärrvärd är uppkopplad Kontrollera om fjärrvärd stöder alla nödvändiga kommandon Chiffer %(cipher)s misslyckades för %(host)s:
%(err)s Chiffer: Kommando hittades inte: %s Kommando: Hjälp för konfigurationsfil Config för krypterad mapp har inte hittats. Fortsätt vid fel (behåll icke kompletta ögonblicksbilder) Kopiera Kopiera länkar (dereference symbolic links) Kopiera offentlig ssh-nyckel "%(pubkey)s" till fjärrvärd "%(host)s".
Ange lösenord för "%(user)s": Kopiera osäkra länkar (fungerar endast med absoluta länkar) Kunde inte installera Udev-regel för profil %(profile_id)s. DBus-tjänst "%(dbus_interface)s" var inte tillgängligt Kunde inte låsa upp privat ssh-nyckel. Fel lösenord eller lösenord är inte tillgängligt för cron. Kunde inte skapa fjärrsökväg:
 %s Kunde inte hitta UUID för "%s" Skapa en ny SSH-nyckel utan lösenord. Anpassade timmar Anpassade timmar kan endast vara en kommaseparerad lista över timmar (t.ex. 8,12,18,23) eller */3 för periodiska säkerhetskopior var 3:e timme Dag(ar) Dag: Avkoda Djupkontroll (mer noggrann, men långsam) Standard Uppskjut säkerhetskopiering vid batteridrift Ta bort Destinationsfilsystem för "%(path)s är en sshfs-monterad delning. sshfs stöder inte hard-links. Vänligen använd läget "SSH" istället. Destinationsfilsystem för "%(path)s" är formaterat med FAT som inte stöder hard-links. Använd ett inbyggt Linux-filsystem. Destinationsfilsystem för "%(path)s" är en SMB-monterad delning. Kontrollera att fjärr-SMB-servern stöder symlinks eller aktivera '%(copyLinks)s' i "%(expertOptions)s". Jämförelse Jämförelsealternativ Inaktivera ögonblicksbilder vid batteridrift Inaktiverad Vill du verkligen ta bort "%(file)s" i %(count)d ögonblicksbilder?
 Vill du verkligen ta bort "%(file)s" i ögonblicksbild "%(snapshot_id)s?
 Vill du verkligen återställa files(s)
till ny mapp '%(path)s': Vill du verkligen återställa denna/dessa fil(er): Vill du utesluta detta? Ta inte bort namngivna ögonblicksbilder Färdig Färdig, ingen säkerhetskopiering behövdes Tid kvar: Redigera Redigera user-callback Aktivera aviseringar Kryptering Fel Fel: Fel Var 10:e minut Var 12:e timme Varannan timme Var 30:e minut Var 4:e timme Var 5:e minut Var 6:e timme Varje dag Varje månad Varje vecka Varje timme Varje: Exkludera Exkludera "%s" från framtida ögonblicksbilder? Exkludera fil Exkludera filer större än värdet i %(prefix)s.
Med "Full rsync-läge" inaktiverat påverkas endast nya filer
eftersom detta är ett överföringsalternativ för rsync, inte ett exkluderingsalternativ.
Så stora filer som har säkerhetskopierats tidigare kommer att behållas i överblicksbilder
även om de hade förändrats. Exkludera filer större än:  Exkludera mapp Exkludera mönster Exkludera mönster, filer eller mappar Avsluta Expertinställningar MISSLYCKADES Vanliga frågor Misslyckades med att skapa ny SSH-nyckel i %(path)s Misslyckades med att ladda konfiguration: %s Misslyckades med att spara konfiguration: %s Misslyckades att skapa ögonblicksbild %s!!! Misslyckades med att skriva ny crontab. Filter: Färdigställer Mapp Hittade överbliven "%s" som kan fortsättas. Full ögonblicksbild-sökväg:  Full systemsäkerhetskopiering kan bara skapa en ögonblicksbild som ska återställas till samma fysiska disk(ar) med samma diskpartitionering som från källan; återställa till nya fysiska skivor eller samma skivor med olika partitionering ger ett potentiellt brutet och oanvändbart system.

Full systemsäkerhetskopiering åsidosätter några inställningar som kan ha anpassats. Fortsätta? Allmänt Global Gå till Hash-kollision inträffade i hash_id %s. Öka globalvärdet för hash_collision och försök igen. Hjälp Starkt rekommenderat: Hem Värd/användare/profil-ID får inte vara tomt! Värd: Timme(-ar) Timme: Timmar: Om lediga inoder är mindre än: Om ledigt utrymme är mindre än: Om du stänger detta fönster kan Back In Time inte stänga av ditt system när överblicksbilden är färdig.
Vill du verkligen stänga? Inkludera Inkludera fil Inkludera filer och mappar Inkludera mapp Information Behåll alla ögonblicksbilder för de senaste Behåll en ögonblicksbild per dag de senaste Behåll en ögonblicksbild per månad de senaste Behåll en ögonblicksbild per vecka för de senaste Behåll en ögonblicksbild per år för alla år Nyckelfil Senaste loggvisning Senaste kontroll %s Förra veckan Licens Begränsa rsync-bandbreddsanvändning:  Lista endast olika ögonblicksbilder Lista endast lika ögonblicksbilder till:  Lokal Lokal krypterad Loggnivå: Huvudprofil Läge: Modifiera för fullständig systemsäkerhetskopiering Månad(er) Monteringsprocess lås tiden ute Ny profil Nyare versioner av filer kommer att omdöpas med efterföljande "%(suffix)s" innan de återställs.
Om du inte behöver dem längre kan du ta bort dem med "%(cmd)s" Ingen konfiguration hittades Inga Inget har förändrats, ingen ny ögonblicksbild behövs Nu Äldre än: Återställ endast filer som inte existerar eller
är nyare än de i destinationen.
Använd alternativet "rsync - update". Inställningar Alternativ måste citeras t.ex. --exclude-from="/sökväg/till/min exkludera fil". Andra ögonblicksbilder blockeras tills den aktuella ögonblicksbilden är färdig.
Detta är en global inställning. Så det kommer att påverka alla profiler för denna användaren.
Men du behöver aktivera detta för alla andra användare också. Parametrar: Lösenord Lösenord matchar inte Lösenordslös autentisering för %(user)s@%(host)s misslyckades. Se "man backintime" för ytterligare instruktioner. Klistra in ytterligare alternativ till rsync Sökväg: Pausa process för ögonblicksbilder Pinga %s misslyckades. Värd är nere eller fel adress. Bekräfta lösenord Navigera till ögonblicksbilden från vilken du vill återställa %(appName)ss konfiguration. Sökvägen kan se ut som: 
%(samplePath)s

Om dina ögonblicksbilder finns på en fjärrdisk eller om de är krypterade måste du manuellt montera dem först. Om du använder Mode SSH kan du också behöva ställa in inloggningen för offentlig nyckel till fjärrvärden %(addFuse)s.
Ta en titt på "man backintime". Kontrollera detta fingeravtryck! Vill du lägga till det i din "known_hosts"-fil? Port: Strömstatus inte tillgängligt från system Prefix att köra före varje kommando på fjärrvärd.
Variabler behöver undvikas med \$FOO.
Detta berör inte rsync. Så för att lägga till ett prefix
för rsync använd "%(cbRsyncOptions)s" med
%(rsync_options_value)s

%(default)s: %(def_value)s Behåll ACL Behåll utökade attribut (xattr) Privat nyckel: Privat nyckelfil "%(file)s" finns inte. Profil "%s" finns redan! Profil "%(profile)s": Ange lösenord för %(mode)s:  Profil: Profil: "%s" Fråga Omdirigera stderr till /dev/null i cronjobb. Omdirigera stdout till /dev/null i cronjobb. Uppdatera listan över ögonblicksbilder Fjärrvärd %(host)s stöder inte "%(command)s":
%(err)s
Se "man backintime" för ytterligare instruktioner Fjärrvärd %s stöder inte hardlinks Fjärrsökväg finns men är inte en katalog:
 %s Fjärrsökväg är inte körbar:
 %s Fjärrsökväg är inte skrivbar:
 %s Ta bort Ta bort ögonblicksbild Ta bort nyare filer i originalmappen Tar bort överbliven "%s"-mapp från senaste körning Tar bort gamla ögonblicksbilder Byt namn på profil Upprepad (anacron) Rapportera ett fel Återskapa Återställ "%s" Återställ "%s" till... Återställ Config Återställ inställningar Återställ behörigheter: Återskapa vald fil eller mapp.
Om denna knapp är gråtonad är det högst sannolikt för att "%(now)s" är vald i listan över ögonblicksbilder på vänstra sidan. Återställ valda filer eller mappar till ursprunglig destinationen och
ta bort filer/mappar som inte finns i ögonblicksbilden.
Detta kommer att ta bort filer/mappar som var exkluderade när ögonblicksbilden togs!
Var extremt försiktig!!! Återskapa aktuella mappen och hela dess innehåll till en ny destination. Återskapa aktuella mappen och hela dess innehåll till den ursprungliga destinationen. Återskapa valda filerna eller mapparna till en ny destination. Återskapa valda filerna eller mapparna till den ursprungliga destinationen. Återskapa till... Återuppta process för ögonblicksbilder Root Kör "ionice": Kör "nice": Kör "rsync" med "nocache": Kör Back In Time så snart enheten är ansluten (endast en gång var X dag).
Du kommer att bli tillfrågad om ditt sudo-lösenord. Kör Back In Time upprepade gånger. Detta är användbart om datorn inte körs regelbundet. Kör i bakgrunden på fjärrvärden. Kör bara en ögonblicksbild i taget SSH SSH-inställningar SSH krypterad SSH privat nyckel Spara lösenord till nyckelring Spara konfigurationsfil... Sparar behörigheter... Schema Schema udev fungerar inte med läget %s Välj alla Skickat: Inställningar Shebang i user-callback skript är inte körbart. Genvägar Visa fullständig logg Visa dolda filer Stäng av Stäng av systemet efter ögonblicksbild har slutförts. Smart borttagning Ögonblicksbild Ögonblicksbilds loggvisning Namn på ögonblicksbild Ögonblicksbild: %s Ögonblicksbilder Mappen för ögonblicksbilder är ogiltig! Hastighet: Starta BackInTime Stoppa process för ögonblicksbilder Ta en ny ögonblicksbild oavsett om det fanns ändringar eller inte. Ta ögonblicksbild Ta ögonblicksbild med checksums Tar ögonblicksbild Autenticiteten för värd "%(host)s" kan inte fastställas.

%(keytype)s nyckelfingeravtryck är: Denna mapp finns inte
i den aktuellt valda ögonblicksbilden! Detta är INTE en ögonblicksbild utan en levande visning av dina lokala filer Denna vecka Idag Översättningar Försöker behålla minst %d%% fria inoder Försöker att behålla minimum ledigt utrymme Upp Använd %1 och %2 för sökvägsparametrar Använd checksum för att detektera ändringar Användare: Visa Visa senaste logg Visa logg för ögonblicksbild Visa det aktuella diskinnehållet Visa ögonblicksbilden skapad %s VARNING: Det här kan inte återkallas! VARNING: Att ta bort filer i root filsystemet kan bryta hela ditt system!!! MED FEL! Väntar %s sekund. Väntar %s sekunder. Varning: om inaktiverad och fjärrvärden
inte stöder alla nödvändiga kommandon,
kan detta leda till några konstiga fel. Varning: om den är inaktiverad och fjärrvärden
inte är tillgänglig, kan detta leda till några
konstiga fel. Webbsida Vecka(-or) Veckodag: När enhet blir ansluten (udev) Var ögonblicksbilder ska sparas Arbetar... Arbetar: Vill du kopiera din offentliga SSH-nyckel till
fjärrvärden för att aktivera lösenordslös inloggning? År Igår Du kan inte jämföra en ögonblicksbild med sig själv Du kan inte inkludera en undermapp för säkerhetskopior! Du kan inte inkludera mappen för säkerhetskopior! Du kan inte ta bort den senaste profilen! Du valde inte en privat nyckelfil för SSH.
Vill du skapa ett nytt lösenordsfritt offentligt/privat nyckelpar? Du måste välja minst en mapp att säkerhetskopiera! [E] Fel, [I] Information, [C] Ändra som cronjobb dag(ar) avkoda sökvägar standard inaktiverad aktiverad encfs version 1.7.2 och tidigare har ett fel med alternativet --reverse. Vänligen uppdatera encfs månad(er) monteringspunkten %s är inte tom. på lokal maskin på fjärrvärd user-callback skript har ingen shebang (#!/bin/sh) rad. vecka/veckor vid tagning av en manuell ögonblicksbild 