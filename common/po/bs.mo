��    8      �  O   �      �     �     �     �     �  
                  0     H     ^     g     n     w     |     �     �  	   �     �  
   �     �     �     �     �     �     �     �            	   -     7     D     H     T     \     h     �     �     �     �     �  	   �     �     �     �  	   	            !        >     \     d  	   l  !   v  #   �  /   �  �  �     �	     �	     �	     �	     �	     
  !   
  "   <
     _
     {
     �
     �
  	   �
     �
     �
     �
  	   �
     �
     �
  	   �
                         "      )     J     S     g     w     �     �     �  
   �     �     �     �     �     �     �     �            &   0     W     c     i  &   n      �  
   �  	   �     �  "   �  #   �  2        0              3   #   )         7                 '      /              4       !   6                (      1         *      	      "       $                -          .                                         
       &       8              2   ,   +              5   %               %s is not a folder ! ... About Add file Add folder Auto-remove Can't create folder: %s Can't remove folder: %s Command not found: %s Command: Day(s) Disabled Done Enable notifications Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exit General Global Go To Help If free space is less than: Include Include folder Last week Main profile Now Older than: Options Parameters: Profile "%s" already exists ! Profile: "%s" Restore Root Saving permissions... Settings Shortcuts Show hidden files Smart remove Snapshots folder is not valid ! This week Today Up Use %1 and %2 for path parameters View the current disk content Week(s) Year(s) Yesterday You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:09+0000
Last-Translator: Germar <Unknown>
Language-Team: Bosnian <bs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s nije direktorij ! ... O Dodaje datoteku Dodaj direktorij Automatsko brisanje Nemoguće kreirati direktorij: %s Nemoguće izbrisati direktorij: %s Komanda nije pronađena: %s Komanda: Dan(i) Isključeno Završeno Uključi obavijesti Svakih 10 minuta Svakih 5 minuta Svaki dan Svaki mjesec Svake sedmice Isključi Izlaz Opšte Globalno Idi na Pomoć Ako je sloboni prostor manji od: Uključi Uključi direktorij Prošle sedmice Glavni profil! Sad Starije od: Opcije Parametri: Profil "%s" već postoji ! Profil: "%s" Obnovi Korijen Spremi dozvole ... Postavke Prečice Prikaži skrivene datoteke Pametno brisanje Folder za slike zaslona nije validan ! Ove sedmice Danas Gore Koristite %1 i %2 za parametre putanje Pogledaj trenutni sadržaj diska Sedmica(e) Godina(e) Jučer Ne možeš dodati folder rezerve ! Ne možeš izbrisati zadnji profil! Moraš izabrati najmanje jedan folder za rezervu ! 