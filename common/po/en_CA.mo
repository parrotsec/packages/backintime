��    b      ,  �   <      H     I     ^     b     h  
   q  2   |  2   �  0   �     	     	     .	  e   F	  [   �	  J   
     S
  7   k
     �
     �
     �
     �
     �
  !   �
     �
          #     (     ?     D     Y     j  	   z     �  
   �     �     �     �     �     �     �  
   �     �     �     �                    )     1  	   @     J     W     c     g     s     {  &   �     �     �     �     �     �     
          !     &     <     R     [  	   d     n     �     �     �  	   �     �     �     �  	   �     �             !   !     C     a  &   ~     �     �     �  
   �     �     �  	   �  &   �  '     !   B  #   d  /   �  �  �     X     l     p     v  
     1   �  1   �  2   �     !     -     <  e   T  _   �  N        i  6   �     �     �     �     �     �  !   �               8     =     T     Y     n       	   �     �  
   �     �     �     �     �     �     �  
   �                              "     >     F  	   U     _     l     x     |     �     �  &   �     �     �     �     �               +     3     8     M     `     i  	   r     |     �     �     �  	   �     �     �     �  	   �               &  !   )     K     i  &   �     �     �     �  
   �     �     �  	   �  &   �  &   "      I  "   j  .   �        X       7       Z   Y   R   0               Q               T      L   :   3   )   ^           (   9   "   M      .           ,   A   J   \         ;             6   !      `   =   /          &   C   N   O       E   _       [   ?   >       -   G              b   U   K   D   	      1   ]         %   @   4             '                    
   +   I      *   8       H                                      S   a      5          B       2   <   P          $   #   W           V   F           %s is not a folder ! ... About Add file Add folder Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s Auto-remove Backup folders Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Command not found: %s Command: Day(s) Diff Diff Options Disable snapshots when on battery Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exclude file Exclude folder Exclude pattern Exit Expert Options Finalizing General Global Go To Help Home If free space is less than: Include Include folder Last week Main profile New profile Now Older than: Options Parameters: Power status not available from system Profile "%s" already exists ! Profile: Profile: "%s" Remove Snapshot Removing old snapshots Rename profile Restore Root Saving config file... Saving permissions... Schedule Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min free space Up Use %1 and %2 for path parameters View the current disk content View the snapshot made at %s Waiting %s second. Waiting %s seconds. Website Week(s) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:25+0000
Last-Translator: Germar <Unknown>
Language-Team: English (Canada) <en_CA@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s is not a folder! ... About Add file Add folder Are you sure you want to change snapshots folder? Are you sure you want to delete the profile "%s"? Are you sure you want to remove the snapshot
"%s"? Auto-remove Backup folders Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive, please plug it in and then press OK Can't find snapshots folder.
If it is on a removable drive, please plug it in. Can't remove folder: %s Can't write to: %s
Are you sure you have write access? Command not found: %s Command: Day(s) Diff Diff Options Disable snapshots when on battery Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exclude file Exclude folder Exclude pattern Exit Expert Options Finalizing General Global Go To Help Home If free space is less than: Include Include folder Last week Main profile New profile Now Older than: Options Parameters: Power status not available from system Profile "%s" already exists! Profile: Profile: "%s" Remove Snapshot Remove old snapshots Rename profile Restore Root Save config file ... Save permission... Schedule Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid! Take snapshot Take snapshot This week Today Try to keep min free space Up Use %1 and %2 for path parameters View the current disk content View the snapshot made at %s Waiting %s second. Waiting %s seconds. Website Week(s) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder! You can't include backup folder! You can't remove the last profile! You must select at least one folder to backup! 