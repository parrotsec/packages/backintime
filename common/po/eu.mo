��    ^       �  �      H     I     i     x  "   �  �   �  �   0  R   �  ;     �   A     �      �           "   �   +   �   �   
   �!  
   �!  
   �!  
   �!  
   �!  
   �!     �!  
   �!  
   �!     �!     �!     �!     �!  
   "     "     ("     7"     F"     O"  2   S"  2   �"  >   �"  H   �"  0   A#     r#     �#     �#     �#     �#  M   �#      $     ($  @   5$     v$  e   �$  [   �$  J   P%     �%  %   �%     �%  %   �%  *   &  7   9&     q&     x&  A   �&  	   �&     �&     �&  q   �&     ['  3   z'  .   �'     �'     �'     �'     (  &   (  .   <(     k(  '   p(  a   �(  2   �(  j   -)  T   �)      �)     *  &   **     Q*  t   ^*     �*     �*     �*  $   �*     +  !   +     5+     <+  �   �+  �   @,     �,     �,  !   �,     -  @   '-  F   h-  G   �-  ,   �-     $.     A.     ^.     c.     z.     .     �.     �.  
   �.     �.     �.     �.     �.     �.     �.     �.     
/     /     (/  	   6/     @/  
   L/  
   W/     b/     i/  #   q/     �/    �/     �0     �0     �0  "   �0     1     1     &1     -1  (   11     Z1     t1     �1     �1     �1  
   �1     �1  +   �1     2  i  %2     �3     �3     �3  ^   �3     4     4     4  '   !4     I4     O4     W4     ]4     d4     �4  �   �4     )5     15     >5     X5     g5     t5  &   �5  (   �5  '   �5  (   6     56     >6     L6  	   Z6     d6     l6     �6     �6     �6     �6  
   �6     �6     �6     �6     7     "7     <7  �   H7     �7     �7  *   �7     8     #8  l   /8     �8  F   �8  �   �8     �9     �9     �9  m   �9  !   H:     j:     p:  .   �:     �:  m  �:  T   <<     �<  &   �<  �   �<     �=  $   �=     �=  +   �=     >  4   ,>     a>     j>     x>  )   �>  )   �>     �>  m   �>  (   Z?  .   �?  "   �?      �?     �?     �?  %   @  +   3@     _@     v@     �@     �@     �@     �@     �@     �@     �@     �@  �   A  �   �A  L   pB  S   �B  ;   C  B   MC     �C     �C     �C     �C     �C     �C  y   �C  U   lD  !   �D     �D     E     E     E     #E     3E     LE     bE     xE  '   �E  
   �E     �E     �E  2   �E  	   �E      F     F      F  ,   )F     VF     cF     lF     ~F     �F  	   �F     �F     �F     �F     �F  <   �F     .G     <G     YG  Z   iG  ;   �G  :    H  	   ;H     EH     KH  #   XH     |H     �H  !   �H     �H     �H     �H     �H     �H     	I     'I  !   DI  K   fI     �I  &   �I  w   �I  `   _J     �J     �J     �J     �J     �J  
   K     K  \   %K     �K  	   �K  &   �K  '   �K  !   �K  #   L  v   )L  /   �L  &   �L     �L     M     
M     M     M     (M  S   0M     �M     �M     �M     �M  5   �M     �M     N  �  #N  !   �O     �O     P  -   P  �   9P  �   �P  N   Q  >   �Q  �   R     �R  +   �R     S     S  �   S  �   �S  
   �T  
   �T  
   �T  
   �T  
   �T  
   �T     �T  
   �T  
   �T  
   	U     U     U     -U     ?U     NU  !   mU     �U  
   �U     �U  4   �U  /   �U  7   "V  Z   ZV  /   �V     �V     �V     W     W     3W  b   GW  9   �W     �W  Q   �W     CX  �   \X  d   �X  f   LY     �Y  )   �Y     �Y  +   Z  3   7Z  =   kZ     �Z     �Z  7   �Z     �Z  	   [     [  |   &[  (   �[  H   �[  A   \  	   W\     a\  	   |\     �\  0   �\  >   �\     ]  8   ]  s   U]  =   �]  }   ^  i   �^  &   �^  $   _  +   ;_     g_  �   _     `     `     "`  6   +`  
   b`  .   m`     �`  �   �`  �   :a  �   �a     �b     �b  2   �b  	   �b  R   �b  X   ;c  N   �c  7   �c     d  "   7d     Zd  #   ad     �d     �d  %   �d     �d  	   �d     �d     �d     �d     �d     �d     e     e     (e  
   8e     Ce     Se     [e     be     ie     pe     ye  %   �e     �e  R  �e  5   g     Ig     Zg  /   ng     �g     �g     �g     �g  8   �g  $   h  %   &h  #   Lh  $   ph     �h     �h     �h  /   �h     �h  H  �h     Bj     Kj  
   Tj  Y   _j     �j     �j     �j  9   �j  	   k      k     %k     ,k  4   4k  $   ik  }   �k     l     l     #l     Bl     Pl     \l  ,   xl  /   �l  +   �l  .   m     0m     @m     _m     qm  	   �m  -   �m  0   �m  4   �m     n     &n  
   6n     An     Pn      Wn     xn  *   �n     �n  �   �n     [o     ro  ;   zo     �o     �o  ~   �o     Qp  Y   Yp  �   �p     �q  	   �q     �q  t   �q  $   &r     Kr     Wr  D   sr     �r  �  �r  Q   bt     �t  2   �t    �t     �u  "   	v     ,v  .   ;v     jv  ;   �v     �v     �v     �v  -   �v  -   w     @w  p   ^w  7   �w  :   x  ,   Bx  3   ox     �x     �x  3   �x  2   �x     %y     Cy     Xy     sy     �y     �y     �y     �y     �y     �y  �   	z  �   �z  G   n{  L   �{  A   |  F   E|     �|     �|     �|     �|     �|  !   �|  v   }  R   }}  -   �}     �}     ~     ~     ,~     9~     K~     [~     v~     �~  ,   �~     �~  	   �~  	   �~  F   �~     0     =     V     u  )   |     �  
   �  "   �     �     �     �  $   �  	   =�     G�     W�  B   s�     ��  $   ƀ     �  Y   ��  8   X�  @   ��     ҁ     ߁  
   �  ,   �  !   �     >�  .   C�  -   r�     ��     ��     ��     ͂     �  !   �  ,   &�  Y   S�     ��  *   ��  �   �  f   m�     Ԅ     ݄     �     �     �  	   �     (�  e   0�     ��     ��  1   ��  8   Ӆ  3   �      @�  t   a�  9   ֆ  &   �     7�     S�     Z�  
   o�  	   z�     ��  i   ��  	   ��  #    �     $�     9�  K   M�  
   ��     ��       �   �   �   U              3              �     \  �   N   B  h       �     S       �       6      -   _   
  W      q   u   W   V   ;  �   ]          �               #  j       H   �   "   X      �   M   �   }           Q      �      �   y   Q         ?           �       �             +      �   %   �   l       J       �   �   -  �   	  �   �           O                           2         �   7        >             �     <   K  )      �          �       �   G  1   `   �       �     �   �   �   �   �   <  @  �           �   �   ^             A   �     D          �       �   �   k       i   T   O  o      P  �              2        F          m   \   '      �   {      �       5   �   !  |           �   *  M  '   Y      �       A  �                  �   �   z        �   w     r           Z   �   �   �   4  1  ,   *   8   R          %  �   �                   �       �   L  �       �       I  �   �       �   �       .  >              #           5  $   9          :         �   4   F   L         I       =                 �   x   P   p   �       s   D  �   @   �      �   ~     C       )   �                  f   0   �   V          d         �   g   �       a   	     /  �   �   �   �   E  &       �   J      C  �   U      �                   �   [  /   v   t      �   �   �   �       G       �   N  �   �   �      :   R  �   �       �   �       (   K   "  �       �   �   S  �   �   c   �   �   �   �   6   0        �   �   �   H  �   =   �   n   �           X   �   �   3       �       ^  $  �   7  T  &     �   .      �          (  �   +   �   ?  ;       Z  �                         ,      �   �   �   b           [   !   �   �   9   8     �   �   
   �         e   �   E   Y       ]   B        
Create a new encrypted folder?  EXPERIMENTAL!  KB/sec  and add your user to group 'fuse' "%s" is a symlink. The linked target will not be backed up until you include it, too.
Would you like to include the symlinks target instead? ### This log has been decoded with automatic search pattern
### If some paths are not decoded you can manually decode them with:
 %(appName)s is not configured. Would you like to restore a previous configuration? %(proc)s not found. Please install e.g. %(install_command)s %(user)s is not member of group 'fuse'.
 Run 'sudo adduser %(user)s fuse'. To apply changes logout and login again.
Look at 'man backintime' for further instructions. %s is not a folder ! %s not found in ssh_known_hosts. ... 3DES-CBC <b>Warning:</b> %(app)s uses EncFS for encryption. A recent security audit revealed several possible attack vectors for this. Please take a look at 'A NOTE ON SECURITY' in 'man backintime'. <b>Warning:</b> Wildcards ('foo*', '[fF]oo', 'fo?') will be ignored with mode 'SSH encrypted'.
Only separate asterisk are allowed ('foo/*', 'foo/**/bar') AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add Add default Add file Add folder Add prefix to SSH commands Add to Exclude Add to Include Advanced All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove all newer files in '%(path)s'? Are you sure you want to remove all newer files in your original folder? Are you sure you want to remove the snapshot:
%s Ask a question At every boot/reboot Authors Auto-remove Backup folders Backup local files before overwriting or
removing with trailing '%(suffix)s'. Backup replaced files on restore Blowfish-CBC Cache Password for Cron (Security issue: root can read password) Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't mount %s Can't mount '%(command)s':

%(error)s Can't remove folder: %s Can't rename %(new_path)s to %(path)s Can't unmount %(proc)s from %(mountpoint)s Can't write to: %s
Are you sure you have write access ? Cancel Cast128-CBC Change these options only if you really know what you are doing ! Changelog Changes Changes & Errors Check commands on host %(host)s returned unknown error:
%(err)s
Look at 'man backintime' for further instructions Check if remote host is online Check if remote host support all necessary commands Cipher %(cipher)s failed for %(host)s:
%(err)s Cipher: Command not found: %s Command: Config File Help Config for encrypted folder not found. Continue on errors (keep incomplete snapshots) Copy Copy links (dereference symbolic links) Copy public ssh-key "%(pubkey)s" to remote host "%(host)s".
Please enter password for "%(user)s": Copy unsafe links (works only with absolute links) Could not install Udev rule for profile %(profile_id)s. DBus Service '%(dbus_interface)s' wasn't available Could not unlock ssh private key. Wrong password or password not available for cron. Couldn't create remote path:
 %s Couldn't find UUID for "%s" Create a new SSH key without Password. Custom Hours Custom Hours can only be a comma separated list of hours (e.g. 8,12,18,23) or */3 for periodic backups every 3 hours Day(s) Day: Decode Deep check (more accurate, but slow) Default Deferring backup while on battery Delete Destination filesystem for '%(path)s is a sshfs mounted share. sshfs doesn't support hard-links. Please use mode 'SSH' instead. Destination filesystem for '%(path)s' is formatted with FAT which doesn't support hard-links. Please use a native Linux filesystem. Destination filsystem for '%(path)s' is a SMB mounted share. Please make sure the remote SMB server supports symlinks or activate '%(copyLinks)s' in '%(expertOptions)s'. Diff Diff Options Disable snapshots when on battery Disabled Do you really want to delete "%(file)s" in %(count)d snapshots?
 Do you really want to delete "%(file)s" in snapshot "%(snapshot_id)s?
 Do you really want to restore this files(s)
into new folder '%(path)s': Do you really want to restore this files(s): Do you want to exclude this? Don't remove named snapshots Done Done, no backup needed ETA: Edit Edit user-callback Enable notifications Encryption Error Error: Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Every: Exclude Exclude "%s" from future snapshots? Exclude file Exclude files bigger than value in %(prefix)s.
With 'Full rsync mode' disabled this will only affect new files
because for rsync this is a transfer option, not an exclude option.
So big files that has been backed up before will remain in snapshots
even if they had changed. Exclude files bigger than:  Exclude folder Exclude pattern Exclude patterns, files or folders Exit Expert Options FAILED FAQ Failed to create new SSH key in %(path)s Failed to load config: %s Failed to save config: %s Failed to take snapshot %s !!! Failed to write new crontab. Filter: Finalizing Folder Found leftover '%s' which can be continued. Full snapshot path:  Full system backup can only create a snapshot to be restored to the same physical disk(s) with the same disk partitioning as from the source; restoring to new physical disks or the same disks with different partitioning will yield a potentially broken and unusable system.

Full system backup will override some settings that may have been customized. Continue? General Global Go To Hash collision occurred in hash_id %s. Incrementing global value hash_collision and try again. Help Highly recommended: Home Host/User/Profile-ID must not be empty! Host: Hour(s) Hour: Hours: If free inodes is less than: If free space is less than: If you close this window Back In Time will not be able to shutdown your system when the snapshot has finished.
Do you really want to close? Include Include file Include files and folders Include folder Informations Keep all snapshots for the last Keep one snapshot per day for the last Keep one snapshot per month for the last Keep one snapshot per week for the last Keep one snapshot per year for all years Key File Last Log View Last check %s Last week License Limit rsync bandwidth usage:  List only different snapshots List only equal snapshots to:  Local Local encrypted Log Level: Main profile Mode: Modify for Full System Backup Month(s) Mountprocess lock timeout New profile Newer versions of files will be renamed with trailing '%(suffix)s' before restoring.
If you don't need them anymore you can remove them with '%(cmd)s' No config found None Nothing changed, no new snapshot necessary Now Older than: Only restore files which do not exist or
are newer than those in destination.
Using "rsync --update" option. Options Options must be quoted e.g. --exclude-from="/path/to/my exclude file". Other snapshots will be blocked until the current snapshot is done.
This is a global option. So it will effect all profiles for this user.
But you need to activate this for all other users, too. Parameters: Password Password doesn't match Password-less authentication for %(user)s@%(host)s failed. Look at 'man backintime' for further instructions. Paste additional options to rsync Path: Pause snapshot process Ping %s failed. Host is down or wrong address. Please confirm password Please navigate to the snapshot from which you want to restore %(appName)s's configuration. The path may look like: 
%(samplePath)s

If your snapshots are on a remote drive or if they are encrypted you need to manually mount them first. If you use Mode SSH you also may need to set up public key login to the remote host%(addFuse)s.
Take a look at 'man backintime'. Please verify this fingerprint! Would you like to add it to your 'known_hosts' file? Port: Power status not available from system Prefix to run before every command on remote host.
Variables need to be escaped with \$FOO.
This doesn't touch rsync. So to add a prefix
for rsync use "%(cbRsyncOptions)s" with
%(rsync_options_value)s

%(default)s: %(def_value)s Preserve ACL Preserve extended attributes (xattr) Private Key: Private key file "%(file)s" does not exist. Profile "%s" already exists ! Profile '%(profile)s': Enter password for %(mode)s:  Profile: Profile: "%s" Question Redirect stderr to /dev/null in cronjobs. Redirect stdout to /dev/null in cronjobs. Refresh snapshots list Remote host %(host)s doesn't support '%(command)s':
%(err)s
Look at 'man backintime' for further instructions Remote host %s doesn't support hardlinks Remote path exists but is not a directory:
 %s Remote path is not executable:
 %s Remote path is not writable:
 %s Remove Remove Snapshot Remove newer files in original folder Removing leftover '%s' folder from last run Removing old snapshots Rename profile Repeatedly (anacron) Report a bug Restore Restore '%s' Restore '%s' to ... Restore Config Restore Settings Restore permissions: Restore selected file or folder.
If this button is grayed out this is most likely because "%(now)s" is selected in left hand snapshots list. Restore selected files or folders to the original destination and
delete files/folders which are not in the snapshot.
This will delete files/folders which where excluded during taking the snapshot!
Be extremely careful!!! Restore the currently shown folder and all its content to a new destination. Restore the currently shown folder and all its content to the original destination. Restore the selected files or folders to a new destination. Restore the selected files or folders to the original destination. Restore to ... Resume snapshot process Root Run 'ionice': Run 'nice': Run 'rsync' with 'nocache': Run Back In Time as soon as the drive is connected (only once every X days).
You will be prompted for your sudo password. Run Back In Time repeatedly. This is useful if the computer is not running regularly. Run in background on remote Host. Run only one snapshot at a time SSH SSH Settings SSH encrypted SSH private key Save Password to Keyring Saving config file... Saving permissions... Schedule Schedule udev doesn't work with mode %s Select All Sent: Settings Shebang in user-callback script is not executable. Shortcuts Show full Log Show hidden files Shutdown Shutdown system after snapshot has finished. Smart remove Snapshot Snapshot Log View Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Speed: Start BackInTime Stop snapshot process Take a new snapshot regardless of there were changes or not. Take snapshot Take snapshot with checksums Taking snapshot The authenticity of host "%(host)s" can't be established.

%(keytype)s key fingerprint is: This folder doesn't exist
in the current selected snapshot! This is NOT a snapshot but a live view of your local files This week Today Translations Trying to keep min %d%% free inodes Trying to keep min free space Up Use %1 and %2 for path parameters Use checksum to detect changes User: View View Last Log View Snapshot Log View the current disk content View the snapshot made at %s WARNING: This can not be revoked! WARNING: deleting files in filesystem root could break your whole system!!! WITH ERRORS ! Waiting %s second. Waiting %s seconds. Warning: if disabled and the remote host
does not support all necessary commands,
this could lead to some weird errors. Warning: if disabled and the remote host
is not available, this could lead to some
weird errors. Website Week(s) Weekday: When drive get connected (udev) Where to save snapshots Working... Working: Would you like to copy your public SSH key to the
remote host to enable password-less login? Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You did not choose a private key file for SSH.
Would you like to generate a new password-less public/private key pair? You must select at least one folder to backup ! [E] Error, [I] Information, [C] Change as cron job day(s) decode paths default disabled enabled encfs version 1.7.2 and before has a bug with option --reverse. Please update encfs month(s) mountpoint %s not empty. on local machine on remote host user-callback script has no shebang (#!/bin/sh) line. weeks(s) when taking a manual snapshot Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-01-01 22:04+0000
Last-Translator: Alexander Gabilondo <alexgabi@irakasle.net>
Language-Team: Basque <eu@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 
Sortu karpeta zifratu berri bat?  ESPERIMENTALA!  KB/sec  eta gehitu zure erabiltzailea 'fuse' taldera "%s"  esteka sinboliko bat da. Estekatutako objektuaren babeskopia ez da egingo are eta bera ere gehitu arte.
Nahi duzu gehitu esteka sinbolikoen helburu-objektuak? ### Erregistro hau automatikoki bilatutako txantiloi baten bidez dekodetu da
### Baldin eta bide-izenen bat ez badago dekodetua eskuz dekodetu dezakezu honekin
 %(appName)s ez dago konfiguratua. Nahi duzu leheneratu aurreko konfiguraziora? Ez du %(proc)s aurkitu. Instala ezazu ad.  %(install_command)s %(user)s ez da 'fuse' taldeko partaidea.
 Exekutatu 'sudo adduser %(user)s fuse'. Aldaketak aplikatzeko atera zaitez eta sartu berriro.
Begira ezazu  'man backintime' azalpen zabalagoa izateko. %s ez da karpeta bat ! Ez da aurkitu %s ssh-known-host ostalarian. ... 3DES-CBC <b>Kontuz:</b> %(app)s-k EncFS erabiltzen du kodetzeko. Berriki egindako segurtasun auditoria batek hari egindako hainbat erasoen berri eman du. Eman begirada bat Backintime-ren man-eko   'A NOTE ON SECURITY' oharrari. <b>Kontuz:</b> Komodin karaktereak ('foo*', '[fF]oo', 'fo?') ez dira ezagutuko 'SSH encrypted' moduan.
Bakarrik erabil daitezke banatutako asteriskoak ('foo/*', 'foo/**/bar') AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Honi buruz Gehitu Gehitu lehenetsia Gehitu fitxategia Gehitu karpeta Gehitu aurrizkia SSH komandoei Gehitu kanpoan utzi beharrekoetan Gehitu sartu beharrekoetan Aurreratua Guztiak Seguru zaude aldatu nahi duzula babeskopien karpeta? Seguru zaude  "%s" profila ezabatu nahi duzula? Seguru zaude '%(path)s' tokiko fitxategi berri guztiak? Seguru zaude ezabatu nahi duzula jatorrizko karpetatik berriagoak diren fitxategi guztiak? Seguru zaude babeskopia ezabatu nahi duzula?
%s Zerbait galdetu Abiatze/berrabiaratze guztietan Egileak Ezabaketa automatikoa Babeskopia-karpetak Egin fitxategi lokalen babeskopia
bukaerako '%(suffix)s'-en bidez gainidatzi edo ezabatu aurretik. Egin berreskurapenean ordeztutako  fitxategien babeskopia Blowfish-CBC Cron-entzako cache-pasahitza (Segurtasun akatsa: root-ek pasahitza irakur dezake) Ezin da %s karpeta sortu Ezin da aurkitu crontab.
Seguru zaude crontab instalatuta dagoela?
Crontab ez baduzu babeskopia automatiko guztiak ezgaitu beharko zenuke. Ezin da aurkitu babeskopien karpeta.
Unitate eramangarri batean badago konekta ezazu eta sakatu Ados Ezin du aurkitu babeskopien karpeta.
Karpeta unitate eramangarri batean badago konekta ezazu, mesedez. Ezin da %s muntatu '%(command)s' ezin da muntatu:

%(error)s Ezin da %s karpeta ezabatu Ezin jarri %(new_path)s izena honi %(path)s Ezin izan du %(proc)s desmontatu %(mountpoint)s-tik Ezin da hor idatzi: %s
Ziur zaude idazteko baimenik baduzula? Utzi Cast128-CBC Aldatu aukera hauek bakarrik zertan ari zaren badakizu! Aldaketen egunkaria Aldaketak Aldaketa eta erroreak %(host)s ostalariak txekeo komandoari errore ezezagunaz erantzun dio:
 %(err)s
Begiratu 'man backintime' azalpenak ikusteko. Markatu urrutiko ostalaria linean badago Markatu urrutiko ostalariak behar diren komando guztiak onartzen baditu. Huts egin du cipher %(cipher)s ostalari honetan: %(host)s
%(err)s Zifratua: Ez da komandoa aurkitu: %s Komandoa: Config fitxategiaren laguntza Ez dira aurkitu zifratutako karpetaren ezarpenak Jarraitu erroreak egon arren (mantendu babeskopia osatu gabea) Kopiatu Kopiatu estekak (erreferentzia kendu esteka sinbolikoei) Kopiatu "%(pubkey)s" ssh-gako publikoa "%(host)s" urrutiko ostalarira.
Sartu "%(user)s" erabiltzailearen pasahitza: Kopiatu esteka inseguruak (egin bakarrik esteka absolutuekin) Ezin izan da Udev araua instalatu %(profile_id)s profilarentzat.  '%(dbus_interface)s'  DBus zerbitzua ez zegoen erabilgarri. Ezin da SSH-ren gako pribatua desblokeatu. Okerreko pasahitza edo cron-entzat baliozkoa ez den pasahitza. Ezin da sortu urrutiko bide-izena:
 %s Ezin da UUID aurkitu honentzat: "%s" Sortu pasahitzik gabeko SSH gako berri bat. ordu pertsonalizatuetan Orduak pertsonalizatzeko komaz banatutako ordu-zerrenda erabili behar duzu (hala nola 8,12,18,23) edo */3 hiru ordutan behingo babeskopia egiteko. Egun(ak) Eguna: Dekodetu Egiaztatze sakona (zehaztasun handiagoa, baina motela) Lehenetsia Atzeratu babeskopia bateriaz dabilen bitartean Ezabatu '%(path)s'-(r)en helburuko fitxategi-sistema sshfs-ez muntatutako partekatzea da. sshfs-ek ez du esteka gogorrik onartzen. Erabili 'SSH' haren ordez. '%(path)s'-(r)en helburuko fitxategi-sistemaren formatua FAT da eta ez du esteka gogorrik onartzen. Erabili Linux jatorriko fitxategi-sistema bat. '%(path)s'-(r)en helburuko fitxategi-sistema SMB-ez muntatutako partekatzea da. Egiaztatu urruneko SMB zerbitzariak  esteka sinbolikoak onartzen dituela edo gaitu '%(copyLinks)s'  '%(expertOptions)s'-en. Diff Diff aukerak Ezgaitu babeskopiak egitea bateriaz funtzionatzean Ezgaituta Seguru zaude  "%(count)d babeskopiako  "%(file)s" fitxategia ezabatu nahi duzula?
 Seguru zaude  "%(snapshot_id)s babeskopiako  "%(file)s" fitxategia ezabatu nahi duzula?
 Seguru zaude fitxategi hau(ek) leheneratu 
nahi dituzula  '%(path)s' karpetan: Ziur zaude fitxategi hori(ek) berreskuratu nahi duzula: Nahi duzu hau kanpoan utzi? Ez ezabatu izena duten babeskopiak Eginda Egina, ez da babeskopia egin behar. ETA: Editatu Editutatu erabiltzailearen atzeradeia Gaitu notifikazioak Zifraketa Errorea Errorea: Akatsak 10 minuturo 12 ordutan behin 2 ordutan behin 30 minuturo 4 ordutan behin 5 minuturo 6 ordutan behin Egunero Hilero Astero Orduro Guztiak: Kanpoan utzi Baztertu "%s" hurrengo babeskopietan? Baztertu fitxategia Utzi kanpoan hau baino handiagoak diren fitxategiak %(prefix)s
'rsync osoko modua' desgaituta badago horrek fitxategi berriei soilik eragingo die
zeren eta rsync-entzat hori transferentzia aukera bat da, ez baztertze aukera bat.
Beraz babeskopia lehendik egina zituzten fitxategi handiak mantenduko dira irudian
nahiz eta aldaketak jasan. Utzi kanpoan hau baino handiagoak diren fitxategiak:  Baztertu karpeta Baztertu txantiloia Baztertu txantiloi, fitxategi edo karpeta hauek Irten Aukera aurreratuak HUTS EGIN DU FAQ Huts egin du %(path)s kokalekuan SSH gako berria sortzen Huts egin du ezarpenak kargatzen: %s Huts egin du ezarpenak gordetzen:  %s Huts egi du %s babeskopia egiten!!! Huts egin du crontab berria idazten. Iragazi: Amaitzen Karpeta '%s'  jarraitu daitekeen soberakina aurkitu du. Babeskopiaren bide osoa:  Sistemaren babeskopia osoa bakarrik leheneratu daiteke disko fisiko bere(et)an eta jatorrizko partiketarekin; jatorrizkoa ez diren diskoetan edo partiketetan leheneratzeak sor dezake hautsitako edo egonkorra ez den sistema bat.
Sistemaren babeskopia osoak pertsonalizatutako hainbat ezarpen gainidatz dezake. Jarraitu nahi duzu? Orokorra Orokorra Joan hona: Hash talka gertatu da hemen: hash_id %s. Handitu hash_collision balioa eta saiatu berriz. Laguntza Oso gomendagarria Hasiera Ostalari/Erabiltzaile/Profila-ID ez da hutsik egon behar! Ostalaria Ordu Ordua: Orduak: Baldin eta nodo libreak hauek baino gutxiago badira: Toki librea hau baino txikagoa bada: Leiho hau ixten baduzu Back In Time-k ezin izango du zure sistema itzali babeskopia bukatzean.
Seguru zaude itxi nahi duzula? Sartu Sartu fitxategia sartu fitxategiak eta karpetak Sartu karpeta Informazioa Mantendu babeskopia guztiak Mantendu eguneko babeskopia bana  azkenerako Mantendu hilabeteko babeskopia bana  azkenerako Mantendu asteko babeskopia bana  azkenerako Mantendu babeskopia bana urteko urte guztietan Gako fitxategia Azken erregistroaren ikuspegia Azken kontrola %s Joan den astean Lizentzia Mugatu rsync-en banda-zabaleraren erabilera:  Zerrendatu bakarri desberdinak diren babeskopiak Zerrendatu bakarri berdinak diren babeskopiak hona:  Lokala Lokala zifratua Log maila: Profil nagusia Modua: Aldatu Full System Backup-entzat Hilabete Muntatze prozedura blokeatuta iraungitzean Profil berria Berriagoak diren fitxategiak berrizendatuko dira '%(suffix)s' kokapenarekin leheneratu baino lehen.
Ez badituzu gehiago behar ezabatu ditzakezu '%(cmd)s'-rekin. Ez da ezarpenik topatu Ezer Ez Ez dago aldaketarik. Ez da babeskopia berririk egin behar.. Orain Hau baino zaharragoa: Leheneratu soilik falta diren fitxategiak edo
helburuko tokikoak baino berriagoak direnak.
"rsync --update" aukera erabiltzen. Aukerak Aukerak kakotxen artean idatzi, esate baterako --exclude-from="/path/to/my exclude file". Beste babeskopiak blokeatuko dira oraingo babeskopia egiten den bitartean.
Hau aukera globala da, beraz, erabiltzaile honen profil guztiei eragiten die.
Baina beste erabiltzaileentzat aktibatu behar da ere. Parametroak: Pasahitza Pasahitzak ez datoz bat Huts egin du  %(user)s@%(host)s-en pasahitzik gabeko autentifikazioa. Begiratu  'man backintime' azalpenak ikusteko. Itsatsi aukera gehigarriak rsync-eri Bide-izena: Pausatu babeskopia prozesua Huts egin du %s ping. Ostalaria erorita dago edo helbidea okerra da. Egiaztatu pasahitza mesedez Nabigatu  berreskuratu nahi duzun %(appName)s-en ezarpenak dauden babeskopiara. Bideak honelako itxura izan behar luke:
%(samplePath)s

Zure babeskopiak urrutiko unitate batean badaude edo zifratuta badaude aldez aurretik eskuz muntatu behar dituzu. SSH modua erabiltzen baduzu gako publikoa ezarri beharko duzu urrutiko  ostalarian%(addFuse)s sartzeko
Eman begirada bat 'man backintime' oharrei.. Egiaztatu hatz-marka! Nahi al duzu bera gehitzea zure  'known_hosts' fitxategira? Ataka: Energia egoera ez dago eskurtagarri sistemarentzat Ezarri exekutatzeko urrutiko ostalariko edozein komandoaren aurretik.
Aldagaiak \$FOO bidez alde egin behar dute.
Honek ez dio eragiten rsync-i. Beraz gehitu aurrizki bat
rsync erabili dezan "%(cbRsyncOptions)s" %(rsync_options_value)s-rekin

%(default)s: %(def_value)s Mantendu ACL Mantendu atributu hedatuak (xattr) Gako pribatua: "%(file)s" gako pribatuko fitxategirik ez dago "%s" profila badago honezkero. '%(profile)s' profila: Sartu pasahitza honentzat %(mode)s:  Profila: Profila: "%s" Galdera Birzuzendu stderr /dev/null-era cronjob-etan. Birzuzendu stdout /dev/null-era cronjob-etan. Freskatu babeskopien zerrenda %(host)s urrutiko ostalaria ez du  '%(command)s' onartzen:
%(err)s
Begiratu 'man backintime' azalpenak ikusteko. %s urrutiko ostalariak ez ditu esteka gogorrak onartzen Urrutiko bide-izena badago baina ez da direktorio bat:
 %s Urrutiko bide-izena ez da exekutagarria:
 %s Urrutiko bide-izena ez dauka idazteko baimenik:
 %s Ezabatu Ezabatu babeskopia Ezabatu jatorrizko karpetako fitxategirik berrienak Azken exekuziotik '%s' karpeta soberakina kentzen. Babeskopia zaharrak ezabatzen Berrizendatu profila Behin eta berriz (anacron) Akats baten berri eman Berreskuratu Berreskuratu  '%s' Berreskuratu  '%s' hona... Leheneratu konfigurazioa Ezarpenak berreskuratu Berrezarri baimenak Leheneratu hautatutako fitxategi edo karpetak.
Botoi hau grisa badago gehienetan da "%(now)s" hautatua dagoelako ezkerreko babeskopien zerrendan. Leheneratu hautatutako fitxategiak edo karpetak jatorrizko kokalekura eta
ezabatu babeskopian ez dauden fitxategi/karpetak.
Honek babeskopia egitean baztertutako fitxategi/karpetak ezabatuko ditu!
Argi ibili!!! Leheneratu erakutsitako karpeta eta bere eduki guztia kokaleku berrira. Leheneratu erakutsitako karpeta eta bere eduki guztia jatorrizko kokalekura. Leheneratu hautatutako fitxategiak eta karpetak kokaleku berrira. Leheneratu hautatutako fitxategiak eta karpetak jatorrizko kokalekura. Berreskuratu hona Laburtu babeskopia prozesua Root Exekutatu 'ionice' Exekutatu 'nice': Exekutatu 'rsync' 'nocache'-rekin Exekutatu Back In Time unitatea konektatu bezain pronto (X egunean behin bakarrik).
Zure sudo pasahitza eskatuko zaizu Erabili Back In Time behin eta berriz. Egin bereziki ordenagailua ez badabil ongi. Exekutatu atzeko planoan urrutiko ostalarian. Egin babeskopia bat aldiko. SSH SSH ezarpenak SSH zifratua SSH gako pribatua Gorde pasahitza Gorde ezarpenen fitxategia Baimenak gordetzen ... Programaketa Programatutako udev-a ez dabil %s moduarekin Hautatu denak Bidalita: Ezarpenak Erabiltzalearen atzera-deiaren scriptaren shebang ez da exekutagarria. Lasterbideak Erakutsi erregistro osoa Erakutsi ezkututko fitxategiak Itzali Itzali sistema babeskopia bukatu ondoren. Ezabatze automatikoa Babeskopia Babeskopiaren erregistro ikuspegia Babeskopiaren izena Babeskopia: %s Babeskopiak Babeskopien karpeta ez da baliozkoa! Abiadura: Hasi BackinTime Gelditu babeskopia prozesua Egin babeskopia berria aldaketarik izan ote den kontuan izan gabe. Egin babeskopia Egin babeskopia kontrol baturarekin. Babeskopia egiten. Ezin da egiaztatu "%(host)s" ostalariaren benekotasuna.

%(keytype)s hatz-marka gakoa da: Karpeta hau ez dago
une honetan hautatutako babeskopian! Hau EZ da babeskopia bat zure fitxategi lokalen ikuspegia baizik Aste honetan Gaur Itzulpenak Saiatu gutxienez %d%% nodo libre mantentzen. Saiatu espazio minimoa mantentzen Gora Erabili %1 eta %2 bide-izenen parametroetarako Erabili kontrol-batura aldaketak detektatzeko Erabiltzailea: Ikusi Ikusi azken erregistroa Ikusi babeskopiaren erregistroa Ikusi diskoaren edukia Ikusi %s -ean egindako babeskopia ARGI IBILI: Honek ez dauka atzera bueltarik! KONTUZ: fitxategi-sistemaren erroko fitxategiak ezabatzeak sistema osoa hondatu dezake!!! ERROREAK DAUDE! Itxaroten  segundo %s Itxaroten %s segundo Kontuz: desgaitzen baduzu eta urrutiko ostalariak
behar diren komando guztiak onartzen ez baditu,
ustekabeko erroreak sor daitezke . Kontuz: desgaitzen baduzu eta urrutiko ostalaria
ezin bada erabili, ustekabeko erroreak sor daitezke . Webgunea aste Asteguna Unitatea konektatzean (udev) Non gorde babeskopiak Lanean... Lanean: Nahi al duzu kopiatzea zure SSH gako publikoa
urrutiko ostalarira pasahitzik gabe sartu ahal izateko? urtez Atzo Ezin duzu alderatu babeskopia bat bere buruarekin Ezin duzu babekopiaren azpi-karpetak sartu babeskopian ! Ezin duzu babeskopiaren karpeta sartu babeskopian ! Ezin duzu ezabatu azken profila! Ez duzu aukeratu SSHrako gako fitxategi pribatua.
Nahi al duzu pasahitzik gabeko gako publiko/pribatu parea sortzea? Gutxienez karpeta bat hautatu behar duzu babeskopiarako ! [E] Error, [I] Information, [C] Change programatutako ataza bezala egunez dekodetu bide-izenak lehenetsia ezgaituta gaituta encfs-ren 1.7.2 bertsioak eta honen aurrekoek badute akats bat --reverse aukeran. Eguneratu encfs mesedez hilabetez %s munstatze-puntua ez dago hutsik. ordenagailuan bertan urrutiko ostalarian Erabiltzailearen atzeradeia-scriptak ez dauka shebang (#!/bin/sh) lerrorik. astetarako eskuzko babeskopia egitean 