��    �      �  �   �	      �     �      �          #  
   ,  
   7  
   B  
   M  
   X  
   c     n  
   v  
   �     �     �  
   �     �     �  2   �  2   �  0        J     Y     n     z     �     �  e   �  [     J   p     �  7   �       A        Y     a     r     z     �     �     �     �     �     �  !   �     �     �               #     (     =     D     K     \     k     y     �     �     �  	   �     �  
   �  
   �     �     �     �                    *     1     P  
   X     c     k     r     x     }     �     �     �     �     �     �     �  	   �     �     �                          "     *     6     ?  &   E     l     y     �     �     �     �     �     �     �                    1     @     E     I     V     f     |     �     �  	   �     �     �     �     �  	   �     �             	   0     :     @     ^  !   a     �     �     �  &   �     �     �       
        &     /  	   7  &   A  '   h  !   �  #   �  /   �            �       �  .   �            
      
   +  
   6  
   A  
   L  
   W     b  
   j  
   u  
   �     �     �     �     �  5   �  K     ?   \     �  !   �     �     �       0     �   @  �   �  �   o  0   �  R   $     w  i   �     �     �  	         "     C     O     _     g     {     �  2   �     �  *   �       &     
   E     P     f     r          �     �     �     �     �     �     �                        (      1      E      [   
   q      |      �   '   �      �      �      �      �      !     !     !     -!     C!  	   K!  #   U!     y!     �!     �!     �!  
   �!     �!     �!     �!     �!  
   "     "     ""  
   2"  	   ="  0   G"     x"  %   �"     �"     �"  "   �"     �"  "   	#     ,#     K#  
   d#     o#     #     �#     �#     �#     �#     �#  "   �#     $  
   &$     1$     >$      M$     n$     �$     �$     �$  3   �$     �$     �$  
   %     %  ;   (%     d%  6   m%  0   �%  '   �%     �%  C   &     U&     e&  "   s&     �&     �&     �&  
   �&  0   �&  L   �&  9   I'  ;   �'  A   �'     (     (     "   ?   4   }         @   C              a   f   9   �       +   k   y   8   K   Q   V   b      6       '       D       T   t       �          /   [              *         |   I       �       z           �                         #       n   �       P                 J                   \   {   p      e   0   S      M       �       u       W   $   �      s   q             �   �   m   h      H       G   U          A       1                 F   :          (   =       -       �       !      <   �       �   R   .       ,      g   %   �                   2   l   r   c       �   Y   X   ^   7   i   `   �       &         w   �          �   �   �       ;   L   >       ]       v       E              B   �   d   �   N   x       �       o   ~   �   Z       5           O       �   	   )   �   _   j      3   
    %s is not a folder ! %s not found in ssh_known_hosts. ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add file Add folder Advanced All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s Ask a question At every boot/reboot Auto-remove Backup folders Blowfish-CBC Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Cast128-CBC Change these options only if you really know what you are doing ! Changes Changes & Errors Cipher: Command not found: %s Command: Day(s) Day: Default Diff Diff Options Disable snapshots when on battery Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Error: Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Exclude Exclude file Exclude folder Exclude pattern Exit Expert Options FAILED Failed to take snapshot %s !!! Filter: Finalizing General Global Go To Help Highly recommended: Home Hour: Hours: If free space is less than: Include Include file Include folder Last week Local Main profile Mode: New profile Now Older than: Options Parameters: Password Port: Power status not available from system Private Key: Profile "%s" already exists ! Profile: Profile: "%s" Refresh snapshots list Remove Snapshot Removing old snapshots Rename profile Report a bug Restore Restore '%s' Restore '%s' to ... Restore to ... Root SSH SSH Settings SSH private key Saving config file... Saving permissions... Schedule Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min free space Up Use %1 and %2 for path parameters View the current disk content View the snapshot made at %s WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! month(s) weeks(s) Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:25+0000
Last-Translator: Konstantin Golenberg <jutko.hapa.nu@gmail.com>
Language-Team: Hebrew <he@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s אינה תיקייה ! %s לא נמצא ברשימה ssh_known_hosts. ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 אודות הוספת קובץ הוספת תיקייה הגדרות מתקדמות הכל האם לשנות את תיקיית הגיבויים? האם אתה בטוח שברצונך למחוק את הפרופיל "%s" ? אתה בטוח שברצונך להסיר את הגיבוי:
%s שאל שאלה כל איתחול של מערכת הסרה-אוטומטית תיקיות הגיבוי Blowfish-CBC לא ניתן ליצור את התיקייה: %s לא ניתן למצוא את crontab.
האם cron מותקן?
במידה שלא יש לבטל את כל הגיבויים האוטומטיים. לא ניתן למצוא את תיקיית הגיבויים.
אם היא נמצאת על כונן נשלף יש לחבר אותו וללחוץ על אישור לא ניתן למצוא את תיקיית הגיבויים.
אם היא נמצאת בכונן נשלף עליך לחבר אותו. לא ניתן להסיר את התיקייה: %s לא ניתן לכתוב אל: %s
האם יש ברשותך גישה לכתיבה ? Cast128-CBC עשו שינוים בהגדרות אלו רק אם אתם באמת יודעים מה אתם עושים ! שינויים שינוים ושגיאות צופן: הפקודה לא נמצאה: %s פקודה: יום/ימים יום: ברירת מחדל שינויים אפשרויות שינויים השבת גיבויים כאשר על בטרייה מנוטרל אל תסיר לכידות בעלות שם בוצע הסתיים, לא נדרש גיבוי עריכה אפשר הודעות שגיאה: שגיאות כל 10 דקות כל 12 שעות כל שעתיים כל 30 דקות כל 4 שעות כל 5 דקות כל 6 שעות כל יום כל חודש כל שבוע כל שעה כלול הכללת קובץ הכללת תיקיה הכללת תבנית יציאה אפשרויות מתקדמות נכשל נכשל בלקיחת גיבוי %s !!! סינון: בהליכי סיום כללי כללי עבור אל עזרה מאוד מומלץ: תיקיית הבית שעה: שעות: אם המקום פנוי קטן מ: כלול לכלול קובץ לכלול תיקיה שבוע שעבר מקומי הפרופיל הראשי מצב: פרופיל חדש כעת ישן מ: אפשרויות פרמטרים: סיסמה פורט: מצב הכוח אינו זמין מהמערכת מפתח פרטי: הפרופיל "%s" כבר קיים ! פרופיל: הפרופיל: "%s" לרענן רשימת גיבוים הסר לכידה הסרת גיבויים ישנים שינוי שם לפרופיל דיווח על תקלה שחזור לשחזר '%s' לשחזר '%s' ל... לשחזר ל... תיקיית העל SSH הגדרות SSH מפתח פרטי SSH שומר קובץ הגדרות ... שמירת ההרשאות ... תזמון הגדרות קיצורים הצג קבצים מוסתרים הסרה חכמה שם הגיבוי גיבוי: %s גיבויים תיקיית הגיבויים אינה תקנית ! ביצוע הגיבוי ביצוע הגיבוי השבוע היום נסיון לשמירת כמה שפחות מקום פנוי מעלה השתמש ב־%1 ו־%2 כפרמטרים לניתוב צפיה בתוכן הנוכחי של הכונן צפיה בגיבוי שבוצע ב־%s עם שגיאות ! בהמתנה של שניה אחת. בהמתנה של %s שניות. אתר הבית שבוע/ות איפה לשמור גיבויים פועל... בעבודה: שנה/שנים אתמול לא ניתן להשוות לכידה לעצמה לא ניתן לכלול תת־תיקיות של תיקיית הגיבוי ! לא ניתן לכלול את תיקיית הגיבוי ! לא ניתן להסיר את הפרופיל האחרון ! עליך לבחור לפחות תיקייה אחת לגיבוי ! חודשים שבועות 