��    f      L  �   |      �     �     �     �     �  
   �  2   �  2   	  0   :	     k	     w	     �	  e   �	  [   
  J   `
     �
  7   �
     �
               !     &  !   3     U     ^     {     �     �     �     �     �     �  	   �     �  
   �     �                    .     3     B  
   a     l     t     {     �     �     �     �     �     �     �  	   �     �     �     �     �            &        ?     ]     f     t     �     �     �     �     �     �     �     �  	   �     �               ,  	   9     C     c     q  	   �     �     �     �  !   �     �     �  &        6     >     F  
   ^     i     r  	   z  &   �  '   �  !   �  #   �  /     �  I     �                         )     E     _     ~     �     �  p   �  U   '  L   }     �  3   �       	   .     8     <     I  !   V     x          �  !   �     �     �  	   �     �     �     �          
  	             (     8     E     L     Y     s     �     �     �     �  	   �     �     �     �     �     �     �     �                    .  	   5     ?     ^     {     �     �     �     �     �  	   �     �     �            	             1     >     K     W     ^     z     �     �     �     �     �     �     �               +     2     6     I     V     d     h     o     �     �  '   �  '   �         5   =      	   3              U   c   C   ]   M           8   :      A       b       f   D   N              F   \   9               &       _      $   1                           G   ,      Z   T   <       (       e      W      P   I      Y   !   #              J   0   >       B       )       +   
              [   "   a      ^                       `       ;   L   ?           V      *   '           Q       /      S       4   6   E   R   d   H      X                          2   %   K   @   7   -   .   O    %s is not a folder ! ... About Add file Add folder Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s Auto-remove Backup folders Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Command not found: %s Command: Day(s) Diff Diff Options Disable snapshots when on battery Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Error: Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exclude file Exclude folder Exclude pattern Exit Expert Options Failed to take snapshot %s !!! Finalizing General Global Go To Help Home Hour: If free space is less than: Include Include file Include folder Last week Main profile New profile Now Older than: Options Parameters: Power status not available from system Profile "%s" already exists ! Profile: Profile: "%s" Remove Snapshot Removing old snapshots Rename profile Restore Root Saving config file... Saving permissions... Schedule Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min free space Up Use %1 and %2 for path parameters View the current disk content View the snapshot made at %s Waiting %s second. Waiting %s seconds. Website Week(s) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:12+0000
Last-Translator: Germar <Unknown>
Language-Team: Chinese (Traditional) <zh_TW@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s並不是資料夾！ ... 關於 新增檔案 新增資料夾 確認修改快照資料夾 確認刪除設定檔"%s" 確認要刪除此快照：
%s 自動移除 備份資料夾 無法新增資料夾：%s 找不到crontab檔案！
請確認cron是否安裝？
如果沒有的話，你應該停用所有自動備份。 快照資料夾不存在。
若存在於移動儲存設備，請插入裝置並確認 快照資料夾不存在。
若存在於移動儲存設備，請插入裝置 無法刪除資料夾：%s 不能寫入：%s
請確認是否有寫入權限？ 指令不存在：%s 指令： 天 差異比較 比對選項 使用電池時停用快照功能 關閉 不要移除命名的快照 完成 已完成，不需要進行備份 編輯 開啟通知 錯誤： 每十分鐘 每五分鐘 每天 每月 每週 不包含 排除檔案 排除資料夾 排除樣式 離開 進階選項 快照 %s 執行失敗!!! 關閉光碟中 一般 全域設定 前往 求助 家目錄 時： 剩餘空間少於： 包含 包含檔案 包含資料夾 上週 主要設定檔 新增設定檔 現在 時間超過： 選項 參數： 無法獲得系統電源狀態 "%s"設定檔已經存在！ 設定組合： 設定檔："%s" 刪除快照 刪除快照 重新命名設定檔 還原 主目錄 保留組態檔... 保留存取權限... 排程 設定 快速鍵 顯示隱藏檔案 智慧移除 快照名稱 快照：%s 快照 快照資料夾不存在！ 進行快照 進行快照 本週 今天 嘗試保留最小剩餘空間 向上 使用%1和%2作為路徑參數 檢視目前磁碟內容 檢視於%s完成之快照 等待%s秒。 網站 週 快照存放位置 作業中... 運作中 ： 年 昨天 無法進行快照自我比對 不能包含備份子目錄 不能包含備份資料夾 你不能刪除最後一個設定檔！ 必須選擇一個資料夾進行備份 