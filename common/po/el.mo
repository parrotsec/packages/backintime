��    ?        Y         p     q     �     �     �  
   �  
   �  
   �  
   �  
   �  
   �     �  
   �  
             (     5  e   M  J   �  7   �     6     =     I     e     l     t  
   }     �     �     �     �     �     �     �  	   �     �  
   	  
             &  
   E     P     V     f     s     w     �     �     �     �     �     �     	     	     "	  &   0	     W	     _	     	  '   �	  !   �	  #   �	  /   �	  �  %
  O   �  *        2     6  
   ?  
   J  
   U  
   `  
   k  
   v     �  
   �  
   �  7   �     �  @   �  �   %  �     |   �     ?     N  '   Z     �     �      �     �     �     �          $     ;     O     e     y     �     �     �     �  ;   �          4  +   A     m     �  +   �  =   �  .   �     '     :  "   >  M   a  !   �  !   �     �  W        [  (   r     �  S   �  M   �  M   J  ^   �     0   +         1                 ,   2   	   7       ;                  8   *       &               :   "          #   9       .   !             
   <       '             ?      (              %          4       =   -       )   3                 $   6                       /             >   5                                 
Create a new encrypted folder? %s is not a folder ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 At every boot/reboot Blowfish-CBC Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it. Can't write to: %s
Are you sure you have write access ? Cancel Cast128-CBC Couldn't find UUID for "%s" Day(s) Default Disabled Encryption Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour FAILED Failed to take snapshot %s !!! Finalizing Local Local encrypted Main profile Now Password doesn't match Please confirm password Profile "%s" already exists ! Profile: "%s" SSH SSH encrypted Snapshots folder is not valid ! Take snapshot Taking snapshot WITH ERRORS ! Waiting %s second. Waiting %s seconds. Week(s) When drive get connected (udev) Year(s) You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:25+0000
Last-Translator: Germar <Unknown>
Language-Team: Greek <el@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 
Δημιουργία νέου κρυπτογραφημένου φακέλου; Το %s δεν είναι φάκελος ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Σε κάθε εκκίνηση/επανεκκίνηση Blowfish-CBC Αδύνατη η δημιουργία του φακέλου: %s Δε βρέθηκε crontab.
Είστε σίγουροι ότι το cron είναι εγκατεστημένο ;
Αν όχι, θα πρέπει να απενεργοποιήσετε όλα τα αυτοματοποιημένα back up. Δε βρέθηκε ο φάκελος στιγμιοτύπων.
Εάν βρίσκεται σε αφαιρούμενη συσκευή, παρακαλώ συνδέστε την. Αδύνατη η εγγραφή στο: %s
Είστε σίγουροι ότι έχετε δικαίωμα εγγραφής; Ακύρωση Cast128-CBC Δεν βρέθηκε UUID για  "%s" Ημέρα(ες) Προεπιλογή Απενεργοποιημένο Κρυπτογράφηση Κάθε 10 λεπτά Κάθε 12 ώρες Κάθε 2 ώρες Κάθε 30 λεπτά Κάθε 4 ώρες Κάθε 5 λεπτά Κάθε 6 ώρες Καθημερινά Κάθε μήνα Κάθε εβδομάδα Κάθε ώρα ΑΠΟΤΥΧΙΑ Αποτυχία λήψης στιγμιοτύπου %s !!! Οριστικοποίηση Τοπικό Τοπικό κρυπτογραφημένο Κύριο Προφίλ Τώρα Ο κωδικός δεν ταιριάζει Παρακαλώ επιβεβαιώστε τον κωδικό Το προφίλ "%s" υπάρχει ήδη ! Προφίλ: "%s" SSH SSH κρυπτογραφημένο Ο φάκελος στιγμιοτύπων δεν είναι έγκυρος ! Λήψη στιγμιοτύπου Λήψη στιγμιοτύπου ΜΕ ΛΑΘΗ ! Αναμονή %s δευτερόλεπτο. Αναμονή %s δευτερόλεπτα. Εβδομάδα(ες) Όταν συνδέεται δίσκος Έτος(η) Δεν μπορείτε να συμπεριλάβετε υποφάκελο backup ! Δεν μπορείτε να συμπεριλάβετε φάκελο backup ! Αδύνατη η αφαίρεση του τελευταίου προφίλ ! Πρέπει να διαλέξετε τουλάχιστον ένα φάκελο για backup! 