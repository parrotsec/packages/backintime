��    �        �   
      �     �     �  �   �  �   J     �           '     +  
   4  
   ?  
   J  
   U  
   `  
   k     v  
   ~  
   �     �     �  2   �  2   �  0        5     D     Y     h     u  e   �  [   �  J   O     �  %   �     �  %   �  7        E     L     X     `  q   q  .   �       &   (  T   O      �     �     �  t   �     c     j     r     w  @   �  F   �            
   $     /     6     =     N     ]     k     |     �     �  	   �     �  
   �  
   �  "   �     �     �            
   &     1  ^   8     �     �  �   �     -     G  	   T     ^     d     t     �     �     �     �     �  m   �  .   5     d     |  4   �     �     �     �  m   �  (   k  .   �  "   �      �               .     =     J     R     _     s     �     �     �     �     �     �     �     �  '   �       	   $     .  ,   @     m     z     �  	   �     �     �     �  	   �     �  #   �          /     2     @     R     p  !   �     �  &   �     �     �     �  
             (  	   0  &   :  '   a  !   �  #   �  /   �  S   �     S  �  l  *   �     )   �   8   �   �      �!  )   �!     �!     �!  
   �!  
   	"  
   "  
   "  
   *"  
   5"     @"  
   H"  
   S"     ^"     f"  1   l"  0   �"  -   �"     �"     #     .#     <#     I#     h#  s   �#  q   \$     �$  2   �$      %  1   =%  C   o%     �%     �%  	   �%     �%  �   �%  /   r&     �&  5   �&  [   �&  '   Q'  %   y'     �'  |   �'     3(     ;(     B(     G(  G   U(  N   �(     �(      �(     )  
   )  	   ))     3)     C)     Q)     ^)     n)     {)     �)     �)     �)     �)  
   �)     �)     �)     �)     �)  %   *     (*     6*  [   =*     �*     �*  �   �*     =+  	   X+     b+     q+     w+     �+     �+     �+  	   �+     �+     �+  p   �+  5   \,     �,     �,  :   �,     -     -     -  {   6-  .   �-  G   �-  /   ).  -   Y.     �.     �.     �.     �.     �.     �.     �.     �.     /      /     %/     )/     9/     K/     i/     ~/  &   �/  
   �/     �/  #   �/  (   �/     0     )0     70     D0     M0     k0     z0  
   �0     �0  %   �0  5   �0     �0     �0     1     ,1  "   I1  &   l1     �1     �1  	   �1  	   �1     �1     �1     �1     2     2  E    2  /   f2  +   �2  ,   �2  8   �2  k   (3     �3            }   �          <       \      #   U   {   o                     A   G   ;   v                  "       b   �   �   (   I   /          �      m      >   `           E   )      -   7          0          H   k       L   x   �   @   l   �       :      '   a      4   f   �   |   �       B   �   e   �       �   �   �   d      �      =              w   _           O   �           [       c   R   �           6       W       y   �   C      �           %          K   Y      u   X       2   �   �           &   �       5       1   �                  �          �           P   �   i   +   F          ~              N   �          ^           p   z   �       V   8   �   ?   �          q   h           �                  3              �   M   �   *           ,   .   �   s   g   	       Q       T       J   
   Z   j   9      n   ]   D   S   t   !   r   $    
Create a new encrypted folder?  EXPERIMENTAL! ### This log has been decoded with automatic search pattern
### If some paths are not decoded you can manually decode them with:
 %(user)s is not member of group 'fuse'.
 Run 'sudo adduser %(user)s fuse'. To apply changes logout and login again.
Look at 'man backintime' for further instructions. %s is not a folder ! %s not found in ssh_known_hosts. ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s Ask a question At every boot/reboot Backup folders Blowfish-CBC Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't mount %s Can't mount '%(command)s':

%(error)s Can't remove folder: %s Can't rename %(new_path)s to %(path)s Can't write to: %s
Are you sure you have write access ? Cancel Cast128-CBC Changes Changes & Errors Check commands on host %(host)s returned unknown error:
%(err)s
Look at 'man backintime' for further instructions Cipher %(cipher)s failed for %(host)s:
%(err)s Command not found: %s Config for encrypted folder not found. Could not unlock ssh private key. Wrong password or password not available for cron. Couldn't create remote path:
 %s Couldn't find UUID for "%s" Custom Hours Custom Hours can only be a comma separated list of hours (e.g. 8,12,18,23) or */3 for periodic backups every 3 hours Day(s) Default Diff Disabled Do you really want to delete "%(file)s" in %(count)d snapshots?
 Do you really want to delete "%(file)s" in snapshot "%(snapshot_id)s?
 Done Done, no backup needed Encryption Error: Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Exclude patterns, files or folders Exit FAILED FAQ Failed to take snapshot %s !!! Finalizing Global Hash collision occurred in hash_id %s. Incrementing global value hash_collision and try again. Help Home If you close this window Back In Time will not be able to shutdown your system when the snapshot has finished.
Do you really want to close? Include files and folders Informations Last week Local Local encrypted Main profile Mountprocess lock timeout New profile None Now Password doesn't match Password-less authentication for %(user)s@%(host)s failed. Look at 'man backintime' for further instructions. Ping %s failed. Host is down or wrong address. Please confirm password Profile "%s" already exists ! Profile '%(profile)s': Enter password for %(mode)s:  Profile: Profile: "%s" Refresh snapshots list Remote host %(host)s doesn't support '%(command)s':
%(err)s
Look at 'man backintime' for further instructions Remote host %s doesn't support hardlinks Remote path exists but is not a directory:
 %s Remote path is not executable:
 %s Remote path is not writable:
 %s Remove Snapshot Removing old snapshots Rename profile Report a bug Restore Restore '%s' Restore '%s' to ... Restore permissions: Restore to ... Root SSH SSH encrypted SSH private key Saving config file... Saving permissions... Schedule Schedule udev doesn't work with mode %s Settings Shortcuts Show hidden files Shutdown system after snapshot has finished. Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min %d%% free inodes Trying to keep min free space Up View Last Log View Snapshot Log View the current disk content View the snapshot made at %s WARNING: This can not be revoked! WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) When drive get connected (udev) Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! encfs version 1.7.2 and before has a bug with option --reverse. Please update encfs mountpoint %s not empty. Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-10-30 18:01+0000
Last-Translator: Germar <Unknown>
Language-Team: Indonesian <id@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 
Buat sebuah folder terenkripsi yang baru?  EXPERIMENTAL! ### Catatan ini telah di decode dengan pencarian pola otomatis
### Jika ada beberapa jalur yang tidak didecode anda dapat mendecodenya secara manual menggunakan:
 %(user)s bukan merupakan anggota dari grup 'fuse'.
 Jalankan 'sudo adduser %(user)s fuse'. Untuk menerapkan perubahan silahkan logout dan login kembali.
Lihat pada 'man backintime' untuk instruksi lebih lanjut. %s bukan sebuah folder ! %s tidak ditemukan dalam ssh_known_hosts. ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Tentang Semua Apakah anda yakin ingin mengubah folder snapshot? Apakah anda yakin untuk menghapus profile "%s" ? Apakah anda yakin untuk menghapus snapsot:
%s Ajukan sebuah pertanyaan Pada setiap boot/reboot Folder Backup Blowfish-CBC Tidak mampu membuat folder: %s Tidak dapat menemukan crontab.
Apakah anda yakin cron sudah terpasang ?
Jika tidak, anda harus mematikan semua backup otomatis. Tidak dapat menemukan folder snapshot.
Jika berada pada perangkat USB atau lainnya, silahkan tancapkan dan tekan OK Tidak dapat menemukan folder snapshot.
Jika ada di hardisk eksternal atau perangkat lainnya silahkan ditancapkan. Tidak dapat mengkaitkan %s Tidak mampu untuk memuat '%(command)s':

%(error)s Tidak mampu menghapus folder: %s Tidak bisa mengubah nama %(new_path)s ke %(path)s Tidak dapat menulis: %s
Apakah anda yakin memiliki hak akses tulis? Batal Cast128-CBC Perubahan Perubahan dan Kesalahan Periksa perintah-perintah pada host %(host)s yang menyebabkan kesalahan:
%(err)s
Lihat pada 'man backintime' untuk penjelasan lebih lanjut Cipher %(cipher)s gagal untuk %(host)s:
%(err)s Perintah tidak ditemukan: %s Konfig untuk folder yang terenkripsi tidak ditemukan. Tidak dapat membuka kunci privat SSH. Kata sandi yang salah atau tidak tersedia untuk cron. Tidak mampu membuat jalur pemantau:
 %s Tidak mampu menemukan UUID untuk "%s" Menentukan Jam Sendiri Custom hour hanya dapat menjadi koma pemisah dalam daftar jam (misal 8,12,18,23) atau */3 untuk backup periodik setiap 3 jam Hari(s) Bawaan Beda Dinonaktifkan Apakah anda yakin ingin menghapus "%(file)s" dalam %(count)d snapshot?
 Apakah anda yakin ingin menghapus "%(file)s" dalam snapshot "%(snapshot_id)s?
 Selesai Selesai, backup tidak diperlukan Enkripsi Kesalahan: Kesalahan Setiap 10 menit Setiap 12 jam Setiap 2 jam Setiap 30 menit Setiap 4 jam Setiap 5 menit Setiap 6 jam Setiap Hari Setiap Bulan Setiap Minggu Setiap jam Diluar pola, berkas atau folder Keluar GAGAL Tanya Jawab Gagal untuk mengambil snapshot %s !!! Menyelesaikan Global Tabrakan hash muncul pada hash_id %s. Naikkan nilai global hash_collision dan cobalah lagi. Bantuan Rumah Jika anda menutup jendela ini Back In Time tidak akan mampu mematikan sistem anda ketika snapshot selesai dibuat.
Apakah anda yakin ingin menutupnya? Termasuk berkas dan folder Informasi Minggu kemarin Lokal Lokal terenkripsi Profil utama Kunci proses mount telah habis Profil baru Tidak ada Saat Ini Kata sandi tidak cocok Otentikasi tak bersandi untuk %(user)s@%(host)s gagal. Lihat pada 'man backintime' untuk instruksi lebih lanjut. Ping %s gagal. Host sedang down atau alamatnya salah. Mohon kata sandi dikonfirmasi Profil "%s" sudah terpakai ! Profil '%(profile)s': Masukkan kata sandi untuk %(mode)s:  Profil: Profil: "%s" Segarkan daftar snapshot Host yang dipantau %(host)s tidak mendukung '%(command)s':
%(err)s
Lihat pada 'man backintime' untuk instruksi lebih lanjut Host %s yang dipantau tidak mendukung hardlink Jalur yang dipantau tersedia tapi sayangnya bukan sebuah direktori:
 %s Jalur yang dipantau tidak dapat dieksekusi:
 %s Jalur yang dipantau tidak dapat ditulisi:
 %s Singkirkan Snapshot Hapus snapshot lama Ubah nama profil Laporkan bug Pulihkan Memulihkan '%s' Pulihan '%s' ke ... Kembalikan hak akses: Pulihkan ke ... Root SSH SSH terenkripsi Kunci private SSH Simpan berkas konfigurasi ... Simpan hak akses ... Penjadwalan Jadwal udev tidak bekerja pada mode %s Pengaturan Jalan Pintas Tampilkan berkas-berkas tersembunyi Matikan sistem setelah snapshot selesai. Penghapusan pintar Nama Snapshot Snapshot: %s Snapshot Folder snapshot tidak valid ! Ambil snapshot Ambil snapshot Minggu Ini Hari ini Coba untuk menyimpan sisa inodes %d%% Coba untuk selalu menyimpan sisa partisi yang minimal Naik Lihat Catatan Terakhir Lihat Catatan Snapshot Lihat isi disk yang sekarang Lihat snapshot yang dibuat pada %s PERINGATAN: Ini tidak bisa dibatalkan! DENGAN KESALAHAN ! Menunggu %s detik. Situs Web Minggu(s) Ketika drive dihubungkan (udev) Sedang Bekerja... Sedang Bekerja: Tahun(s) Kemarin Anda tidak dapat membandingkan sebuah snapshot dengan dirinya sendiri Anda jangan memasukkan juga sub-folder backup ! Anda jangan memasukkan juga folder backup ! Anda tidak dapat menghapus profil terakhir ! Anda harus memilih setidaknya satu folder untuk backup ! versi encfs 1.7.2 dan setelahnya mempunyai bug ketika menggunakan opsi --reverse. Silahkan perbaharui encfs mountpoint %s tidak kosong. 