��    �      �  �   �	           	     )  �   8  �   �     a     v     z  
   �  
   �  
   �  
   �  
   �  
   �     �  
   �  
   �     �     �  
   �  2   �  2   0  0   c     �     �     �     �     �  e   �  [   O  J   �     �  %        +  %   C  7   i     �     �     �     �  &   �  T   �     O     k     x          �     �  !   �     �     �     �     �     �       
        "     )     0     A     P     ^     o     }     �  	   �     �  
   �  
   �     �     �     �     �     �                  
   5     @     H     O  ^   U     �     �     �     �     �  	   �     �                    8     D     H     T     \     h  m        �  &        ,  4   J          �     �     �     �     �     �     �     �     �     �               3     I  '   R     z  	   �     �     �     �     �  	   �     �     �     �  	          #        C     a  !   d     �     �     �  &   �     �     �          &  
   >     I     R  	   Z  &   d  '   �  !   �  #   �  /   �  S   )     }  �  �     )     E  �   T  �   �     �     �     �  
   �  
   �  
   �  
   �  
   �  
   �     �  
   �  
   �                 ?   %  1   e  <   �     �  
   �     �            �   8  l   �  a   .      �   -   �      �   *   �   E   !  	   `!     j!     v!  	   �!  '   �!  m   �!     /"     H"     W"     ^"     g"     l"  0   "     �"  '   �"     �"  -   �"     #     ##  
   =#     H#     N#     S#     c#     q#     ~#     �#     �#     �#     �#     �#     �#  	   �#  
   �#     �#     �#     $     $     &$     ?$  (   K$  	   t$     ~$     �$     �$  \   �$     �$     �$     %  	   %%     /%  
   @%     K%     Q%     a%  #   m%  	   �%     �%     �%     �%  
   �%     �%  [   �%     3&  )   M&     w&  4   �&     �&     �&     �&     �&     '     $'     6'     ='     Q'     V'     Z'     h'     z'     �'     �'  -   �'     �'     �'     �'     �'     (     "(     8(      K(     l(     �(  	   �(     �(  (   �(      �(     �(     �(     )  "   6)  	   Y)  %   c)     �)     �)  "   �)  #   �)     �)  	   �)     �)     �)  9    *  5   :*  3   p*  &   �*  >   �*  b   
+      m+     &   C   7   }         D   G                  h   =   �   �   �   l   y   <   r   T   �           |       '              U   s   "       �      2   �       �      :             M           R               �         x      f       6       �                            q                   Z   {   �       *   3       .   P   �           o       W   N   �   
       p   c         �   �   H          L       K   V   ,      E   #   4          +      J   >           (   A   n   0   !       w   $   g   @   �           �   1       /   	   i   z   �                   5   m       e       %   Y   X   _   ;   j   a   8       b         v   ]      u   �   \   �      ?   O   B   �   ^       d       I              F       �   �   Q          �   �   t   ~       [       9           S   )   �      -   �   `   k      �       
Create a new encrypted folder?  EXPERIMENTAL! ### This log has been decoded with automatic search pattern
### If some paths are not decoded you can manually decode them with:
 %(user)s is not member of group 'fuse'.
 Run 'sudo adduser %(user)s fuse'. To apply changes logout and login again.
Look at 'man backintime' for further instructions. %s is not a folder ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add file Add folder Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s At every boot/reboot Auto-remove Backup folders Blowfish-CBC Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't mount %s Can't mount '%(command)s':

%(error)s Can't remove folder: %s Can't rename %(new_path)s to %(path)s Can't write to: %s
Are you sure you have write access ? Cancel Cast128-CBC Command not found: %s Command: Config for encrypted folder not found. Could not unlock ssh private key. Wrong password or password not available for cron. Couldn't find UUID for "%s" Custom Hours Day(s) Default Diff Diff Options Disable snapshots when on battery Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Encryption Error: Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Exclude Exclude file Exclude folder Exclude pattern Exit Expert Options FAILED Failed to take snapshot %s !!! Finalizing General Global Go To Hash collision occurred in hash_id %s. Incrementing global value hash_collision and try again. Help Home If free space is less than: Include Include folder Last week Local Local encrypted Main profile Mountprocess lock timeout New profile Now Older than: Options Parameters: Password doesn't match Password-less authentication for %(user)s@%(host)s failed. Look at 'man backintime' for further instructions. Please confirm password Power status not available from system Profile "%s" already exists ! Profile '%(profile)s': Enter password for %(mode)s:  Profile: Profile: "%s" Remove Snapshot Removing old snapshots Rename profile Report a bug Restore Restore permissions: Root SSH SSH encrypted SSH private key Saving config file... Saving permissions... Schedule Schedule udev doesn't work with mode %s Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min %d%% free inodes Trying to keep min free space Up Use %1 and %2 for path parameters View the current disk content View the snapshot made at %s WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) When drive get connected (udev) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! encfs version 1.7.2 and before has a bug with option --reverse. Please update encfs mountpoint %s not empty. Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:25+0000
Last-Translator: Germar <Unknown>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 
Lav en ny krypteret mappe?  EXPERIMENTAL! ### Denne log er blevet afkodet med automatisk søge mønster
### Hvis nogle stier ikke er afkodet kan du manuelt afkode dem med:
 %(user)s er ikke medlem af gruppe 'fuse'.
 Kør 'sudo adduser %(user)s fuse'. Bekræft ændringerne ved at logge ud og ind igen.
Se 'man backintime' for yderlig information. %s er ikke en mappe ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Om Tilføj fil Tilføj mappe Er du sikker på du ønsker at skifte tilstands-billede-mappe ? Er du sikker på at du vil slette profilen "%s" ? Er du sikker på du ønsker at fjerne tilstands-billedet:
%s Ved hver opstart/genstart Auto-fjern Sikkerhedskopi-mapper Blowfish-CBC Kan ikke oprette mappen: %s Kan ikke finde crontab.
Er du sikker på, at cron er installeret ?
Hvis ikke, skal du deaktivere alle automatiske sikkerhedskopieringer. Kan ikke finde tilstands-billede-mappen.
Hvis den er på et flytbar drev, indsæt venligst drevet og tryk OK Kan ikke finde tilstands-billede-mappen.
Hvis den er på et flytbart drev indsæt venligst dette. Kan ikke tilslutte %s Kan ikke tilslutte  '%(command)s':

%(error)s Kan ikke fjerne mappen: %s Kan ikke omdøbe %(new_path)s til %(path)s Kan ikke skrive til: %s
Er du sikker på, du har skrive-rettigheder ? Annullér Cast128-CBC Kommando ikke fundet: %s Kommando: Konfig for krypteret mappe ikke fundet. Kunne ikke fjerne låsen på ssh privat nøgle. Forkert kodeord elle kodeordet er ikke tilgængelig for cron. Fandt ikke UUID for "%s" Angiv interval Dag(e) Standard Diff Diff-indstillinger De-aktivér tilstands-billeder ved batteri-drift Deaktiveret Fjern ikke navngivne tilstands-billeder Færdig Færdig, ingen sikkerhedskopiering nødvendig Redigér Aktivér bekendtgørelser Kryptering Fejl: Fejl Hvert 10. minut Hver 12. time Hver 2. time Hvert 30. minut Hver 4. time Hver 5 minutter Hver 6. time Hver dag Hver måned Hver uge Hver time Ekskludér Ekskludér fil Ekskludér mappe Ekskludér mønster Afslut Avancerede indstillinger MISLYKKEDES Kunne ikke tage tilstands-billede %s !!! Afslutter Generelt Globalt Gå til Hash kollision skete i hash_id %s. Forøg den globale værdi hash_collision og forsøg igen. Hjælp Hjemmemappe Hvis fri plads er mindre end: Inkludér Inkludér mapper Sidste uge Lokal Lokal krypteret hovedprofil Lås af tilslutningsproces udløbet Ny profil Nu Ældre end: Indstillinger Parametre: Kodeord passer ikke Kodeløs login for %(user)s@%(host)s fejlede. Se'man backintime' for yderlig informationer. Bekræft venligst kodeord Strømstatus ikke tilgængelig fra system Profil "%s" findes allerede ! Profil '%(profile)s': Indtast kodeord for %(mode)s:  Profil: Profil: "%s" Fjern tilstands-billede Fjern gamle tilstands-billeder Omdøb profil Rapporter en fejl Gendan Gendan tilladelser: Root SSH SSH krypteret SSH privat nøgle Gem config fil ... Gem tilladelse ... Planlæg Skedulere udev virker ikke med indstilling %s Indstillinger Genveje Vis skjulte filer Smart fjern Tilstands-billede-navn Tilstands-billede: %s Tilstamds-billeder Snapshots mappe er ikke gyldig ! Tag et tilstands-billede Tag et tilstands-billede Denne uge I dag Prøv at beholde minimum %d%% inoder fri Prøv at holde minimum fri plads Op Brug %1 og %2 som sti-parametre Se nuværende disk-indhold Se tilstands-billedet lavet ved %s MED FEJL! Venter %s sekund. Venter %s sekunder. Websted Uge(r) Når drev bliver tilsluttet (udev) Hvor skal tilstands-billeder gemmes Arbejder... Arbejder: År I går Du kan ikke sammenligne et tilstands-billede med sig selv Du kan ikke inkludere en sikkerhedskopierings-mappe ! Du kan ikke inkludere sikkerhedskopierings-mappen ! Du kan ikke fjerne den sidste profil ! Du skal vælge mindst én mappe, der skal sikkerhedskopieres ! encfs version 1.7.2 og tidligere har en fejl ved brug af option --reverse. Opdatér venligst encfs tilsluttelsespunkt %s ikke tomt. 