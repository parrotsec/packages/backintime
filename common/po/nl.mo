��    \     �
  �  �      (     )     I     X  "   `  �   �  �     R   �  ;   �  �   !     �      �     �        �      �   �   
   c!  
   n!  
   y!  
   �!  
   �!  
   �!     �!  
   �!  
   �!     �!     �!     �!     �!  
   �!     �!     "     "     &"     /"  2   3"  2   f"  >   �"  H   �"  0   !#     R#     a#     v#     ~#     �#  M   �#      �#     $  @   $     V$  e   n$  [   �$  J   0%     {%  %   �%     �%  %   �%  *   �%  7   &     Q&     X&  A   d&  	   �&     �&     �&  q   �&     ;'  3   Z'  .   �'     �'     �'     �'     �'  &   �'  .   (     K(  '   P(  a   x(  2   �(  j   )  T   x)      �)     �)  &   
*     1*  t   >*     �*     �*     �*  $   �*     �*  !   �*     +     +  �   �+  �    ,     �,     �,  !   �,     �,  @   -  F   H-  G   �-  ,   �-     .     !.     &.     =.     B.     G.     Z.  
   o.     z.     �.     �.     �.     �.     �.     �.     �.     �.     �.  	   �.     /  
   /  
   /     %/     ,/  #   4/     X/    e/     w0     �0     �0  "   �0     �0     �0     �0     �0  (   �0     1     71     Q1     p1     �1  
   �1     �1  +   �1     �1  i  �1     R3     Z3     a3  ^   g3     �3     �3     �3  '   �3     4     4     4      4     '4     D4  �   `4     �4     �4     5     5     *5     75  &   W5  (   ~5  '   �5  (   �5     �5     6     6  	   6     '6     /6     M6     k6     �6     �6  
   �6     �6     �6     �6     �6     �6     �6  �   7     �7     �7  *   �7     �7     �7     �7  F   �7  �   A8     9     9     9  m   09  !   �9     �9     �9  .   �9     :  m  $:  T   �;     �;  &   �;  �   <     �<  $   =     +=  +   8=     d=  4   �=     �=     �=     �=  )   �=  )   >     +>  m   B>  (   �>  .   �>  "   ?      +?     L?     S?  %   c?  +   �?     �?     �?     �?     �?     �?     @     @     &@     5@     F@  �   [@  �   �@  L   �A  S   B  ;   gB  B   �B     �B     �B     C     C      C     ,C  y   HC  U   �C  !   D     :D     ZD     ^D     kD     yD     �D     �D     �D     �D  '   �D  
   �D     
E     E  2   E  	   LE     VE     dE     vE  ,   E     �E     �E     �E     �E     �E  	   �E     �E     F      F     1F  <   GF     �F     �F     �F  Z   �F  ;   G  :   VG  	   �G     �G     �G  #   �G     �G     �G  !   �G     H     4H     :H     ?H     MH     _H     }H  !   �H  K   �H     I  &   I  w   =I  `   �I     J     J     &J     /J     OJ  
   gJ     rJ  \   {J     �J  	   �J  &   �J  '   K  !   9K  #   [K  v   K  /   �K  &   &L     ML     YL     `L     mL     uL     ~L  S   �L     �L     �L     �L     M  5   M     RM     [M  �  yM  "   O     6O     FO  2   NO  �   �O  �   OP  O   �P  ;   0Q  �   lQ     gR  4   wR     �R     �R  �   �R  �   �S  
   _T  
   jT  
   uT  
   �T  
   �T  
   �T     �T  
   �T  
   �T     �T  	   �T     �T     �T     �T  (   U     1U     MU     fU     rU  4   xU  6   �U  J   �U  V   /V  @   �V     �V     �V  
   �V     �V  #   W  }   8W  =   �W     �W  W   X     YX  �   xX  u   Y  a   �Y     �Y  /   Z  !   =Z  -   _Z  0   �Z  3   �Z  	   �Z     �Z  6   [     ?[     Q[     ][  �   r[  2   
\  R   =\  2   �\  	   �\     �\  	   �\     �\  1   ]  8   =]  	   v]  0   �]  �   �]  ;   6^  r   r^  e   �^  %   K_  5   q_  2   �_     �_  �   �_     �`     �`  	   �`  ,   �`     �`  3   �`     a  �   a  �   �a  �   Vb     c     .c  5   >c     tc  N   �c  R   �c  Z   $d  <   d  *   �d     �d     �d     e     'e     0e     Ge     [e     ie     ne     te     {e     �e     �e     �e     �e     �e     �e     �e  
   �e  	   �e     �e     f  
   f  0   f  ;   Cf  e  f  *   �g  7   h     Hh  (   _h  	   �h     �h     �h     �h  0   �h  (   �h  *   i  !   7i  "   Yi     |i  	   �i     �i  1   �i     �i  �  �i     �k     �k  	   �k  �   �k     tl     yl     �l  5   �l     �l     �l     �l     �l  (   �l  &   m  �   Cm     �m     �m     �m     n  
   &n  ,   1n  2   ^n  4   �n  3   �n  1   �n     ,o     ;o     So     go     vo  %   o  ,   �o  .   �o     p     p     p     *p     7p  ,   >p  	   kp  %   up     �p  �   �p     fq     �q  N   �q     �q  
   �q     �q  [   �q  �   Xr     Es  
   Qs     \s  �   ys     t     $t     )t  ]   It     �t  �  �t  Y   yv     �v  8   �v  5  w     Ix  &   Ux     |x  &   �x     �x  9   �x     y     y     y  +   "y  +   Ny     zy  �   �y  7   z  ,   Vz  '   �z  )   �z     �z     �z  5   �z  8   0{      i{     �{     �{     �{  
   �{     �{     �{     �{     |     '|    ;|  +  S}  H   ~  R   �~  A     K   ]     �      �     �     �     �      �  �   -�  �   ΀  7   R�  -   ��     ��     ��     ́     ݁  #   �     �     2�     E�  %   N�     t�  
   ��     ��  4   ��     ҂     �     ��     �  :    �     [�     l�     y�     ��     ��     ��     ȃ  	   �     �     �  ;   %�     a�  "   t�     ��  o   ��  ?   �  J   Z�  	   ��     ��     ��  3   Å  .   ��     &�  %   -�  1   S�  
   ��     ��     ��     ��     ̆  #   �  3   �  c   C�     ��  "   ��  �   և  �   ��     �  
   �     $�  5   5�  &   k�     ��  
   ��  �   ��  
   1�     <�  5   E�  4   {�  )   ��  ,   ڊ  �   �  C   ��  '   ؋      �     �     �     #�     3�     A�  \   N�  	   ��     ��     Ό     ��  :   ��  
   6�  -   A�       �   �   �   U              1              �     Z  �   N   @  h       �   �   S       �       4      -   _     U      p   t   W   V   9  �   [          �               !  i       H   �   "   V      �   M   �   |           O      �      �   x   Q         ?           �       �             )      �   %   �   k       J       �   �   +  �     �   �           O                           0         �   7        <             �     <   I  '      �          �       �   E  1   `   �       �     �   �   �   �   �   :  >  �           �   �   ^             A   �     D          �       �   �   j           T   M  n      N  �              2        D          l   \   %      �   z      �       5   �     {           �   (  K  '   W      �       ?  �                  �   �   y        �   v     q           Z   �   �   �   2  /  ,   *   8   R          #  �   �                   �       �   J  �       �       G  �   �       �   �       ,  >              #           3  $   7          8         �   4   F   L   �      I       ;                 �   w   P   o   �       r   B  �   @   �      �   }   
  C       )   �                  f   0   �   T          d           �   g   �       a   	     -  �   �   �   �   C  &       �   H      A  �   S      �                   �   Y  /   u   s      �   �   �   �       G       �   L  �   �   �      :   P  �   �       �   �       (   K      �       �   �   Q  �   �   c   �   �      �   6   .  	      �   �   �   F  �   =   �   m   �           X   �   �   3       �       \  "  �   5  R  $     �   .      �       ~   &  �   +   �   =  ;       X  �                         *      �   �   �   b           [   !   �   �   9   6     �   �   
   �         e     E   Y       ]   B        
Create a new encrypted folder?  EXPERIMENTAL!  KB/sec  and add your user to group 'fuse' "%s" is a symlink. The linked target will not be backed up until you include it, too.
Would you like to include the symlinks target instead? ### This log has been decoded with automatic search pattern
### If some paths are not decoded you can manually decode them with:
 %(appName)s is not configured. Would you like to restore a previous configuration? %(proc)s not found. Please install e.g. %(install_command)s %(user)s is not member of group 'fuse'.
 Run 'sudo adduser %(user)s fuse'. To apply changes logout and login again.
Look at 'man backintime' for further instructions. %s is not a folder ! %s not found in ssh_known_hosts. ... 3DES-CBC <b>Warning:</b> %(app)s uses EncFS for encryption. A recent security audit revealed several possible attack vectors for this. Please take a look at 'A NOTE ON SECURITY' in 'man backintime'. <b>Warning:</b> Wildcards ('foo*', '[fF]oo', 'fo?') will be ignored with mode 'SSH encrypted'.
Only separate asterisk are allowed ('foo/*', 'foo/**/bar') AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add Add default Add file Add folder Add prefix to SSH commands Add to Exclude Add to Include Advanced All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove all newer files in '%(path)s'? Are you sure you want to remove all newer files in your original folder? Are you sure you want to remove the snapshot:
%s Ask a question At every boot/reboot Authors Auto-remove Backup folders Backup local files before overwriting or
removing with trailing '%(suffix)s'. Backup replaced files on restore Blowfish-CBC Cache Password for Cron (Security issue: root can read password) Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't mount %s Can't mount '%(command)s':

%(error)s Can't remove folder: %s Can't rename %(new_path)s to %(path)s Can't unmount %(proc)s from %(mountpoint)s Can't write to: %s
Are you sure you have write access ? Cancel Cast128-CBC Change these options only if you really know what you are doing ! Changelog Changes Changes & Errors Check commands on host %(host)s returned unknown error:
%(err)s
Look at 'man backintime' for further instructions Check if remote host is online Check if remote host support all necessary commands Cipher %(cipher)s failed for %(host)s:
%(err)s Cipher: Command not found: %s Command: Config File Help Config for encrypted folder not found. Continue on errors (keep incomplete snapshots) Copy Copy links (dereference symbolic links) Copy public ssh-key "%(pubkey)s" to remote host "%(host)s".
Please enter password for "%(user)s": Copy unsafe links (works only with absolute links) Could not install Udev rule for profile %(profile_id)s. DBus Service '%(dbus_interface)s' wasn't available Could not unlock ssh private key. Wrong password or password not available for cron. Couldn't create remote path:
 %s Couldn't find UUID for "%s" Create a new SSH key without Password. Custom Hours Custom Hours can only be a comma separated list of hours (e.g. 8,12,18,23) or */3 for periodic backups every 3 hours Day(s) Day: Decode Deep check (more accurate, but slow) Default Deferring backup while on battery Delete Destination filesystem for '%(path)s is a sshfs mounted share. sshfs doesn't support hard-links. Please use mode 'SSH' instead. Destination filesystem for '%(path)s' is formatted with FAT which doesn't support hard-links. Please use a native Linux filesystem. Destination filsystem for '%(path)s' is a SMB mounted share. Please make sure the remote SMB server supports symlinks or activate '%(copyLinks)s' in '%(expertOptions)s'. Diff Diff Options Disable snapshots when on battery Disabled Do you really want to delete "%(file)s" in %(count)d snapshots?
 Do you really want to delete "%(file)s" in snapshot "%(snapshot_id)s?
 Do you really want to restore this files(s)
into new folder '%(path)s': Do you really want to restore this files(s): Don't remove named snapshots Done Done, no backup needed ETA: Edit Edit user-callback Enable notifications Encryption Error Error: Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Every: Exclude Exclude "%s" from future snapshots? Exclude file Exclude files bigger than value in %(prefix)s.
With 'Full rsync mode' disabled this will only affect new files
because for rsync this is a transfer option, not an exclude option.
So big files that has been backed up before will remain in snapshots
even if they had changed. Exclude files bigger than:  Exclude folder Exclude pattern Exclude patterns, files or folders Exit Expert Options FAILED FAQ Failed to create new SSH key in %(path)s Failed to load config: %s Failed to save config: %s Failed to take snapshot %s !!! Failed to write new crontab. Filter: Finalizing Folder Found leftover '%s' which can be continued. Full snapshot path:  Full system backup can only create a snapshot to be restored to the same physical disk(s) with the same disk partitioning as from the source; restoring to new physical disks or the same disks with different partitioning will yield a potentially broken and unusable system.

Full system backup will override some settings that may have been customized. Continue? General Global Go To Hash collision occurred in hash_id %s. Incrementing global value hash_collision and try again. Help Highly recommended: Home Host/User/Profile-ID must not be empty! Host: Hour(s) Hour: Hours: If free inodes is less than: If free space is less than: If you close this window Back In Time will not be able to shutdown your system when the snapshot has finished.
Do you really want to close? Include Include file Include files and folders Include folder Informations Keep all snapshots for the last Keep one snapshot per day for the last Keep one snapshot per month for the last Keep one snapshot per week for the last Keep one snapshot per year for all years Key File Last Log View Last check %s Last week License Limit rsync bandwidth usage:  List only different snapshots List only equal snapshots to:  Local Local encrypted Log Level: Main profile Mode: Modify for Full System Backup Month(s) Mountprocess lock timeout New profile Newer versions of files will be renamed with trailing '%(suffix)s' before restoring.
If you don't need them anymore you can remove them with '%(cmd)s' No config found None Nothing changed, no new snapshot necessary Now Older than: Options Options must be quoted e.g. --exclude-from="/path/to/my exclude file". Other snapshots will be blocked until the current snapshot is done.
This is a global option. So it will effect all profiles for this user.
But you need to activate this for all other users, too. Parameters: Password Password doesn't match Password-less authentication for %(user)s@%(host)s failed. Look at 'man backintime' for further instructions. Paste additional options to rsync Path: Pause snapshot process Ping %s failed. Host is down or wrong address. Please confirm password Please navigate to the snapshot from which you want to restore %(appName)s's configuration. The path may look like: 
%(samplePath)s

If your snapshots are on a remote drive or if they are encrypted you need to manually mount them first. If you use Mode SSH you also may need to set up public key login to the remote host%(addFuse)s.
Take a look at 'man backintime'. Please verify this fingerprint! Would you like to add it to your 'known_hosts' file? Port: Power status not available from system Prefix to run before every command on remote host.
Variables need to be escaped with \$FOO.
This doesn't touch rsync. So to add a prefix
for rsync use "%(cbRsyncOptions)s" with
%(rsync_options_value)s

%(default)s: %(def_value)s Preserve ACL Preserve extended attributes (xattr) Private Key: Private key file "%(file)s" does not exist. Profile "%s" already exists ! Profile '%(profile)s': Enter password for %(mode)s:  Profile: Profile: "%s" Question Redirect stderr to /dev/null in cronjobs. Redirect stdout to /dev/null in cronjobs. Refresh snapshots list Remote host %(host)s doesn't support '%(command)s':
%(err)s
Look at 'man backintime' for further instructions Remote host %s doesn't support hardlinks Remote path exists but is not a directory:
 %s Remote path is not executable:
 %s Remote path is not writable:
 %s Remove Remove Snapshot Remove newer files in original folder Removing leftover '%s' folder from last run Removing old snapshots Rename profile Repeatedly (anacron) Report a bug Restore Restore '%s' Restore '%s' to ... Restore Config Restore Settings Restore permissions: Restore selected file or folder.
If this button is grayed out this is most likely because "%(now)s" is selected in left hand snapshots list. Restore selected files or folders to the original destination and
delete files/folders which are not in the snapshot.
This will delete files/folders which where excluded during taking the snapshot!
Be extremely careful!!! Restore the currently shown folder and all its content to a new destination. Restore the currently shown folder and all its content to the original destination. Restore the selected files or folders to a new destination. Restore the selected files or folders to the original destination. Restore to ... Resume snapshot process Root Run 'ionice': Run 'nice': Run 'rsync' with 'nocache': Run Back In Time as soon as the drive is connected (only once every X days).
You will be prompted for your sudo password. Run Back In Time repeatedly. This is useful if the computer is not running regularly. Run in background on remote Host. Run only one snapshot at a time SSH SSH Settings SSH encrypted SSH private key Save Password to Keyring Saving config file... Saving permissions... Schedule Schedule udev doesn't work with mode %s Select All Sent: Settings Shebang in user-callback script is not executable. Shortcuts Show full Log Show hidden files Shutdown Shutdown system after snapshot has finished. Smart remove Snapshot Snapshot Log View Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Speed: Start BackInTime Stop snapshot process Take a new snapshot regardless of there were changes or not. Take snapshot Take snapshot with checksums Taking snapshot The authenticity of host "%(host)s" can't be established.

%(keytype)s key fingerprint is: This folder doesn't exist
in the current selected snapshot! This is NOT a snapshot but a live view of your local files This week Today Translations Trying to keep min %d%% free inodes Trying to keep min free space Up Use %1 and %2 for path parameters Use checksum to detect changes User: View View Last Log View Snapshot Log View the current disk content View the snapshot made at %s WARNING: This can not be revoked! WARNING: deleting files in filesystem root could break your whole system!!! WITH ERRORS ! Waiting %s second. Waiting %s seconds. Warning: if disabled and the remote host
does not support all necessary commands,
this could lead to some weird errors. Warning: if disabled and the remote host
is not available, this could lead to some
weird errors. Website Week(s) Weekday: When drive get connected (udev) Where to save snapshots Working... Working: Would you like to copy your public SSH key to the
remote host to enable password-less login? Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You did not choose a private key file for SSH.
Would you like to generate a new password-less public/private key pair? You must select at least one folder to backup ! [E] Error, [I] Information, [C] Change as cron job day(s) decode paths default disabled enabled encfs version 1.7.2 and before has a bug with option --reverse. Please update encfs month(s) mountpoint %s not empty. on local machine on remote host user-callback script has no shebang (#!/bin/sh) line. weeks(s) when taking a manual snapshot Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-05-13 20:27+0000
Last-Translator: rob <linuxned@gmail.com>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 
Nieuwe versleutelde map aanmaken?  EXPERIMENTEEL!  KB/sec  en uw gebruiker toe te voegen aan de groep 'fuse' "%s" is een symbolische link. Er zal geen reservekopie gemaakt worden van het gekoppelde doel totdat u het doel zelf ook opneemt.
Wilt u i.p.v. de symbolische link het doel zelf opnemen in de reservekopie? ### Dit logboek is gedecodeerd met automatisch zoekpatroon
### Als sommige paden niet gedecodeerd zijn dan kunt u deze handmatig decoderen met:
 %(appName)s is niet geconfigureerd. Wilt u een eerdere configuratie herstellen? %(proc)s niet gevonden. Installeer b.v. %(install_command)s %(user)s is geen lid van de groep 'fuse'.
 Voer de opdracht 'sudo adduser %(user)s fuse' uit. Om de wijzigingen toe te passen dient u zich eerst af te melden en daarna weer aan te melden.
Bekijk de 'Back In Time-handleiding' voor verdere instructies. %s is geen map! %s is niet gevonden in de ssh_bekende_gastcomputers. ... 3DES-CBC <b>Waarschuwing:</b> %(app)s gebruikt EncFS voor versleuteling. Na een recente veiligheidscontrole is gebleken dat er diverse kwetsbaarheden in EncFS zitten. Bekijk a.u.b. de 'Back In Time-handleiding' over 'A NOTE ON SECURITY'. <b>Waarschuwing:</b> Wildcards ('foo*', '[fF]oo', 'fo?') zullen genegeerd worden in de modus 'SSH-encrypted'.
Alleen ster-tekens (*) die gescheiden zijn zijn toegestaan ('foo/*', 'foo/**/bar') AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Info Toevoegen Standaardwaarde toevoegen Bestand toevoegen Map toevoegen Voorvoegsel aan SSH-opdrachten toevoegen Toevoegen om uit te sluiten Toevoegen om op te nemen Geavanceerd Alles Weet u zeker dat u de reservekopiemap wilt wijzigen? Weet u zeker dat u het profiel "%s" wilt verwijderen ? Weet u zeker dat u alle nieuwere bestanden in '%(path)s' wilt verwijderen? Weet u zeker dat u alle nieuwere bestanden in de oorspronkelijke map wilt verwijderen? Weet u zeker dat u de volgende reservekopie wilt verwijderen?
%s Een vraag stellen Bij elke start/herstart Schrijvers Automatisch verwijderen Mappen om reservekopie van te maken Maak reservekopie van lokale bestanden voordat u ze overschrijft of
verwijdert d.m.v. het plaatsen van '%(suffix)s' erachter. Reservekopie maken van vervangen bestanden bij het herstellen Blowfish-CBC Wachtwoordbuffergeheugen voor Cron (Veiligheidskwestie: beheerder kan wachtwoord lezen) Kan deze map niet aanmaken: %s Kan crontab niet vinden.
Weet u zeker dat cron geïnstalleerd is?
Indien dit niet het geval is, dient u het maken van automatische reservekopieën uit te schakelen. Kan reservekopiemap niet vinden.
Als deze op een extern medium staat, gelieve dit aan te sluiten en op OK te drukken. Kan reservekopiemap niet vinden.
Als deze op een extern medium staat, gelieve dit aan te sluiten. Kan %s niet aankoppelen Kan '%(command)s' niet aankoppelen :

%(error)s Kan deze map niet verwijderen: %s Kan %(new_path)s niet hernoemen naar %(path)s Kon %(proc)s niet ontkoppelen van %(mountpoint)s Kan niet schrijven naar: %s
Heeft u schrijfrechten? Annuleren Cast128-CBC Wijzig deze opties alleen als u zeker weet wat u doet! Wijzigingslogboek Wijzigingen Wijzigingen & fouten Controleer de opdrachten op de gastcomputer. %(host)s gaf een onbekende fout aan:
%(err)s
Bekijk de 'Back In Time-handleiding' voor verdere instructies Controleer of de gastcomputer op afstand online is Controleer of de gastcomputer op afstand alle noodzakelijke opdrachten ondersteunt Codering %(cipher)s mislukt voor %(host)s:
%(err)s Codering: Opdracht niet gevonden: %s Opdracht: Configuratiebestand-hulp Configuratie voor versleutelde map niet gevonden. Doorgaan op fouten (incomplete reservekopieën behouden) Kopiëren Links kopiëren (symbolische links dereferentie) Kopieer de openbare SSH-sleutel "%(pubkey)s" naar gastcomputer op afstand "%(host)s".
Voer a.u.b. het wachtwoord in voor "%(user)s": Onveilige links kopiëren (werkt alleen met absolute links) Kon udev-regel voor profiel %(profile_id)s niet installeren. DBus-dienst '%(dbus_interface)s' was niet beschikbaar Kon SSH-privésleutel niet ontgrendelen. Het wachtwoord is ongeldig of is niet beschikbaar voor cron. Kon geen pad op afstand aanmaken:
 %s Kon de unieke identificatiecode voor "%s" niet vinden Maak een nieuwe SSH-sleutel aan zonder wachtwoord. Aangepaste uren Aangepaste uren kunnen alleen een door een komma gescheiden lijst met uren zijn (b.v. 8,12,18,23) of */3 om elke 3 uren een periodieke reservekopie te maken Dag(en) Dag: Decoderen Diepe controle (nauwkeuriger, maar langzaam) Standaardwaarde Maken van reservekopieën uitstellen indien op accu Verwijderen Doelbestandssysteem voor '%(path)s' is een aangekoppeld sshfs-netwerk dat geen harde links ondersteunt. Gebruik hiervoor in de plaats a.u.b. de 'SSH-modus'. Doelbestandssysteem voor '%(path)s' is geformatteerd als FAT dat geen harde links ondersteunt. Gebruik hiervoor in de plaats a.u.b een Linux-bestandssysteem. Doelbestandssysteem voor '%(path)s' is een aangekoppeld SMB-netwerk. Zorg er a.u.b. voor dat de SMB-server op afstand symbolische links ondersteunt of activeer '%(copyLinks)s' in '%(expertOptions)s'. Vergelijken met Vergelijkopties Maken van reservekopieën uitschakelen indien op accu Uitgeschakeld Weet u zeker dat u "%(file)s" uit %(count)d reservekopieën wilt verwijderen?
 Weet u zeker dat u "%(file)s" uit reservekopie "%(snapshot_id)s wilt verwijderen?
 Weet u zeker dat u het volgende bestand(en) wilt herstellen
naar de nieuwe map '%(path)s': Weet u zeker dat u het volgende bestand(en) wilt herstellen: Geen hernoemde reservekopieën verwijderen Voltooid Klaar, geen reservekopie nodig Geschatte aankomsttijd: Bewerken User-callback bewerken Meldingen activeren Versleuteling Fout Fout: Fouten Elke 10 minuten Elke 12 uren Elke 2 uren Elke 30 minuten Elke 4 uren Elke 5 minuten Elke 6 uren Elke dag Elke maand Elke week Elk uur Elke: Uitsluiten "%s" uitsluiten van toekomstige reservekopieën? Selecteer een bestand om uit te sluiten van de reservekopie Sluit bestanden uit die groter zijn dan de waarde in %(prefix)s.
Indien de 'Volledige rsync-modus' uitgeschakeld wordt zal dit alleen gelden voor nieuwe bestanden
omdat voor rsync dit een optie is voor verplaatsen en niet voor uitsluiten.
Dus grote bestanden waar reeds een reservekopie van bestond zullen behouden blijven
zelfs bij een eventuele wijziging. Bestanden uitsluiten die groter zijn dan:  Selecteer een map om uit te sluiten van de reservekopie Uit te sluiten patroon Patronen, bestanden en mappen uitsluiten Afsluiten Geavanceerde opties MISLUKT FAQ Kon geen nieuwe SSH-sleutel aanmaken in %(path)s Kon volgende configuratie niet laden: %s Kon volgende configuratie niet opslaan: %s Kon reservekopie %s niet maken!!! Kon geen nieuwe crontab schrijven. Filter: Voltooien Map Restant '%s' gevonden waarmee u verder kunt gaan. Volledige reservekopiepad:  Wanneer u met Volledige systeemreservekopie een reservekopie gaat maken zal deze alleen werken wanneer u deze herstelt naar dezelfde schijf/schijven en met dezelfde schijfpartitionering als de bron waar de reservekopie van wordt gemaakt; herstellen naar een andere schijf of dezelfde schijf maar met een andere partitionering zal mogelijk een gebroken en onbruikbaar systeem als gevolg hebben.

Volledige systeemreservekopie zal enige instellingen die mogelijk aangepast zijn opheffen. Wilt u doorgaan? Algemeen Algemeen Gaan naar Er heeft een Hash collision plaatsgevonden in hash_id %s. Vergroot de globale waarde voor hash_collision en probeer het opnieuw. Hulp Zeer aanbevolen: Persoonlijke map Gastcomputer/Gebruiker/Profiel-ID mag niet leeg zijn! Gastcomputer: Uur/uren Uur: Uren: Indien de vrije inodes kleiner zijn dan: Indien de vrije ruimte kleiner is dan: Als u dit venster sluit, zal Back In Time de computer niet uit kunnen schakelen wanneer de reservekopie voltooid is.
Weet u zeker dat u het wilt sluiten? Opnemen Bestand opnemen Bestanden en mappen opnemen Volgende map opnemen Informatie Behoud alle reservekopieën van de afgelopen Behoud één reservekopie per dag van de afgelopen Behoud één reservekopie per maand van de afgelopen Behoud één reservekopie per week van de afgelopen Behoud één reservekopie per jaar van alle jaren Sleutelbestand Laatste logboekweergave Laatste controle %s Afgelopen week Licentie rsync-bandbreedteverbruik limiteren:  Alleen reservekopieën tonen die verschillen Alleen soorgelijke reservekopieën tonen als:  Lokaal Lokaal versleuteld Logboekniveau: Hoofdprofiel Modus: Aanpassen voor volledige systeemreservekopie Maand(en) Time-out aankoppelprocesvergrendeling Nieuw profiel Nieuwere versies van bestanden zullen hernoemd worden met een '%(suffix)s' erachter voordat deze hersteld worden.
Indien u deze niet meer nodig heeft kunt u ze verwijderen d.m.v. '%(cmd)s' Geen configuratie gevonden Geen Er is niets gewijzigd, dus hoeft er geen nieuwe reservekopie gemaakt te worden Huidige schijfinhoud Ouder dan: Opties Opties moeten tussen aanhalingstekens staan b.v. --exclude-from="/path/to/my exclude file". Andere reservekopieën zullen geblokkeerd worden totdat de huidige is voltooid.
Dit is een algemene optie en zal daarom effect hebben op alle profielen van deze gebruiker.
U moet dit echter wel nog instellen voor alle andere gebruikers. Parameters: Wachtwoord Wachtwoord komt niet overeen Authenticatie aanmelden zonder wachtwoord voor %(user)s@%(host)s is mislukt. Bekijk de 'Back In Time-handleiding' voor verdere instructies. Extra opties aan rsync plakken Pad: Maken van reservekopie pauzeren Pingen van %s is mislukt. Gastcomputer is uitgeschakeld of er is een verkeerd adres gebruikt. Wachtwoord bevestigen Navigeer a.u.b. naar de reservekopie waarvan u de %(appName)s's configuratie wilt herstellen. Het pad kan er mogelijk zo uitzien: 
%(samplePath)s

Als deze reservekopieën op een extern medium staan of versleuteld zijn dient u deze eerst handmatig aan te koppelen. Wanneer u SSH-versleuteling gebruikt dient u het aanmelden met een openbare sleutel op de gastcomputer op afstand in te stellen %(addFuse)s.
Bekijk de 'Back In Time-handleiding'. Verifieer a.u.b. deze vingerafdruk! Wilt u deze toevoegen aan uw 'bekende_gastcomputers'? Poort: Status stroomvoorziening niet beschikbaar vanuit systeem Voorvoegsel om vooraf uit te voeren bij elke opdracht op gastcomputer op afstand.
Variabelen dienen te worden afgesloten met \$FOO.
Dit heeft geen consequenties voor rsync. Dus om een voorvoegsel
voor rsync in te stellen gebruik dan "%(cbRsyncOptions)s" met
%(rsync_options_value)s

%(default)s: %(def_value)s ACL bewaren Uitgebreide attributen (xattr) bewaren Privésleutel: Privésleutel "%(file)s" bestaat niet. Profiel "%s" bestaat al! Profiel '%(profile)s': Voer wachtwoord in voor %(mode)s:  Profiel: Profiel: "%s" Vraag stderr omleiden naar /dev/null in cronjobs. stdout omleiden naar /dev/null in cronjobs. Reservekopielijst vernieuwen Gastcomputer op afstand %(host)s ondersteunt geen '%(command)s':
%(err)s
Bekijk de 'Back In Time-handleiding' voor verdere instructies Gastcomputer op afstand %s ondersteunt geen harde links Pad op afstand bestaat maar is geen map:
 %s Pad op afstand is niet uitvoerbaar:
 %s Pad op afstand is niet beschrijfbaar:
 %s Verwijderen Reservekopie verwijderen Nieuwere bestanden in oorspronkelijke map verwijderen De van de laatste keer overgebleven map '%s' verwijderen Oude reservekopieën verwijderen Profiel hernoemen Herhaaldelijk (anacron) Een fout melden Herstellen '%s' herstellen '%s' herstellen naar... Configuratie herstellen Instellingen herstellen Rechten herstellen: Geselecteerd bestand of map herstellen.
Wanneer deze knop grijs gedimd is komt dat hoogstwaarschijnlijk omdat "%(now)s" is geselecteerd in de reservekopielijst aan de linkerkant.
Deze knop zal weer geactiveerd worden wanneer u op een reservekopie in deze reservekopielijst klikt. Herstel de geselecteerde bestanden of mappen naar het oorspronkelijke doel
en verwijder de bestanden/mappen die niet opgenomen waren in de reservekopie. Pas op:
Hierbij zullen dus de bestanden/mappen verwijderd worden die niet opgenomen waren in de reservekopie!
Wees hier uiterst voorzichtig mee!!! Herstel de momenteel getoonde map en al zijn inhoud naar een nieuw doel. Herstel de momenteel getoonde map en al zijn inhoud naar het oorspronkelijke doel. Herstel de geselecteerde bestanden of mappen naar een nieuw doel. Herstel de geselecteerde bestanden of mappen naar het oorspronkelijke doel. Herstellen naar... Maken van reservekopie hervatten Hoofdmap 'ionice' uitvoeren: 'nice' uitvoeren: 'rsync' uitvoeren met 'nocache': Laat Back In Time een reservekopie maken zodra er een extern medium aangesloten wordt (alleen om de X dagen).
Er zal om uw beheerderswachtwoord gevraagd worden. Laat Back In Time herhaaldelijk een reservekopie maken. Dit is vooral bruikbaar wanneer de computer niet regelmatig wordt gebruikt. Op de achtergrond uitvoeren op gastcomputer op afstand. Alleen maar één reservekopie tegelijk maken SSH SSH-instellingen SSH-versleuteld SSH-privésleutel Wachtwoord toevoegen aan sleutelbos Configuratiebestand opslaan... Rechten opslaan... Planning Planning udev werkt niet bij modus %s Alles selecteren Verzonden: Instellingen Shebang in user-callback-script is niet uitvoerbaar. Snelkoppelingen Volledig logboek tonen Verborgen bestanden tonen Uitschakelen Computer uitschakelen wanneer de reservekopie voltooid is. Slim verwijderen Reservekopie Reservekopielogboekweergave Naam reservekopie Reservekopie: %s Reservekopieën Reservekopiemap is niet geldig! Snelheid: Back InTime starten Maken van reservekopie stoppen Maak ook een nieuwe reservekopie als er niets gewijzigd is. Reservekopie maken Reservekopie maken met controlesom Reservekopie maken De authenticatie van gastcomputer "%(host)s" kon niet geverifieerd worden.

%(keytype)s-sleutelvingerafdruk is: Deze map bestaat niet
in de huidige geselecteerde reservekopie! Dit is GEEN reservekopie maar een actuele weergave van uw lokale bestanden Deze week Vandaag Vertalingen Probeer een minimum van %d%% vrije inodes te houden Probeer een minimum aan vrije ruimte te houden Omhoog %1 en %2 voor padparameters gebruiken Controlesom gebruiken om wijzigingen op te sporen Gebruiker: Beeld Laatste logboek bekijken Reservekopielogboek bekijken Overzicht huidige schijfinhoud Reservekopie gemaakt op %s bekijken WAARSCHUWING: Dit kan niet ongedaan worden gemaakt! WAARSCHUWING: het verwijderen van systeembestanden uit de hoofdmap kan uw hele systeem ruïneren!!! MET FOUTEN! Wacht %s seconde Wacht %s seconden Waarschuwing: indien uitgeschakeld en wanneer de gastcomputer
op afstand niet alle noodzakelijke opdrachten ondersteunt, kan dit mogelijk
leiden tot enige vreemde fouten. Waarschuwing: indien uitgeschakeld en wanneer de gastcomputer
op afstand niet beschikbaar is, kan dit mogelijk leiden tot
enige vreemde fouten. Website Week/weken Dag van de week: Wanneer er een extern medium aangesloten wordt (udev) Waar wilt u de reservekopieën opslaan Bezig met... Bezig met: Wilt u uw openbare SSH-sleutel kopiëren naar de gastcomputer op afstand
zodat u zich in het vervolg zonder wachtwoord kunt aanmelden? Jaar/jaren Gisteren U kunt een reservekopie niet met zichzelf vergelijken U kunt geen submap van de reservekopiemap toevoegen! U kunt de reservekopiemap niet toevoegen! U kunt het laatste profiel niet verwijderen! U heeft geen privésleutel voor SSH geselecteerd.
Wilt u een nieuwe openbare en privésleutel aanmaken voor het aanmelden zonder wachtwoord? Selecteer tenminste één map om een reservekopie te kunnen maken ! [E] Fout, [I] Informatie, [C] Wijziging als cron job dag(en) decodeerpaden standaardwaarde uitgeschakeld ingeschakeld encfs-versie 1.7.2 en daarvoor hebben een fout met de optie --reverse. Werk encfs a.u.b. bij maand(en) koppelpunt-%s niet leeg. op lokale machine op gastcomputer op afstand User-callback-script heeft geen shebang-regel (#!/bin/sh). week/weken bij het maken van een handmatige reservekopie 