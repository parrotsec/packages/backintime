��    �      �          p     q     �     �     �      �     �     �  
   �  
   �  
     
     
     
   "     -  
   5  
   @     K     Q  
   Z     e     n  2   r  2   �  0   �     	          -     9     H     U  e   m  [   �  J   /     z  %   �     �  7   �     �       A        T     \     m     �  &   �  .   �  '   �  2   
  T   =      �     �     �     �     �  $   �                    !  !   .     P     Y     v     {     �     �  
   �     �     �     �     �     �     �               "  	   0     :  
   F  
   Q     \     d     q     �  "   �     �     �     �     �     �     �  
   �                 ^        x     }     �     �     �     �     �     �     �     �     �            &   0  (   W  '   �  (   �  	   �     �     �       
        (     5     ;     U     a     f     j     v     ~     �     �     �  .   �     �     �  &   �     $  $   1     V     c     �     �     �  (   �  .   �  "         *     K     [     r     �     �     �     �     �     �     �     �     �     �               0     F  
   O     Z  	   c     m          �     �  	   �     �     �     �  	   �     �     �       !         B     a     g     u     �     �  !   �     �  &   �           !      )      2      R   
   j      u      ~   	   �   &   �   '   �   !   �   #   !  /   %!  &   U!     |!  S   �!     �!     �!     �!  �  "  +   �#     �#     �#     �#  &    $     '$     +$  
   4$  
   ?$  
   J$  
   U$  
   `$  
   k$     v$  
   ~$  
   �$     �$     �$     �$     �$  	   �$  $   �$  #   �$     %     1%  	   >%     H%     U%     e%     r%  r   �%  k   &  X   m&     �&  (   �&     �&  1   '     M'     T'  E   `'     �'     �'     �'  	   �'  '   �'  /   (      4(  /   U(  O   �(     �(     �(  	   )     )     )  (   %)     N)     U)     \)     a)     m)  	   �)     �)  	   �)     �)     �)     �)     �)  	   �)     �)     *     *  
   *     '*  
   3*  
   >*  
   I*     T*     [*     b*  	   i*     s*     z*     �*     �*  $   �*     �*     �*     �*     �*     �*  	   +     +     "+     )+     0+  ^   7+     �+     �+  	   �+  	   �+  	   �+  	   �+  !   �+     �+     �+     ,     $,     4,     A,  !   ],  !   ,  !   �,     �,     �,     �,     -     -     !-     .-  	   >-     H-     ^-     k-     o-  	   v-     �-  	   �-     �-     �-  	   �-  D   �-     �-  	   .  $   .  
   3.     >.  	   Y.     c.  	   z.     �.     �.  %   �.  .   �.  "   �.     /     :/     G/     Z/  
   j/     u/     |/     �/     �/     �/  	   �/     �/  
   �/  
   �/     �/     �/     0     0     +0     80     ?0     L0     _0  	   l0     v0     �0     �0     �0     �0     �0     �0     �0     �0      �0     1     *1     71     M1     `1     y1  !   �1     �1     �1     �1     �1     �1     �1     �1     2     2     *2     .2  !   52  -   W2     �2     �2  -   �2  $   �2     3  i   3     �3     �3     �3             �      2   �       �   �   V   ~   >   �   /   �   g   �      �   �          �   �   �   *   L   s   �           �   �   �   w   i   I   ?   �          -               �       D   e   T          z   h   \   �   B   0       �   `   E       �   m           �   5      �   %       K   ^   (   �   �   k   Q          |       �   �   �   �       ,   o   �                 3   �       7             &   N   
   u   �       �   �      y       r   G           F   A      _           �   )   �   #   H   �          f   �      �           �   �   Y   �              �   {   �   �   x   �   �      �   [   8   �   O   6                 �   P   �      �   �       l   �                       �   �   W   �   n           �               a   	   <       R      "          C       �   �   U   X       Z         �           �   �      +       9   J   4           �       j   p   '   ;   @       �       �   �   v   �          �       �   M   !   $   �       S          b      �   �   d   �   .       t   1      c   }   :       �   =   ]       �   q   �       �           �   �   �    
Create a new encrypted folder?  EXPERIMENTAL!  KB/sec %s is not a folder ! %s not found in ssh_known_hosts. ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add file Add folder Advanced All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s Ask a question At every boot/reboot Auto-remove Backup folders Blowfish-CBC Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't mount %s Can't mount '%(command)s':

%(error)s Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Cancel Cast128-CBC Change these options only if you really know what you are doing ! Changes Changes & Errors Command not found: %s Command: Config for encrypted folder not found. Continue on errors (keep incomplete snapshots) Copy links (dereference symbolic links) Copy unsafe links (works only with absolute links) Could not unlock ssh private key. Wrong password or password not available for cron. Couldn't create remote path:
 %s Couldn't find UUID for "%s" Custom Hours Day(s) Day: Deep check (more accurate, but slow) Default Delete Diff Diff Options Disable snapshots when on battery Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Encryption Error: Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Exclude Exclude file Exclude folder Exclude pattern Exclude patterns, files or folders Exit Expert Options FAILED FAQ Failed to take snapshot %s !!! Filter: Finalizing General Global Go To Hash collision occurred in hash_id %s. Incrementing global value hash_collision and try again. Help Highly recommended: Home Host: Hour: Hours: If free space is less than: Include Include file Include files and folders Include folder Informations Keep all snapshots for the last Keep one snapshot per day for the last Keep one snapshot per month for the last Keep one snapshot per week for the last Keep one snapshot per year for all years Last week Limit rsync bandwidth usage:  List only different snapshots Local Log Level: Main profile Mode: Mountprocess lock timeout New profile None Now Older than: Options Parameters: Password Password doesn't match Path: Ping %s failed. Host is down or wrong address. Please confirm password Port: Power status not available from system Preserve ACL Preserve extended attributes (xattr) Private Key: Profile "%s" already exists ! Profile: Profile: "%s" Refresh snapshots list Remote host %s doesn't support hardlinks Remote path exists but is not a directory:
 %s Remote path is not executable:
 %s Remote path is not writable:
 %s Remove Snapshot Removing old snapshots Rename profile Report a bug Restore Restore '%s' Restore '%s' to ... Restore permissions: Restore to ... Root SSH SSH Settings SSH private key Save Password to Keyring Saving config file... Saving permissions... Schedule Select All Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min free space Up Use %1 and %2 for path parameters Use checksum to detect changes User: View Last Log View Snapshot Log View the current disk content View the snapshot made at %s WARNING: This can not be revoked! WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) Weekday: When drive get connected (udev) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! [E] Error, [I] Information, [C] Change day(s) encfs version 1.7.2 and before has a bug with option --reverse. Please update encfs month(s) mountpoint %s not empty. weeks(s) Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-10-30 18:06+0000
Last-Translator: Germar <Unknown>
Language-Team: Simplified Chinese <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 
是否创建一个新的加密文件夹？  实验功能！  KB/秒 %s 不是一个文件夹！ 未在 ssh_known_hosts 中找到 %s。 ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 关于 添加文件 添加文件夹 高级 所有的 确定要改变快照文件夹吗？ 是否确定删除配置“%s”？ 您确定要移除快照：
%s 提出问题 重启时 自动移除 备份文件夹 Blowfish-CBC 无法新建文件夹： %s 无法找到 crontab。
您确定已经安装了 cron？
如果没有安装，您应当禁用所有自动备份。 找不到快照文件夹。
如果使用的是可移动磁盘，请连接到电脑，然后按 “确定” 找不到快照文件夹。
如果使用的是可移动磁盘，请先连接到电脑。 无法挂载 %s 无法挂载 '%(command)s'：

%(error)s 无法删除文件夹： %s 不能写入： %s
您确定有写入权限吗？ 取消 Cast128-CBC 除非您知道自己在干什么，否则不要变更这些选项！ 变更 变更 & 错误 命令未找到：%s 命令： 未找到加密文件夹的配置文件 忽略错误并继续(保留不完整的快照) 复制链接(废除符号链接) 复制不安全的链接(仅绝对链接有效) 无法解锁 SSH 私钥，可能密码不正确或该密码不适用于 cron。 无法创建远程路径：
 %s 无法找到 "%s" 的 UUID 自定义 天 日： 深度检查(更精确，但是较慢） 默认 删除 Diff Diff 选项 使用电池时禁用快照 已禁用 不要移除已命名的快照 已完成 已完成，无需备份。 编辑 启用通知 加密 错误： 错误 每10分钟 每12小时 每2小时 每30分钟 每4小时 每5分钟 每6小时 每天 每月 每周 每小时 排除 排除文件 排除文件夹 排除模式 不包含模式，文件或文件夹 退出 专家选项 失败 常见问题 创建快照 %s 失败!!! 过滤器 正在完成 常规 全局 转到 在 hash_id %s 中发现哈希碰撞。请递增 hash_collision 的全局设定值并重试。 帮助 强烈推荐： 主目录 主机： 小时： 小时： 如果磁盘可用空间少于： 包含 包含文件 包含文件和文件夹 包含文件夹 提示信息 保留最新的所有快照 每天保留一个最新的快照 每月保留一个最新的快照 每周保留一个最新的快照 每年保留一个快照 上周 限制 rsync 使用带宽：  仅列出不同的快照 本地 日志级别 主要配置。 模式： 解除挂载锁超时 新建配置 空 现在 早于： 选项 参数： 密码 密码不一致 路径： Ping %s 失败。可能主机未上线或您输入的地址错误。 确认密码 端口： 无法从系统中获取电源状态 保留 ACL 保留扩展属性 (xattr) 私钥： "%s"配置已存在！ 配置： 配置："%s" 刷新快照列表 远程主机 %s 不支持硬链接。 远程路径存在但不是一个目录：
 %s 远程路径不可被执行：
 %s 远程路径不可写：
 %s 移除快照 移除旧的快照 重命名配置 报告 BUG 还原 还原 '%s' 还原 '%s' 至 ... 恢复权限： 还原至： 根目录 SSH SSH 设置 SSH 私钥 保存密码至密钥环 保存配置文件 ... 保存权限 ... 计划任务 选择所有 设置 快捷方式 显示隐藏文件 智能移除 快照名 快照：%s 快照 快照文件夹无效！ 创建快照 创建快照 本周 今天 尝试保留最小可用空间 上移 用 %1 和 %2 作为路径参数 使用校验检测变更 用户名： 查看最近的日志 查看快照日志 查看当前磁盘内容 查看快照 %s 警告：该操作无法撤销！ 发生错误！ 等待 %s 秒 网站 周 工作日： 当设备连接时(udev) 何处保存快照 正在进行... 运行中： 年 昨天 不能将快照与其自身比较 不能包含备份文件夹的子文件夹！ 不能包含备份文件夹！ 不能删除唯一的配置！ 请至少选择一个文件夹进行备份！ [E] 错误，[I] 通知，[C] 变更 天 encfs 1.7.2 及更早的版本中 --reverse 选项存在缺陷，请更新系统中安装的 encfs 版本 月 挂载点 %s 不为空。 周 