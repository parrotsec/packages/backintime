��    8      �  O   �      �     �     �     �  
   �               5     =     F     M     V     [     `     u     |     �     �  	   �     �  
   �     �     �     �     �     �     �     �     �     �        	             $     )     -     5     A     _     h     v     ~  	   �     �  	   �     �     �     �     �     �  
   �     �  	   �  !   �  #     /   /  �  _     	     &	     *	     :	     L	     h	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  
   

     
  
   !
     ,
  	   2
  	   <
     F
     M
     S
     Y
  	   _
     i
     r
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
          %     -     4     <  
   C     N     [     c  +   k  &   �  5   �                 
      %   *         7                       .   '          4                 2                1   "       +            $       5   !             -                     	      #   3                   /   0   )      8                      ,       6          (   &            %s is not a folder ! About Add file Add folder At every boot/reboot Can't remove folder: %s Changes Command: Day(s) Disabled Done Edit Enable notifications Error: Errors Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exit Filter: General Go To Help Home Hour: Include Informations Last week Main profile None Now Options Parameters: Profile "%s" already exists ! Profile: Profile: "%s" Restore Settings Shortcuts Show hidden files This week Today Up User: Website Week(s) Working... Year(s) Yesterday You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-04-14 10:04+0000
Last-Translator: Michael Moroni <michaelmoroni@disroot.org>
Language-Team: Esperanto <eo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s ne estas dosierujo! Pri Aldoni dosieron Aldoni dosierujon Je ĉiu startigo/restartigo Ne povas forigi dosieron: %s Ŝanĝoj Komando: Tago(j) Malŝaltita Farite Redakti Aktivigi sciigojn Eraro: Eraroj Je ĉiuj 10 minutoj Je ĉiuj 5 minutoj Ĉiutage Ĉiumonate Ĉiusemajne Ekskluzivi Eliri Filtrilo: Ĝenerale Iri al Helpo Hejmo Horo: Inkluzivi informoj Lasta semajno Ĉefa profilo Neniu Nun Agordoj Parametroj: Profilo "%s" jam ekzistas! Profilo: Profilo: "%s" Restaŭri Agordoj Klavkombinoj Montri kaŝitajn dosierojn Nuna semajno Hodiaŭ Supren Uzanto: Retejo Semajno(j) Laborante... Jaro(j) Hieraŭ Vi ne povas inkluzivi sekurkopian dosieron! Vi ne povas forigi la lastan profilon! Vi devas almenaŭ elekti unu dosieron por sekurkopio! 