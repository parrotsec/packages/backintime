��    J      l  e   �      P     Q     f     j  2   p  2   �     �     �  e   �  [   c  J   �     
  7   "     Z     p     w     |     �     �     �     �     �  	   �     �  
   �     �     �     	     	  
   	     $	     +	     0	     5	  	   D	     N	     [	     g	     k	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  	   
     "
     4
     A
     O
  	   \
     f
     �
     �
  	   �
     �
     �
     �
     �
  &   �
       
   !     ,     5  	   =  &   G  '   n  !   �  #   �  /   �  �       �     �     �  !   �  %   �     $     5  x   T  i   �  T   7      �  H   �     �            	     	   '  %   1     W     ]     n     ~     �  	   �     �     �     �     �     �     �     �     �     �            	        &     )     G     O     \     u     �     �     �     �     �     �  	   �             	   !     +     A  '   S     {     �  
   �     �     �  ,   �  ,   �  #        8     @  	   M     W     [  ;   b  7   �  '   �  '   �  *   &           *   <       
   #   	   ;   1       ?   2           =   8             "       3   $             I   )             0      /       @                                 &                       !   .            9           ,                        +   (   :   B   G       6          '   A         -       >   C           F       4       5   D   7   J       E   %   H        %s is not a folder ! ... About Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Backup folders Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Command not found: %s Day(s) Diff Disabled Done Done, no backup needed Edit Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude file Exclude folder Exclude pattern Exit Finalizing Global Help Home Include folder Last week Main profile New profile Now Profile "%s" already exists ! Profile: Profile: "%s" Remove Snapshot Removing old snapshots Rename profile Restore Root Saving config file... Saving permissions... Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Up View the current disk content View the snapshot made at %s Waiting %s second. Waiting %s seconds. Week(s) Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:25+0000
Last-Translator: Yngve Spjeld Landro <l10n@landro.net>
Language-Team: Norwegian Nynorsk <nn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s er ikkje ei mappe. … Om Vil du verkeleg endra biletmappa? Vil du verkeleg sletta profilen "%s"? Kopieringsmapper Klarer ikkje å laga mappa: %s Finn ikkje crontab.
Er du sikker på at cron er installert?
Dersom ikkje, skru av alle automatiske tryggleikskopijobbar. Finn ikkje augneblinksbiletmappa.
Dersom ho er på ei ekstern lagringseining, plugg eininga i og trykk OK Finn ikkje augneblinksmappa.
Dersom ho er på ei ekstern lagringseining, plugg ho i. Klarer ikkje å fjerna mappa: %s Klarer ikkje å skriva til: %s
Er du sikker på at du har skrivetilgang? Fann ikkje kommandoen: %s Dag(ar) Diff Slått av Fullført Ferdig, trong ikkje tryggleikskopiera Endra Kvart 10. minutt Kvart 5. minutt Kvar dag Kvar månad Kvar veke Ekskluder fil Ekskluder mappe Ekskluder mønster Avslutt Sluttfører Globalt Hjelp Heim Inkluder mappe Førre veke Hovudprofil Ny profil No Profilen "%s" finst allereie. Profil: Profil: "%s" Fjern augneblinksbiletet Fjern gamle augneblinksbilete Gje profilen nytt namn Før tilbake Root Lagra innstillingsfila … Lagra løyve … Innstillingar Snarvegar Vis gøymde filer Smart-sletting Biletnamn Augneblinksbilete: %s Augneblinksbilete Mappa for augneblinksbileta er ugyldig. Ta augneblinksbilete Ta augneblinksbilete Denne veka I dag Opp Sjå på innhaldet til det gjeldande lageret Sjå på augneblinksbiletet som blei laga %s Venter %s sekund. Ventar %s sekund. Veke(r) Arbeider … Arbeider: År I går Du kan ikke samanlikna eit augneblinksbilete med seg sjølv Undermapper til tryggleikskopimappa kan ikkje vera med. Tryggleikskopimappa kan ikkje vera med. Du kan ikkje fjerna den siste profilen. Du må velja minst éi mappe for kopiering 