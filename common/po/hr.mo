��    }        �   �      �
     �
     �
     �
     �
  
   �
     �
     �
  2   �
  2   �
  0   /     `     u     �     �  e   �  [     J   j     �  7   �  A        G     O     `     v  .        �  $   �     �  !   �          
     '     ,     C     H     ]     d     k     |  	   �     �  
   �     �     �     �     �  "   �          	          7  
   ?     J     R     Y     _     d     x     }     �     �     �     �     �     �     �  	   �     �  
        #     0     <     A     E     Q     Y  &   e     �     �     �     �     �     �     �               #     (     >     T     ]  	   f     p     �     �     �  	   �     �     �     �  	   �     �                #     )     7     I     g     �  &   �     �     �     �  
   �     �     �  	   �  &     '   .  !   V  #   x  /   �  &   �  �  �     �     �  
   �     �  
   
            3   "  2   V  /   �     �     �     �     �  o     b   �  Z   �     >  E   ]  9   �     �     �     �       0        P  +   W     �  *   �     �      �  	   �      �               )     2     ;     L  	   \     f     s  	   �     �     �     �  %   �     �     �      �          "  	   /     9     B     I     P     d  	   m     w  !   |     �     �     �     �     �     �     �          *     8     D     L     Q     ]  
   d  (   o     �     �     �     �     �     �     �               %     -     J     a     j     s     |     �  
   �  
   �     �     �     �     �     �       &        /  	   4     >     T      j     �     �  >   �     �  	             %     3     :     A  +   H  *   t  &   �  $   �  -   �  +            Z   g   6           	   y   v                |   z   x   =           W       !       ;   u   ,   k   8      h      }      +       c   r   t          w               
      *      q   \       b   7   B   <       j   0   /   o          [              I   #   M   G   J   P   Y           C   {      L       D   $       p   -   :      m   F   O   K      5   R   d           `   ]       >       T   %   N                n   Q                  X   @   f   A         i   ?   S       l               E   9   U   a      e             .      H              3       V           "       (         ^   1   4   2       )   &           s   _   '    %s is not a folder ! ... About Add file Add folder Advanced All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s At every boot/reboot Auto-remove Backup folders Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Change these options only if you really know what you are doing ! Changes Changes & Errors Command not found: %s Command: Continue on errors (keep incomplete snapshots) Day(s) Deep check (more accurate, but slow) Diff Disable snapshots when on battery Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Error: Errors Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exclude file Exclude folder Exclude pattern Exclude patterns, files or folders Exit Expert Options Failed to take snapshot %s !!! Filter: Finalizing General Global Go To Help Highly recommended: Home Host: Hour: If free space is less than: Include Include file Include files and folders Include folder Informations Last week List only different snapshots Log Level: Main profile New profile None Now Older than: Options Parameters: Power status not available from system Preserve ACL Profile "%s" already exists ! Profile: Profile: "%s" Refresh snapshots list Remove Snapshot Removing old snapshots Rename profile Restore Root Saving config file... Saving permissions... Schedule Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min free space Up User: View Last Log View Snapshot Log View the current disk content View the snapshot made at %s WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! [E] Error, [I] Information, [C] Change Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:12+0000
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s nije mapa ! ... O programu Dodaj datoteku Dodaj mapu Napredno Sve Jeste li sigurni da želite promjeniti mapu snimki? Jeste li sigurni da želite obrisati profil "%s" ? Jeste li sigurni da želite ukloniti snimku:
%s Pri svakom paljenju/podizanju Auto-ukloni Mape rezerve Nije moguće napraviti mapu: %s Nije moguće pronaći crontab.
Provjerite jeli cron instaliran ?
Ako nije, onesposobite sve automatske rezerve. Nije moguće pronaći mapu snimki.
Ako je na izmjenjivom disku, molimo, spojite ga i pritisnite OK Nije moguće pronači mapu snimki.
Ako se nalazi na izmjenjivom disku, molimo, spojite ga. Nije moguće ukloniti mapu: %s Nije moguće zapisati u: %s
Provjerite imate li pristup zapisivanju ? Mjenjajte ove opcije samo ako stvarno znate što radite ! Promjene Promjene & Grješke Naredba nije pronađena: %s Naredba: Nastavi ne grješkama (zadrži nepotpune snimke) Dan(a) Dubinska provjera (preciznije, ali sporije) Razlika Onesposobi snimke tijekom rada na bateriji Onemogućeno Nemoj uklanjati imenovane snimke Završeno Završeno, nije potrebna rezerva Uredi Omogući obavijesti Greška: Grješke Svakih 10 minuta Svakih 5 minuta Svaki dan Svaki mjesec Svaki tjedan Isključi Isključi datoteku Isključi mapu Isključi slijed Iskljući sljedove, datoteke ili mape Izlaz Stručne opcije Neuspjelo uzimanje snimke %s !!! Filter: Završavanje Općenito Globalno Idi Na Pomoć Visoko preporučeno Početak Domaćin: Sat: Ako je slobodan prostor manji od: Uključi Uključi datoteku Ukljući mape i datoteke Uključi mapu Informacije Prošli tjedan Prikaži samo različite snimke Razina Zapisa: Glavni profil Novi profil Nijedno Sada Starije od: Opcije Parametri: Stanje energije nije dostupno od sustava Sačuvaj ACL Profil "%s" već postoji! Profil: Profil: "%s" Osvježi popis snimki Ukloni Snimku Ukloni stare snimke Preimenuj profil Povrati Korijen Spremi datoteku postavki ... Spremi dopuštenje ... Raspored Postavke Prečaci Prikaži skrivene datoteke Smart uklanjanje Ime Snimke Snimka: %s Snimke Mapa snimki nije valjana Uzmi snimku Uzmi snimku Ovaj tjedan Danas Pokušaj sačuvati min slobodno mjesta Gore Korisnik: Prikaži Zadnji Zapis Prikaži Zapis Snimke Prikaži trenutni sadržaj diska Prikaži snimku napravljenu %s S GRJEŠKAMA ! Čekanje %s sekunda. Čekanje %s sekundi. Čekanje %s sekundi. Web stranica Tjedan(a) Gdje sačuvati snimke Obrađujem... Radim: Godina Jučer Nije moguće usporediti snimku s njom samom Nije moguće uključiti pod-mapu rezerve ! Nije moguće uključiti mapu rezerve ! Nije moguće ukloniti zadnji profil! Morate odabrati barem jednu mapu za rezervu ! [E] Grješka, [I] Informacije, [C] Promjeni 