��    b      ,  �   <      H     I     ^     b     h  
   q     |     �  2   �  2   �  0   �      	     5	     D	  e   \	     �	  7   �	     
     
     +
     A
     J
     Q
     Z
     _
     v
     {
     �
     �
     �
     �
  	   �
     �
  
   �
     �
     �
     �
     �
                              #     )     /     K     S     `     o  	   |     �     �     �     �     �     �     �     �     �     �               '     /     4     J     `     i  	   r     |     �     �  	   �     �     �     �  	   �     �               "     (     F     c  &   q     �     �     �  
   �     �     �  	   �  &   �  '     !   5  #   W  /   {  �  �     ?     V     Z     g     v  
   �     �  2   �  .   �  3   �  !   -     O     i  �   �  %     5   7  
   m     x     �     �     �     �     �  $   �     �     �     
               )  
   9     D     S     `     i     q     �     �  
   �     �     �     �     �     �  %   �     �     �     �               *     :     H     N  
   R     ]     d     �     �     �     �     �     �     �     �               %  	   2     <     P     d     t  '   �     �     �     �     �  *   �       	     !     ,   >     k  #   y     �     �  $   �     �  	   �     �     �  >   �  2   9  /   l  6   �  ;   �                 9      T   Y   Q   /               P   U           S      K   :   4   G   ^   Z       %           ?   	   +           )   A   I   H            L         8   !      `   =   .              C   X   N   ]   E       -      ;   >       *              B   b   ,   J   D         1         M       @   5             $                "   _   (   &   F   '       0   [   \              
                R       a      7                    <               O   #   W   V   6   2           3    %s is not a folder ! ... About Add file Add folder Advanced All Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s At every boot/reboot Backup folders Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Changes Changes & Errors Command not found: %s Command: Day(s) Disabled Done Done, no backup needed Edit Enable notifications Error: Errors Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exit Expert Options Filter: General Global Go To Help Home Host: Hour: If free space is less than: Include Include file Include folder Informations Last week Main profile New profile None Now Older than: Options Profile "%s" already exists ! Profile: Profile: "%s" Remove Snapshot Removing old snapshots Rename profile Restore Root Saving config file... Saving permissions... Schedule Settings Shortcuts Show hidden files Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min free space Up User: View the current disk content View the snapshot made at %s WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:15+0000
Last-Translator: Germar <Unknown>
Language-Team: Faroese <fo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s er ikki ein skjatta ... Um forritið Legg fílu til Legg skjáttu til Framkomið Alt Er tú viss/ur í at broyta støðumyndaskjáttu ? Er tú viss/ur í at strika umhvarvið  "%s" ? Er tú viss/ur í at taka støðumyndina burtur:
%s Við hvørjari byrjan/endurbyrjan Trygdarritingar-skjáttur Kann ikki stovna skjáttuna: %s Kann ikki finna crontab.
Er tú viss/ur í at cron er innlagt ?
Um so ikk er, eigur tú at ógilda allar sjálvvirknar trygdarritingar. Kaann ikki taka burtur skjáttuna: %s Kann ikki skriva til: %s
Hevur tú skrivi rættindi ? Broytingar Broytingar & brek Stýriboð ikki funni: %s Stýriboð: Dag(ar) Ógilda Liðugt Liðug, eingin trygdarriting neyðug Ritstjórna Virkja kunningar Villa: Villur Hvønn 10 minutt Hvønn 5 minutt Hvønn dag Hvønn mánað Hvørja viku Útiloka Far úr Framkomnir kostir Filtur: Alment Heiltøkur Far til Hjálp Heim Vertur: Tími: Um tað tøka plássið er minni enn: Tak við Tak fílu við Tak skjáttur við Upplýsingar Fyrra vikan Høvuðsumhvarv Nýtt umhvarv Einki Nú Eldri enn: Kostir Umhvarv "%s" finnst longu ! Umhvarv: Umhvarv: "%s" Tak støðumynd burtur Tak gamlar støðumyndir burtir Nýnevn umhvarv Endurstovna Rót Goymi samansetingarfíluna ... Goymi loyvi .... Skrá Instillingar Snarvegir Vís fjaldar fílur Navn á støðumynd Støðumynd: %s Støðumyndir Støðumyndaskjáttan er ikki lóglig ! Tak eina løtumynd Tak eina løtumynd Henda vikan Í dag Royn at halda minstamark av tøkum plássi Upp Brúkari: Sýn núverandi innihald á diski Sýn støðumyndina ið gjørd varð við %s VIÐ VILLUM ! Bíði %s sekund. Bíði %s sekund. Heimasíða Vika(ur) Hvar skullu støðumyndirnar goymast Arbeiði... Arbeiði: Ár Í gjár Tú kannst ikki samanbera eina støðumynd við sær sjálvari Tú kann ikki hava eina trygdarritsskjáttu við ! Tú kann ikki hava trygdarritsskjáttuna við ! Tú kannst ikki taka tað síðsta umhvarvið burtur ! Tú má í minnsta lag velja eina skjáttu at trygdarrita ! 