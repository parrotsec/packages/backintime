��    �      4  �   L                (     =     A  
   J  
   U  
   `  
   k  
   v  
   �     �  
   �  
   �     �     �  
   �  2   �  2   �     *     ?     K     Z     g  e     [   �  J   A     �     �  7   �     �     �       '     j   >     �     �     �     �  �   E  �   �     s     |     �     �     �  
   �     �     �     �     �     �               !  	   /     9  
   E  
   P     [     `     o     �     �  
   �     �     �     �     �  '   �                    7     ?     L  	   [     e     k     {     �     �     �     �     �     �     �     �                    )     @     O     W     \     `     n     ~     �     �  	   �     �     �     �  	   �     �            	   %     /     5     S  !   V     x     �     �  &   �     �     �     �  
        #     ,  	   4  &   >  '   e  !   �  #   �  /   �  S     �  W  
             )     -  
   6  
   A  
   L  
   W  
   b  
   m     x  
   �  
   �  	   �  
   �     �  E   �  2   �     1     O     [     q     ~  �   �  s   ,  #   �     �     �  8   �     +     7     M  E   T  k   �          &     -  �   9  �   �  �   `     D     W     c          �  
   �     �     �     �     �     �     �     �               (  	   /  
   9  	   D     N     a     w  (   �  	   �     �     �     �     �  2   �                       9      B      T      g      t      z   
   �      �      �   
   �      �      �      �      �      �      !     !     +!     =!     X!     r!     ~!     �!     �!     �!     �!  	   �!     �!     �!     �!     �!     "     ""  (   2"     ["     k"     {"     �"  #   �"     �"  (   �"  (   �"  -   	#  
   7#     B#  
   V#     a#     i#     �#     �#     �#     �#  8   �#  .   �#  *   $      F$  1   g$  l   �$     z   ;   ^   '       a       9       W           P   b       ]   S       *   Q   @                           %   +       N       2   ,   �       n   v   6   1       r          )   3         E           !   C       p   	           [   Z         d       _   4              m      7                             .           ?   f   -   B   �   T       :          `   y      q   {       <             F   0   5   D   8   (   &   A              L           G   g      M          $   R   i         #   X           �   =   >      e       K   O   
   }   c   |   u   j          U       J   o           t   V   �   /       l   s   "      H   ~       h         \      w       k      Y   x   I     EXPERIMENTAL! %s is not a folder ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add file Add folder Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? At every boot/reboot Auto-remove Backup folders Blowfish-CBC Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't mount %s Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Cast128-CBC Command not found: %s Command: Copy links (dereference symbolic links) Could not install Udev rule for profile %(profile_id)s. DBus Service '%(dbus_interface)s' wasn't available Custom Hours Day(s) Default Destination filesystem for '%(path)s is a sshfs mounted share. sshfs doesn't support hard-links. Please use mode 'SSH' instead. Destination filesystem for '%(path)s' is formatted with FAT which doesn't support hard-links. Please use a native Linux filesystem. Destination filsystem for '%(path)s' is a SMB mounted share. Please make sure the remote SMB server supports symlinks or activate '%(copyLinks)s' in '%(expertOptions)s'. Disabled Done Done, no backup needed Edit Enable notifications Encryption Error: Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Day Every Month Every Week Every hour Exit Expert Options Failed to load config: %s Failed to save config: %s Failed to write new crontab. Finalizing General Global Go To Help Host/User/Profile-ID must not be empty! Hour(s) Hour: If free space is less than: Include Include file Include folder Last week Local Local encrypted Main profile Month(s) Now Older than: Options Parameters: Password doesn't match Please confirm password Profile "%s" already exists ! Profile: Profile: "%s" Remove Snapshot Removing old snapshots Rename profile Restore Root SSH SSH encrypted SSH private key Saving permissions... Schedule Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min free space Up Use %1 and %2 for path parameters View the current disk content View the snapshot made at %s WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) When drive get connected (udev) Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! encfs version 1.7.2 and before has a bug with option --reverse. Please update encfs Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-01-11 12:24+0000
Last-Translator: Numan Demirdöğen <if.gnu.linux@gmail.com>
Language-Team: Turkish <tr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
  KARARSIZ! %s bir klasör değil ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Hakkında Dosya ekle Klasör ekle Anlık durum klasörünü değiştirmek istediğinizden emin misiniz? "%s" profilini silmek istediğinize emin misiniz ? Her yeniden başlattığında Oto-kaldır Yedekleme klasörleri Blowfish-CBC Dizin oluşturulamadı: %s crontab bulunamadı.
cron'un yüklenmiş olduğundan emin misiniz ?
Eğer değilseniz tüm otomatik yedeklemeleri etkisiz hale getirmeniz gerekir. Anlık durum klasörü bulunamadı.
Eğer çıkarılabilir bir sürücüde ise lütfen takın ve TAMAM'a tıklayın Anlık durum klasörü bulunamadı. %s takılamadı Klasör kaldırılamıyor: %s Yazılamıyor: %s
Yazma izniniz olduğuna emin misiniz ? Cast128-CBC Komut bulunamadı: %s Komut: Bağları kopyala (sembolik bağların hedef dosyaslarını kopyalar) %(profile_id)s profilleri için Udev kuralı yüklenemedi. '%(dbus_interface)s' DBus Servisi mevcut değil. Kullanıcı tanımlı saatlerde Günde Varsayılan '%(path)s' için girilen hedef dosya sistemi bir paylaşımlı sshfs dizini. sshfs sabit bağları desteklemiyor. Lütfen 'SSH' usulünü kullanın. '%(path)s' için girilen hedef dosya sistemi sabit bağları desteklemeyen FAT ile biçimlendirilmiş. Lütfen bir Linux dosya sistemi kullanın. '%(path)s' için girilen hedef dosya sistemi bir paylaşımlı SMB dizini. Lütfen SMB sunucunun sembolik bağları desteklediğini teyit edin ya da '%(expertOptions)s' bölümünden '%(copyLinks)s' seçeneğini etkinleştirin. Etkisizleştirildi Tamamlandı Bitti, yedekleme gerekmiyor Düzenle Bildirimleri etkinleştir Şifreleme Hata: Her 10 dakikada bir 12 saatte bir 2 saatte bir 30 dakikada bir 4 saatte bir Her 5 dakikada bir 6 saatte bir Her gün Her Ay Her Hafta Saatte bir Çıkış Uzman Seçenekleri Ayar yüklenemedi: %s Ayar kaydedilemedi: %s Yeni crontab yazımı başarısız oldu. Bitiriyor Genel Evrensel Git Yardım Sunucu/Kullanıcı/Profil ID boş bırakılmamalı Saatte Saat: Eğer boş alan daha az ise: Dahil et Dosyayı dahil et Klasörü dahil et Geçen hafta Yerel Şifrelenmiş yerel Ana profil Ayda Şimdi Daha eski: Seçenekler Parametreler: Şifre eşleşmiyor Lütfen şifreyi onaylatın "%s" profili zaten mevcut ! Profil: Profil: "%s" Anlık durumu sil Eski anlık durumları sil Profili yeniden adlandır Geri Yükle Kök Dizini SSH Şifrelenmiş SSH Gizli SSH anahtarı ... iznini kaydet Zamanlama Ayarlar Kısayollar Gizli dosyaları göster Akıllı kaldırma Anlık durum ismi Anlık durumlar Anlıkçekim klasörü geçerli değil ! Anlık durum al Anlık durum al Bu hafta Bugün En az boş alanı korumaya çalış Yukarı Yol parametreleri olarak %1 ve %2 kullan Şu andaki disk içeriğini görüntüle %s'da oluşturulan anlık durumu görüntüle HATALARLA! %s saniye bekliyor. Web sitesi Haftada Disk bağlandığında (udev) Çalışıyor... Çalışıyor: Yılda Dün Bir anlık durumu kendisi ile karşılaştıramazsınız Yedekleme alt-klasörünü dahil edemezsiniz ! Yedekleme klasörünü dahil edemezsiniz ! Son profili kaldıramazsınız ! Yedekleme için en az bir klasör seçmelisiniz ! 1.7.2 ve önceki encfs sürümlerinin --reverse seçeneğinde bir böcek var. Lütfen encfs'yi güncelleyin. 