��    �        �         �
     �
     �
     �
     �
  
     
     
     
   #  
   .  
   9     D  
   L  
   W     b     h  
   q  2   |  2   �  0   �          (     4     C     P  e   h  [   �  J   *     u  7   �     �     �     �     �     �          !     (     0     5  !   B     d     m     �     �     �     �  
   �     �     �     �     �                     /     =  
   I  
   T     _     g     t     �     �     �  
   �     �     �     �     �     �     �     �     �       	        (     .     >     K     O     [     c     o     �     �     �     �     �     �     �     �     �     �     	          (  	   1     ;     M     Z     h  	   u          �     �  	   �     �     �     �  !   �          .     K  &   Y     �     �     �     �  
   �     �     �  	   �  &   �  '     !   =  #   _  /   �  �  �  	   P     Z     p     t  
   }  
   �  
   �  
   �  
   �  
   �     �  
   �  
   �     �     �     �  7   �  2   6  5   i     �     �     �     �     �  |     n   �  ]   �     Q  A   k     �  	   �     �  	   �     �  
                  !  	   &  +   0  
   \  %   g     �  '   �     �     �  
   �     �     �     �     	          )     :     J     [     g  	   p  
   z     �     �     �     �     �  	   �     �     �     �     �     �          $     -     F     X     d     j     y     �  
   �     �     �  !   �     �     �     �     �          %     2     6     :     G     Z     n     w  	   �     �     �     �     �     �  '   �          )  
   <     G  !   L     n  "   r     �     �  
   �  %   �                    4     P  	   ]     g     k  7   q  5   �  -   �  &     .   4         i      +   B              k      s   9   P       ;   3   b   S   E   8          q   z       ~   !   L   f      `   O   Q   _   T   :   e   5   <   �           \   p   )                              1          $   
                      {   Z          '   y                   &   "               w   I   X       N   *           ]   l   >                  M      %          |              V       g      @               [   H      ?   U       0       h   }   r   -   4   n   m   (   t          u              o   F   x   R   G   c       D   d   C       a           W   2   ,   6      Y   ^   A   v   7   /   J                  .   #   =         j   	   K     EXPERIMENTAL! %s is not a folder ! ... 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 About Add file Add folder Are you sure you want to change snapshots folder ? Are you sure you want to delete the profile "%s" ? Are you sure you want to remove the snapshot:
%s At every boot/reboot Auto-remove Backup folders Blowfish-CBC Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't find snapshots folder.
If it is on a removable drive please plug it and then press OK Can't find snapshots folder.
If it is on a removable drive please plug it. Can't remove folder: %s Can't write to: %s
Are you sure you have write access ? Cast128-CBC Changes Command not found: %s Command: Couldn't find UUID for "%s" Custom Hours Day(s) Default Diff Diff Options Disable snapshots when on battery Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Encryption Errors Every 10 minutes Every 12 hours Every 2 hours Every 30 minutes Every 4 hours Every 5 minutes Every 6 hours Every Month Every Week Every hour Exclude Exclude file Exclude folder Exclude pattern Exit Expert Options Finalizing General Global Go To Help Home If free space is less than: Include Include files and folders Include folder Last week Local Local encrypted Main profile Now Older than: Options Parameters: Profile "%s" already exists ! Profile: Profile: "%s" Remove Snapshot Removing old snapshots Rename profile Restore Root SSH SSH encrypted SSH private key Saving permissions... Schedule Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Snapshots folder is not valid ! Take snapshot Taking snapshot This week Today Trying to keep min free space Up Use %1 and %2 for path parameters View the current disk content View the snapshot made at %s WITH ERRORS ! Waiting %s second. Waiting %s seconds. Website Week(s) When drive get connected (udev) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:15+0000
Last-Translator: Germar <Unknown>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
  USTABIL! %s er ikke en mappe ! … 3DES-CBC AES128-CBC AES128-CTR AES192-CBC AES192-CTR AES256-CBC AES256-CTR ARCFOUR ARCFOUR128 ARCFOUR256 Om Legg til fil Legg til katalog Er du sikker på at vil bytte øyeblikksbilder mappen ? Er du sikker på at du vil slette profilen «% s"? Er du sikker på at du vil fjerne øyeblikksbilde:
%s For hver start/omstart Fjern automatisk Øyeblikksbilde mapper Blowfish-CBC Kan ikke opprette mappe: %s Kan ikke finne crontab.
Er du sikker på at cron er installert?
Hvis ikke bør du skru av alle automatiske sikkerhetskopier. Kan ikke finne øyeblikksbilde mappe.
Hvis den er på en ekstern disk vennligst plugg den i, deretter trykk OK Kan ikke finne øyeblikksbilde mappe.
Hvis den er på en ekstern disk, vennligst plugg den i. Kan ikke fjerne mappe: %s Kan ikke skrive til:% s
Er du sikker på at du har skrivetilgang? Cast128-CBC Endringer Kommando ikke funnet: %s Kommando: Kunne ikke finne UUID for "%s" Velg timer Dag(er) Standard Diff Diff Valg Slå av funksjonen snapshot ved bateridrift Deaktivert Ikke fjern navngitte øyeblikksbilder Ferdig Ferdig, sikkerhetskopiering trengs ikke Rediger Slå på meldinger Kryptering Feil Hvert 10. minutt Hver tolvte time Annenhver time Hvert 30. minutt Hver fjerde time Hvert 5. minutt Hver sjette time Hver måned Hver uke Hver time Ekskludér Ekskluder fil Ekskludér katalog Utelukk mønster Avslutt Avanserte innstillinger Avslutter Generelt Global Gå Til Hjelp Hjem Hvis ledig plass er mindre enn: Inkluder Inkluder filer og mapper Inkludér katalog Forrige uke Lokal Lokal kryptert Hovedprofil Nå Eldre enn: Alternativer Parametere: Profil "% s" eksisterer allerede! Profil: Profil: "%s" Fjern Øyeblikksbilde Slett gamle øyeblikksbilder Omdøp profil Gjennopprett Rot SSH SSH kryptert Privat SSH nøkkel Lagre rettighet ... Timeplan Innstillinger Snarveier Vis skjulte filer Smart sletting Navn på øyeblikksbilde Øyeblikksbilde: %s Øyeblikksbilder Øyeblikksbilde mappen er ikke gyldig ! Ta øyeblikksbilde Ta øyeblikksbilde Denne uken Idag Forsøk å bevare min ledig plass Opp Bruk %1 and %2 for path parameters Se nåværende stasjons innhold Se øyeblikksbilde laget den %s MED FEIL ! Venter %s sekund. Venter %s sekunder. Nettside Uke(r) Når disken kobles til (udev) Hvor lagre øyeblikksbilder Arbeider … Arbeider: År Igår Du kan ikke sammenlikne et øyeblikksbilde med seg selv Du kan ikke inkludere en sikkerhetskopi under-mappe ! Du kan ikke inkludere sikkerhetskopi mappen ! Du kan ikke fjerne den siste profilen! Du velge minst en mappe å sikkerhetskopiere ! 