��    E      D  a   l      �     �          
       
     2   $     W     l     �     �     �     �     �     �     �     �     �     �        	          
   &     1     9     F     U     Z  
   i     t     {     �     �     �     �     �  	   �     �     �     �     �     �     �          #     ,     :     I     Q     V     _  	   h     r     �     �  	   �     �     �     �     �     �  
   �     �     �  	   �  '   	  !   0	  #   R	  /   v	  �  �	     �     �     �     �     �  0   �     �       "   :     ]  	   u  	        �  
   �     �  	   �     �     �     �     �     �     �                    .     6     F     V     ]     f     m  *   t     �     �     �     �  
   �     �     �     �  
              2     :     K     _     l     x     }     �     �     �  *   �     �          
  
             %     2     B     P     T  '   Y  '   �  #   �  D   �             +      1             =         <      @   *   9   :      "                                $   !      '                   >   	   (             #   -         0          2                   /   )       ,      .   7      D                   8       &   C       ?          4   ;      E       6   %                    
       3          A       5   B    %s is not a folder ! ... About Add file Add folder Are you sure you want to delete the profile "%s" ? At every boot/reboot Can't create folder: %s Can't remove folder: %s Command not found: %s Command: Day(s) Diff Disabled Done Edit Enable notifications Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exclude file Exclude folder Exit Expert Options Finalizing Global Go To Help Home If free space is less than: Include Include folder Last week Main profile New profile Now Older than: Options Parameters: Profile "%s" already exists ! Profile: Profile: "%s" Rename profile Restore Root Schedule Settings Shortcuts Show hidden files Smart remove Snapshots folder is not valid ! This week Today Up WITH ERRORS ! Website Week(s) Working... Working: Year(s) Yesterday You can't include a backup sub-folder ! You can't include backup folder ! You can't remove the last profile ! You must select at least one folder to backup ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-01-08 00:47+0000
Last-Translator: mariusshh@gmail.com <Unknown>
Language-Team: Romanian <ro@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1 ? 0: (((n % 100 > 19) || ((n % 100 == 0) && (n != 0))) ? 2: 1));
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s nu este un director ! ... Despre Adaugă fișier Adaugă director Sigur doriți să ștergeți profilul „%s” ? La fiecare pornire/restart Nu se poate crea directorul: %s Nu se poate elimina directorul: %s Comandă negăsită: %s Comandă: Zi (zile) Diff Dezactivat Gata Modifică Activează notificările La fiecare 10 minute La fiecare 5 minute Zilnic Lunar Săptămânal Exclude Exclude fișier Exclude director Ieșire Opțiuni expert Se finalizează Global Mergi la Ajutor Acasă Dacă spațiul disponibil este mai mic de: Include Include director Săptămâna trecută Profil principal Profil nou Acum Mai vechi de: Opțiuni Parametri: Profilul „%s” există deja ! Profil: Profil: „%s” Redenumește profil Restaurează Rădăcină Orar Setări Scurtături Arată fișierele ascunse Eliminare inteligentă Folderul pentru instantaneu nu este valid! Săptămâna aceasta Astăzi Sus CU ERORI ! Pagină web Săptămâni Se lucrează... Se lucrează: Ani Ieri Nu poti include un sub-folder de backup Nu poți include directorul de backup ! Nu puteți șterge ultimul profil ! Trebuie să selectați măcar un director pentru copia de rezervă ! 