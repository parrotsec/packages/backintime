��    \      �     �      �     �     �     �     �  
     0        =     I     X  e   p     �     �     �     	     	     &	     -	     2	     ?	     H	     e	     j	     �	     �	     �	     �	     �	     �	  	   �	     �	  
   �	     �	     �	      
     
     
     $
     3
  
   ;
     F
     N
     U
     [
     `
     e
     k
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
                '     /     4     J     S  	   \     f     x     �     �  	   �     �     �  	   �     �     �     �  !   �          :     @     ^  &   {     �     �     �  
   �     �     �  	   �  &   �  '     !   ?  �  a     K     ^     b     g     w  7   �     �     �     �          +  
   K     V     l     �     �     �     �  	   �  6   �     �  $        )     0     F     N     U     f     u     }     �  	   �     �     �     �     �     �     �     �                         %     .     7  	   W     a     r     �     �     �     �     �  	   �     �     �  #     	   ,     6  $   ;     `  
   n     y     �     �     �     �     �     �          0  	   ?     I     Q  &   Z  0   �     �     �  +   �  A   	     K     _  !   t  
   �     �     �     �  8   �  ;   �  8   /     T   C             6   K   O   R   ,      D            @                +   G              4          X       ?             [      2   0      P                       H   8   Z       	   
   <   %   1   M                   #       5   E                 \           "   -      B   7      )   3       ;          >       L   &           A          $       =   (   *               U              '       Y   F   !   S          I   .   /   Q   N               W      9   :   J   V    %s is not a folder ! ... About Add file Add folder Are you sure you want to remove the snapshot:
%s Auto-remove Backup folders Can't create folder: %s Can't find crontab.
Are you sure cron is installed ?
If not you should disable all automatic backups. Can't remove folder: %s Changes Changes & Errors Command not found: %s Command: Day(s) Diff Diff Options Disabled Don't remove named snapshots Done Done, no backup needed Edit Enable notifications Error: Errors Every 10 minutes Every 5 minutes Every Day Every Month Every Week Exclude Exclude file Exclude folder Exclude pattern Exit Expert Options Filter: Finalizing General Global Go To Help Home Hour: If free space is less than: Include Include file Include files and folders Include folder Informations Last week Now Older than: Options Parameters: Remove Snapshot Removing old snapshots Restore Root Saving config file... Schedule Settings Shortcuts Show hidden files Smart remove Snapshot Name Snapshot: %s Snapshots Take snapshot Taking snapshot This week Today Trying to keep min free space Up Use %1 and %2 for path parameters Use checksum to detect changes User: View the current disk content View the snapshot made at %s Waiting %s second. Waiting %s seconds. Website Week(s) Where to save snapshots Working... Working: Year(s) Yesterday You can't compare a snapshot to itself You can't include a backup sub-folder ! You can't include backup folder ! Project-Id-Version: backintime
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-12 04:18+0000
Last-Translator: Mantas Kriaučiūnas <mantas@akl.lt>
Language-Team: Lithuanian <lt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2019-04-14 16:16+0000
X-Generator: Launchpad (build 18920)
 %s nėra aplankas! ... Apie Pridėti failą Pridėti aplanką Ar jūs tikrai norite pašalinti momentinę kopiją:
%s Pašalinti automatiškai Atsarginių kopijų aplankai Nepavyko sukurti aplanko: %s Negaliu rasti crontab. Nepavyko pašalinti aplanko: %s Pakeitimai Pakeitimai ir klaidos Komanda nerasta: %s Komanda: Diena (os/ų) Pasikeitimai Pasikeitimų parinktys Išjungta Nešalinti momentinių kopijų, turinčių pavadinimus Atlikta Atlikta, atsarginė kopija nebūtina Keisti Įgalinti pranešimus Klaida: Kaidos Kas 10 minučių Kas 5 minutės Kasdien Kas mėnesį Kas savaitę Išskirti Išskirti failą Išskirti aplanką Išskirti šabloną Baigti Parinktys ekspertams Filtras: Baigiu Bendri Globalus Eiti į Pagalba Pradžia Valanda: Jei laisvos vietos mažiau nei: Įtraukti Įtraukti failą Įtraukti failus bei aplankus Įtraukti aplanką Informacija Praeitą savaitę Dabar Senesni nei: Parinktys Parametrai: Pašalinti momentines kopijas Pašalinti senas momentines kopijas Atstatyti Root Išsaugoti konfigūracijos failą... Tvarkaraštis Nustatymai Nuorodos Rodyti paslėptus failus Sumanus pašalinimas Momentinės kopijos pavadinimas Momentinė kopija: %s Momentinės kopijos Priimti momentinę kopiją Priimti momentinę kopiją Šią savaitę Šiandien Bandyti Aukštyn naudoti %1 ir %2 kaip kelio parametrus Pasikeitimų aptikimui naudoti kontrolines sumas Naudotojas: Žiūrėti esamo disko turinį Žiūrėti momentinę kopiją, padarytą %s Laukti %s sekundes. Laukti %s sekundžių. Laukti %s sekundžių. Interneto svetainė Savaitė (ės/čių) Kur išsaugoti momentinę kopiją Dirbama... Dirbu: Metai (-ų) Vakar Jūs negalite palyginti momentinės kopijos su ja pačia Negalima įtraukti atsarginėms kopijoms skirto poaplankio! Negalima įtraukti atsarginėms kopijoms skirto aplanko! 