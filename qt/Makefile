PREFIX=/usr
DEST=$(DESTDIR)$(PREFIX)

all:	build

build:	compress

clean:
	rm -f po/*.mo
	rm -f man/C/*.gz

install:
	#install python
	install -d $(DEST)/share/backintime/qt
	install --mode=644 app.py $(DEST)/share/backintime/qt
	install --mode=644 icon.py $(DEST)/share/backintime/qt
	install --mode=644 logviewdialog.py $(DEST)/share/backintime/qt
	install --mode=644 messagebox.py $(DEST)/share/backintime/qt
	install --mode=644 qtsystrayicon.py $(DEST)/share/backintime/qt
	install --mode=644 qttools.py $(DEST)/share/backintime/qt
	install --mode=644 restoredialog.py $(DEST)/share/backintime/qt
	install --mode=644 serviceHelper.py $(DEST)/share/backintime/qt
	install --mode=644 settingsdialog.py $(DEST)/share/backintime/qt
	install --mode=644 snapshotsdialog.py $(DEST)/share/backintime/qt

	#install plugin
	install -d $(DEST)/share/backintime/plugins
	install --mode=644 plugins/notifyplugin.py $(DEST)/share/backintime/plugins
	install --mode=644 plugins/qt4plugin.py $(DEST)/share/backintime/plugins

	#install application
	install -d $(DEST)/bin
	install --mode=755 backintime-qt $(DEST)/bin
	install --mode=755 backintime-qt_polkit $(DEST)/bin

	#install dbus service
	install -d $(DEST)/share/dbus-1/system-services
	install --mode=644 net.launchpad.backintime.serviceHelper.service $(DEST)/share/dbus-1/system-services

	#install dbus conf
	install -d $(DEST)/../etc/dbus-1/system.d
	install --mode=644 net.launchpad.backintime.serviceHelper.conf $(DEST)/../etc/dbus-1/system.d

	#install polkit action
	install -d $(DEST)/share/polkit-1/actions
	install --mode=644 net.launchpad.backintime.policy $(DEST)/share/polkit-1/actions

	#install documentation
	install -d $(DEST)/share/doc/backintime-qt
	install --mode=644 ../debian/copyright $(DEST)/share/doc/backintime-qt
	install --mode=644 ../AUTHORS $(DEST)/share/doc/backintime-qt
	install --mode=644 ../LICENSE $(DEST)/share/doc/backintime-qt
	install --mode=644 ../README.md $(DEST)/share/doc/backintime-qt
	install --mode=644 ../TRANSLATIONS $(DEST)/share/doc/backintime-qt
	install --mode=644 ../VERSION $(DEST)/share/doc/backintime-qt
	install --mode=644 ../CHANGES $(DEST)/share/doc/backintime-qt

	#install .desktop
	install -d $(DEST)/share/applications
	install --mode=644 backintime-qt.desktop $(DEST)/share/applications
	install --mode=644 backintime-qt-root.desktop $(DEST)/share/applications

	#install docbook
	install -d $(DEST)/share/doc/qt/HTML/en/backintime
	install --mode=644 docbook/en/index.docbook $(DEST)/share/doc/qt/HTML/en/backintime

	#install man
	install -d $(DEST)/share/man/man1
	install --mode=644 man/C/backintime-qt.1.gz $(DEST)/share/man/man1

	#install icons
	install -d $(DEST)/share/icons/hicolor/scalable/actions
	install --mode=644 icons/scalable/actions/show-hidden.svg $(DEST)/share/icons/hicolor/scalable/actions
	install -d $(DEST)/share/icons/hicolor/48x48/actions
	install --mode=644 icons/48x48/actions/show-hidden.svg $(DEST)/share/icons/hicolor/48x48/actions
	install -d $(DEST)/share/icons/hicolor/32x32/actions
	install --mode=644 icons/32x32/actions/show-hidden.svg $(DEST)/share/icons/hicolor/32x32/actions
	install -d $(DEST)/share/icons/hicolor/24x24/actions
	install --mode=644 icons/24x24/actions/show-hidden.svg $(DEST)/share/icons/hicolor/24x24/actions
	install -d $(DEST)/share/icons/hicolor/22x22/actions
	install --mode=644 icons/22x22/actions/show-hidden.svg $(DEST)/share/icons/hicolor/22x22/actions
	install -d $(DEST)/share/icons/hicolor/16x16/actions
	install --mode=644 icons/16x16/actions/show-hidden.svg $(DEST)/share/icons/hicolor/16x16/actions

compress:
	#man pages
	for i in $$(ls -1 man/C/); do case $$i in *.gz|*~) continue;; *) gzip -n --best -c man/C/$$i > man/C/$${i}.gz;; esac; done

uninstall: uninstall_files uninstall_dirs

uninstall_files:
	#uninstall files python
	rm -f $(DEST)/share/backintime/qt/__pycache__/*.pyc
	rm -f $(DEST)/share/backintime/qt/app.py
	rm -f $(DEST)/share/backintime/qt/icon.py
	rm -f $(DEST)/share/backintime/qt/logviewdialog.py
	rm -f $(DEST)/share/backintime/qt/messagebox.py
	rm -f $(DEST)/share/backintime/qt/qtsystrayicon.py
	rm -f $(DEST)/share/backintime/qt/qttools.py
	rm -f $(DEST)/share/backintime/qt/restoredialog.py
	rm -f $(DEST)/share/backintime/qt/serviceHelper.py
	rm -f $(DEST)/share/backintime/qt/settingsdialog.py
	rm -f $(DEST)/share/backintime/qt/snapshotsdialog.py

	#uninstall files plugin
	rm -f $(DEST)/share/backintime/plugins/__pycache__/*.pyc
	rm -f $(DEST)/share/backintime/plugins/notifyplugin.py
	rm -f $(DEST)/share/backintime/plugins/qt4plugin.py

	#uninstall files application
	rm -f $(DEST)/bin/backintime-qt
	rm -f $(DEST)/bin/backintime-qt_polkit

	#uninstall files dbus service
	rm -f $(DEST)/share/dbus-1/system-services/net.launchpad.backintime.serviceHelper.service

	#uninstall files dbus conf
	rm -f $(DEST)/../etc/dbus-1/system.d/net.launchpad.backintime.serviceHelper.conf

	#uninstall files polkit action
	rm -f $(DEST)/share/polkit-1/actions/net.launchpad.backintime.policy

	#uninstall files documentation
	rm -f $(DEST)/share/doc/backintime-qt/copyright
	rm -f $(DEST)/share/doc/backintime-qt/AUTHORS
	rm -f $(DEST)/share/doc/backintime-qt/LICENSE
	rm -f $(DEST)/share/doc/backintime-qt/README.md
	rm -f $(DEST)/share/doc/backintime-qt/TRANSLATIONS
	rm -f $(DEST)/share/doc/backintime-qt/VERSION
	rm -f $(DEST)/share/doc/backintime-qt/CHANGES

	#uninstall files .desktop
	rm -f $(DEST)/share/applications/backintime-qt.desktop
	rm -f $(DEST)/share/applications/backintime-qt-root.desktop

	#uninstall files docbook
	rm -f $(DEST)/share/doc/qt/HTML/en/backintime/index.docbook

	#uninstall files man
	rm -f $(DEST)/share/man/man1/backintime-qt.1.gz

	#uninstall files icons
	rm -f $(DEST)/share/icons/hicolor/scalable/actions/show-hidden.svg
	rm -f $(DEST)/share/icons/hicolor/48x48/actions/show-hidden.svg
	rm -f $(DEST)/share/icons/hicolor/32x32/actions/show-hidden.svg
	rm -f $(DEST)/share/icons/hicolor/24x24/actions/show-hidden.svg
	rm -f $(DEST)/share/icons/hicolor/22x22/actions/show-hidden.svg
	rm -f $(DEST)/share/icons/hicolor/16x16/actions/show-hidden.svg

uninstall_dirs:
	#uninstall directory python
	if [ -d $(DEST)/share/backintime/qt/__pycache__ ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/backintime/qt/__pycache__; fi
	if [ -d $(DEST)/share/backintime/qt ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/backintime/qt; fi

	#uninstall directory plugin
	if [ -d $(DEST)/share/backintime/plugins/__pycache__ ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/backintime/plugins/__pycache__; fi
	if [ -d $(DEST)/share/backintime/plugins ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/backintime/plugins; fi
	if [ -d $(DEST)/share/backintime ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/backintime; fi

	#uninstall directory application
	if [ -d $(DEST)/bin ]; then rmdir --ignore-fail-on-non-empty $(DEST)/bin; fi

	#uninstall directory dbus service
	if [ -d $(DEST)/share/dbus-1/system-services ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/dbus-1/system-services; fi
	if [ -d $(DEST)/share/dbus-1 ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/dbus-1; fi

	#uninstall directory dbus conf
	if [ -d $(DEST)/../etc/dbus-1/system.d ]; then rmdir --ignore-fail-on-non-empty $(DEST)/../etc/dbus-1/system.d; fi
	if [ -d $(DEST)/../etc/dbus-1 ]; then rmdir --ignore-fail-on-non-empty $(DEST)/../etc/dbus-1; fi
	if [ -d $(DEST)/../etc ]; then rmdir --ignore-fail-on-non-empty $(DEST)/../etc; fi

	#uninstall directory polkit action
	if [ -d $(DEST)/share/polkit-1/actions ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/polkit-1/actions; fi
	if [ -d $(DEST)/share/polkit-1 ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/polkit-1; fi

	#uninstall directory documentation
	if [ -d $(DEST)/share/doc/backintime-qt ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/doc/backintime-qt; fi

	#uninstall directory .desktop
	if [ -d $(DEST)/share/applications ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/applications; fi

	#uninstall directory docbook
	if [ -d $(DEST)/share/doc/qt/HTML/en/backintime ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/doc/qt/HTML/en/backintime; fi
	if [ -d $(DEST)/share/doc/qt/HTML/en ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/doc/qt/HTML/en; fi
	if [ -d $(DEST)/share/doc/qt/HTML ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/doc/qt/HTML; fi
	if [ -d $(DEST)/share/doc/qt ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/doc/qt; fi
	if [ -d $(DEST)/share/doc ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/doc; fi

	#uninstall directory man
	if [ -d $(DEST)/share/man/man1 ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/man/man1; fi
	if [ -d $(DEST)/share/man ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/man; fi

	#uninstall directory icons
	if [ -d $(DEST)/share/icons/hicolor/scalable/actions ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/scalable/actions; fi
	if [ -d $(DEST)/share/icons/hicolor/scalable ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/scalable; fi
	if [ -d $(DEST)/share/icons/hicolor/48x48/actions ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/48x48/actions; fi
	if [ -d $(DEST)/share/icons/hicolor/48x48 ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/48x48; fi
	if [ -d $(DEST)/share/icons/hicolor/32x32/actions ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/32x32/actions; fi
	if [ -d $(DEST)/share/icons/hicolor/32x32 ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/32x32; fi
	if [ -d $(DEST)/share/icons/hicolor/24x24/actions ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/24x24/actions; fi
	if [ -d $(DEST)/share/icons/hicolor/24x24 ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/24x24; fi
	if [ -d $(DEST)/share/icons/hicolor/22x22/actions ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/22x22/actions; fi
	if [ -d $(DEST)/share/icons/hicolor/22x22 ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/22x22; fi
	if [ -d $(DEST)/share/icons/hicolor/16x16/actions ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/16x16/actions; fi
	if [ -d $(DEST)/share/icons/hicolor/16x16 ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor/16x16; fi
	if [ -d $(DEST)/share/icons/hicolor ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons/hicolor; fi
	if [ -d $(DEST)/share/icons ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share/icons; fi
	if [ -d $(DEST)/share ]; then rmdir --ignore-fail-on-non-empty $(DEST)/share; fi

